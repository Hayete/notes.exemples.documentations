#REDUX
------

*Les Hooks de base

    *useState
    *useEffect
    *useContext

*Hooks supplémentaires

    *useReducer
    *useCallback
    *useMemo
    *useRef
    *useImperativeHandle
    *useLayoutEffect
    *useDebugValue



001 | Redux - Présentation => https://youtu.be/kYxhuL1yXaA
----------------------------------------------------------


![img_39.png](img_39.png)

C'est un magasin de données, où l'on peut récupérer les données, et où modifier les données au sein de notre application,
ce state global est accessible par tous les composants de notre application.


002 | Redux - Installation => https://youtu.be/BBPaJGxFdz4
----------------------------------------------------------
Redux Toolkit.org=>quick start=>

DANS LE CAS D'UNE NOUVELLE APPLICATION : 

| npm install @reduxjs/toolkit react-redux |
|------------------------------------------|

SINON :

 
 | import { configureStore } from '@reduxjs/toolkit' |
 |---------------------------------------------------|

  export default configureStore({                   
  reducer: {}                                       
  })                                                
 

003 | Redux - Simple state => https://youtu.be/95F8fpeTWxk
----------------------------------------------------------
créer dossier store.js

import './store'; (dans App.js)

reprendre le store.js (on peut vérifier dans la console.log que le fichier est bien configuré avec 'ok' :

import * as Redux from '@redux/toolkit';
````js
function counter(state, action) {
    if(typeof state === 'indefined') { //point de vérification
        return 0;
    }
    console.log('counter :', action, state); //C'est facultatif c'est juste pour vérifier.
    
    switch (action.type) {
        case 'INCREMENT': //S'il y a plusieurs mots on mets un tiret à chaque fois avec le 8.
            return state + 1;
        case  'DECREMENTE':
            return state - 1;
        default: return state;

    }
}
const store = Redux.createStore(counter);
window.addEventListener(type:'click', listener: function() {
    store.dispatch(action: {type : 'INCREMENT'});//On ajoute paylod si notre action a une données.
    
});
````

004 | Redux - Store courant => https://youtu.be/wsjJI86dXzo
-----------------------------------------------------------
````js
//Types
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';
//Actions
function increment() {
    return (type: increment);
} 

function decrement() {
    return (type: decrement);
    
}
//Reducer
function counter(state: number = 0, action) {

console.log('counter :', action, state); //C'est facultatif c'est juste pour vérifier.

    switch (action.type) {
        case 'INCREMENT': //S'il y a plusieurs mots on mets un tiret à chaque fois avec le 8.
            return state + 1;
        case  'DECREMENT':
            return state - 1;
        default: return state;

    }
}
const store = Redux.createStore(counter);
window.addEventListener(type:'click', listener: function() {
store.dispatch(action: {type : 'INCREMENT'});//On ajoute paylod si notre action a une données.
````
(type me retourne un objet)

005 | Redux - configureStore et l'extension Redux DevTools => https://youtu.be/gUPxHVsLUlY
------------------------------------------------------------------------------------------
Installez Redux DevTools

````js
//Types
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';
//Actions
function increment2() {
return (type: increment);
}
const incremente = Redux.createAction(INCREMENT);
const newAction = Redux.createAction(type: 'NEW_ACTION', prepareaction(nb) => {
    return {paylod: nb * 2};
});
    
function decrement() {
return (type: decrement);

}
//Reducer
function counter(state: number = 0, action) {

console.log('counter :', action, state); //C'est facultatif c'est juste pour vérifier.

    switch (action.type) {
        case 'INCREMENT': //S'il y a plusieurs mots on mets un tiret à chaque fois avec le 8.
            return state + 1;
        case  'DECREMENT':
            return state - 1;
        default: return state;

    }
}
//const store = Redux.createStore(counter);
const store = Redux.configureStore(options: {
    reducer: counter
});
window.addEventListener(type:'click', listener: function() {
store.dispatch(action: (increment));//On ajoute paylod si notre action a une données.
store.dispatch(newAction: (3));
````

006 | Redux - createAction => https://youtu.be/Pk-iQ7mteiQ
----------------------------------------------------------
![img_41.png](img_41.png)

007 | Redux - createReducer => https://youtu.be/errrE6qdftU
-----------------------------------------------------------

![img_44.png](img_44.png)
où
![img_45.png](img_45.png)
C'est presque la même chose.

008 | Redux - createSlice => https://youtu.be/KdA38JjHHrM
---------------------------------------------------------
![img_46.png](img_46.png)

009 | Redux - Récupérer le state global dans mes composants => https://youtu.be/kE4rfYetWmE
-------------------------------------------------------------------------------------------
  | npm i react-redux |
  |-------------------|

![img_47.png](img_47.png)
App.js
![img_48.png](img_48.png)

![img_49.png](img_49.png)

![img_50.png](img_50.png)

010 | Redux - Dispatcher une action => https://youtu.be/8fE_Nscck9M
-------------------------------------------------------------------
![img_51.png](img_51.png)

![img_52.png](img_52.png)

![img_53.png](img_53.png)
