*Analyser les URL visitées par les internautes n’est pas si simple, car cette information est généralement chiffrée, grâce au protocole HTTPS.*
*Mais il existe des solutions techniques pour y arriver quand même.*
*Avec son nouveau projet de loi antiterroriste, le gouvernement veut renforcer considérablement la surveillance du web pour y détecter de potentiels terroristes.*
*L’idée est de pouvoir collecter « les adresses complètes de ressources sur internet utilisées » par ces personnes.*
*Au micro de France Inter, le ministre de l’Intérieur précise qu’il s’agira de surveiller les URL afin de pouvoir, par exemple, détecter*
*« si quelqu’un regarde trois ou quatre fois une vidéo de décapitation de Daesch ».*


Mais il y a un léger hic. Les URL des pages ne sont généralement pas visibles, ni par les opérateurs ni par les fournisseurs d’accès.

La grande majorité des sites et des services web chiffrent les échanges avec leurs utilisateurs au moyen du protocole HTTPS.

Un navigateur qui reçoit de la part de l’utilisateur une demande de connexion à une URL va d’abord chercher à identifier l’adresse IP du serveur en question,

ce qui lui sera fourni par l’intermédiaire d’un service DNS. Le navigateur va ensuite établir un canal de communication chiffrée avec ce serveur, puis envoyer la requête d’URL.

Au mieux, les opérateurs et les fournisseurs d’accès pourront voir le domaine sur lequel se connecte l’utilisateur, mais jamais l’URL.

Celle-ci n’est accessible en clair que dans le navigateur et, évidemment, au niveau des serveurs du site ou du service. Dès lors, comment cette collecte pourrait-elle être

réalisée ?

Solution 1 : la force brute
--------------------------
Face à ce problème, une première idée serait de déchiffrer les échanges HTTPS. En effet, les services de renseignement travaillent en permanence sur des méthodes de cryptanalyse.

La NSA dispose d’un programme baptisé « Bullrun » pour, justement, casser le chiffrement sur la Toile, comme en témoignent les documents publiés par Edward Snowden.

La DGSE, de son côté, gère une « Plate-forme nationale de cryptanalyse et de décryptement » que les autres services de renseignement peuvent également utiliser.

Si les capacités de cet outil ne sont pas connues, il paraît peu probable qu’un déchiffrement d’une grande quantité de connexions HTTPS soit possible, d’autant plus que ce

traitement devrait se faire presque à la volée…

Solution 2 : la méthode kazakhe
-------------------------------
Il est possible de déchiffrer des flux HTTPS par la technique du « Man in the middle ». 

L’échange passe alors par un boîtier intermédiaire qui, côté utilisateur, se fait passer pour le site web et relaie les échanges avec ce dernier.

« Mais pour que ça marche, il faudrait installer un certificat racine du gouvernement sur tous les ordinateurs à surveiller », nous explique Stéphane Bortzmeyer,

ingénieur réseau. C’est d’ailleurs ce qui a été fait au Kazakhstan, où les fournisseurs d’accès ont contraint leurs utilisateurs à installer un tel certificat.

Dans un pays démocratique comme la France, ce n’est évidemment pas possible.

Solution 3 : le mouchard
------------------------
Comme l’URL est visible dans le navigateur, pourquoi ne pas y installer un mouchard qui transmette toutes les adresses qu’une personne visite ?

Il sera sans doute difficile de convaincre les éditeurs d’intégrer un tel logiciel-espion dans leur produit. Une alternative pourrait être alors une extension de navigateur.

Les chercheurs en sécurité tombent régulièrement sur des extensions malveillantes qui enregistrent tout l’historique de navigation, pour les besoins d’un ciblage publicitaire

par exemple. Mais là encore, les chances de succès sont faibles, car personne n’installera (de plein gré) une telle extension.

Solution 4 : la backdoor
------------------------
C’est la méthode qui fait saliver les forces de l’ordre. L’idée est d’imposer par la loi aux fournisseurs de services de communication et de services cloud à implémenter un

accès parallèle. Et ça marche ! Avec son programme « Prism », la NSA a pu accéder — et accède sans doute encore aujourd’hui — aux contenus des grandes plates-formes du web :

Google, Facebook, Microsoft, Yahoo, Apple, Skype, AOL, Paltalk, etc. L’accès ne se faisait pas directement, mais au travers d’un filtre logiciel.

Et c’était suffisamment fluide pour recevoir des notifications en temps réel. Avec un tel outil, il serait donc facile de savoir si quelqu’un consulte des vidéos de décapitation.


C’est bien sûr la dernière solution qui semble la plus simple à mettre en œuvre d’un point de vue technique.

*Reste à savoir si elle est juridiquement possible… et si les acteurs du web veulent bien coopérer.*

source : 01net
--------------
