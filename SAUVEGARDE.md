main.tsx

import React from "react"
import "../../App.css"
import aside from "./aside"
import lessonPage from "./lessonPage";

const mainForm = undefined;

function Main(){
    return (
        <div className='main'>
            <aside></aside>
            <lessonPage></lessonPage>
        </div>
    )
}

export default Main;

aside.tsx

import React from 'react'
import '../../App.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faBook, faTimes, faPen } from '@fortawesome/free-solid-svg-icons'

function Aside(){
    return(
        <aside className="aside">
            <div className="container-menu">
                <div className="menu-chapitre ajout">
                    <FontAwesomeIcon icon={faBars} />
                    <p>Ajout chapitre</p>
                </div>
                <div className="menu-lesson ajout">
                    <FontAwesomeIcon icon={faBook} />
                    <p>Ajout Lesson</p>
                </div>
            </div>
            <div className="container-items">
                <ul className="fa-ul">
                    <li className="big-item">
                        <div className="rg">
                            <div className="st">
                            <span className="fa-li icp">
                                <FontAwesomeIcon icon={faBars} />
                            </span>
                                <h6>lorem ipsum chapitre 1</h6>
                            </div>

                            <div className="container-icon-big-items">
                                <div className="i">
                                    <FontAwesomeIcon icon={faTimes} />
                                </div>
                                <div className="i">
                                    <FontAwesomeIcon icon={faPen} />
                                </div>
                            </div>
                        </div>

                        <ul className="fa-ul fa-ul-me">
                            <li className="d2">
                                <span className="fa-li icp">
                                    <FontAwesomeIcon icon={faBars} />
                                </span>
                                <input placeholder="    Lorem 1 lorem ipsum" type="text"/>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
    )
}

export default Aside;

lessonPage.tsx

import React from 'react'
import '../../App.css'

function LessonPage(){
    return (
        <div className="container-lessonPage cl">

        </div>
    )
}

export default LessonPage;

npm install --save react-dropzone

**une archive est un .zip .rar .tar.gz**

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
//const cors = require('cors');
//import express from 'express';
//import bodyParser from 'body-parser';
//import mongoose from 'mongoose';
//import cors from 'cors';

const app = express()

app.get('/pp', function (req, res){
  console.log('ok')
})


app.listen(8000, function (err){
  if(err){
    console.log(err)
  }
  console.log("le serveur a etait lancer sur le port 8000")
})
//dans le html du cv
<script type="text/javascript">
 //dans index.js
</script>

*sur LINKEDIN*

ékko ))) Sylvie CLERJON Thierry Bideault Philip Anderson pour plus de visibilité

//HTML/CV/TROUVER SUR LINKEDIN

````html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title> Visit Card (Glassmorphism) </title>
    <link href="VisitCard.css" rel="stylesheet">
</head>
<body>
    <section>
        <div class="card">
             <div class="face back">
                <div class="skill">
                    <h3>Mes Compétences</h3>
                    <li>
                        <h4>HTML</h4><span class="bar"><span class="HTML"></span></span>
                    </li>
                    <li>
                        <h4>CSS</h4><span class="bar"><span class="CSS"></span></span>
                    </li>
                    <li>
                        <h4>JavaScript</h4><span class="bar"><span class="JavaScript"></span></span>
                    </li>
                    <li>
                        <h4>PHP</h4><span class="bar"><span class="PHP"></span></span>
                    </li>
                </div>    
            </div>
            <div class="face front">
                    <div class="content">
                        <div class="picture"><img src="PhotoIDOrange.png" width="100px" height="150px"></div>
                        <div class="contentbox">
                            <h3>Alexandre CLER<br><span>Developpeur Web Junior </span></h3>
                            <a href=""> Compétences </a>
                            <a href="CV_Alexandre.CLER.pdf"> À propos </a>
                        </div>
                    </div>
                <ul class="network">
                    <li>
                        <a href="https://www.linkedin.com/feed/"><ion-icon name="logo-linkedin"></ion-icon></a>
                    </li>
                    <li>
                        <a href="https://github.com/alex-cler"><ion-icon name="logo-github"></ion-icon></a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/alexandre_cler/?hl=fr"><ion-icon name="logo-instagram"></ion-icon></a>
                    </li>
                </ul>
            </div>
           
        </div>                    
    </section>
    <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
    <script type="text/javascript">
        // Ajout de la classe au lien pour retourner
        document.querySelector( ".card .face.front .contentbox > a:first-of-type" ).classList.add( "fliptoggle" )
        // Ajout du lien pour retourner sur le back
        document.querySelector( ".card .face.back .skill" ).innerHTML += `<a href="" class="fliptoggle"><ion-icon name="return-down-back-outline"></ion-icon></a>`;

        // Code pour retourner la carte avec les liens
        document.querySelectorAll( "a.fliptoggle" ).forEach( a => {
            a.addEventListener( "click", e => {
                e.preventDefault();
                document.querySelector( "body > section > .card" ).classList.toggle( "flipped" )
            } )
        } )
    </script>

    <style>
        
        /* Animations */
        .HTML,
        .CSS,
        .JavaScript,
        .PHP {
            animation: none;
        }
        /* Preserve 3d */
        .card,
        .card .face {
            transform-style: flat;
        }
        /* Perspective */
        .card {
            perspective: none;
        }

        /* -- Hidden Face -- */
        /* Hidden */
        .card:not( .flipped ) .face.back {
            transform: rotateY( -90deg );
            transition-timing-function: linear;
        }
        /* Display */
        .card.flipped .face.back {
            transition-delay: 1s;
            transition-timing-function: linear;
        }
        /* Animations when visible */
        .card.flipped .face.back .HTML {
            animation: HTML 3s 1s;
        }

        .card.flipped .face.back .CSS {
            animation: CSS 3s 1s;
        }

        .card.flipped .face.back .JavaScript {
            animation: JavaScript 3s 1s;
        }

        .card.flipped .face.back .PHP {
            animation: PHP 3s 1s;
        }

        /* -- Visible face -- */
        /* hidden */
        .card.flipped .face.front {
            transform: rotateY( 90deg );
            transition-timing-function: linear;
        }
        /* show */
        .card:not( .flipped ) .face.front {
            transition-delay: 1s;
            transition-timing-function: linear;
        }
    </style>
</body>
</html>
````
//CSS/CV
````css
*{
  margin: 0;
  padding: 0;
  list-style: none;
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
}

body {
  background: rgb(0, 0, 30);
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
}

/*Right Square*/
section::before {
  content:"";
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient( #2ed495,#0f8bdd);
  clip-path: circle(35% at 82% 80%);/*(dimension du cercle at center x center y)*/
}

/*Left Square*/
section::after {
  content: "";
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(to right, #ac2e0f, #fda500);
  clip-path: circle(35% at 20% 5%); /*(dimension du cercle at center x center y)*/
}

/*Card Shape*/
.card {
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 175px;
  left: -20px;
  width: 325px;
  height: 425px;
  transform-style: preserve-3d;
  perspective: 400px;
}

/*Setting Card*/
.card .face {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(255, 255, 255, 0.10);
  margin: 20px;
  box-shadow: 0 15px 35px rgba(0, 0, 0, 0.05);
  border-radius: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  transform-style: preserve-3d;
  transition: 1s;
  -webkit-backdrop-filter: blur(10px);
  backdrop-filter: blur(10px);
  backface-visibility: hidden;
}

/*Card content*/
.card .face.front .content {
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  opacity: 0.5;
  transition: 0.5s;
}

/*Hover Visibility*/
.card .face.front:hover .content {
  opacity: 1;
}

/*Image layout*/
.card .face.front .content .picture {
  position: relative;
  top: -25px;
  width: 140px;
  height: 140px;
  border-radius: 100%;
  overflow: hidden;
  border: 10px solid rgba(255, 255, 255, 0.05);
}

/*Image display*/
.card .face.front .content .picture img{
  position: absolute;
  top: 0;
  left: 0;
  object-fit: cover;
}

/*
tete de hayete
*/
.photo-profil{
  width: 100%;
}
/*Text setting*/
.card .face.front .content .contentbox h3 {
  color: lightgray;
  text-align: center;
  font-weight: 600;
  font-size: 18px;
  margin: 0 0 10px 0;
  line-height: 1.2em;
}

/*Texte size*/
.card .face.front .content .contentbox h3 span {
  text-transform: initial;
  font-weight: 500;
  font-size: 15px;
}

/*Button*/
.card .face.front .content .contentbox a {
  text-decoration: none;
  color:white;
  border: solid 1px #fda500;
  padding: 10px 15px;
  margin: 20px 5px 20px 5px;
  text-align: center;
  display: inline-block;
  font-size: 12px;
  opacity: 0;
}

/*Color hover*/
.card .face.front .content .contentbox a:hover {
  background-color:#e25808;
}

/*Visible hover*/
.card .face.front:hover .content .contentbox a {
  opacity: 1;
}

/*Position network*/
.card .face.front .network {
  position: absolute;
  bottom: 40px;
  display: flex;
}

/*Network style*/
.card .face.front .network li {
  list-style: none;
  margin: 0 10px 0 10px;
  transform: translateY(0px);
  transition: 0.5s;
}

/*Animation hover network*/
.card .face.front .network li:hover {
  transform : translateY(-10px)
}

/*Before hover*/
.card .face.front .network li a {
  color: white;
  font-size: 20px;
  opacity: 0;
}

/*Visible after hover*/
.card .face.front:hover .network li a {
  opacity: 1;
}

/*Font skill*/
h4 {
  font-size: 15px;
  font-weight: 100;
}

/*skill text*/
.skill {
  color: white;
}

/*Space between skill*/
.skill li {
  margin: 30px 0;
}

/*Skill bar*/
.bar {
  background-color: rgba(0, 0, 0, 0.20);
  border: 1px solid black;
  border-radius: 20px;
  display: block;
  width: 200px;
  height: 10px;
  margin-top: 10px;
}

/*Progressive bar*/
.bar span {
  height: 10px;
  float: left;
}

/*HTML bar*/
.HTML {
  background: linear-gradient( to right, #2ed495,#0f8bdd);
  width: 85%;
  border-radius: 20px;
  animation: HTML 3s;
}

/*CSS bar*/
.CSS {
  background: linear-gradient( to right, #2ed495,#0f8bdd);
  width: 70%;
  border-radius: 20px;
  animation: CSS 3s;
}

/*.JavaScript bar*/
.JavaScript {
  background: linear-gradient( to right, #2ed495,#0f8bdd);
  width: 55%;
  border-radius: 20px;
  animation: JavaScript 3s;
}

/*PHP bar*/
.PHP {
  background:linear-gradient( to right, #2ed495,#0f8bdd);
  width: 48%;
  border-radius: 20px;
  animation: PHP 3s;
}

/*Back toggle*/
.fliptoggle {
  text-decoration: none;
  color: white;
}

/*Skill bar animation*/
@keyframes HTML {
  0%{
    width: 0;
  }

  100%{
    width: 85%;
  }
}

@keyframes CSS {
  0%{
    width: 0;
  }

  100%{
    width: 70%;
  }
}

@keyframes JavaScript {
  0%{
    width: 0;
  }

  100%{
    width: 55%;
  }
}

@keyframes PHP {
  0%{
    width: 0;
  }

  100%{
    width: 48%;
  }
}
````
//JS/je l'ai enlevé au html(trop d'erreurs)

import './index.html';

// Ajout de la classe au lien pour retourner
document.querySelector( ".card .face.front .contentbox > a:first-of-type" ).classList.add( "fliptoggle" )
// Ajout du lien pour retourner sur le back
document.querySelector( ".card .face.back .skill" ).innerHTML += `<a href="" class="fliptoggle"><ion-icon name="return-down-back-outline"></ion-icon></a>`;

// Code pour retourner la carte avec les liens
document.querySelectorAll( "a.fliptoggle" ).forEach( a => {
  a.addEventListener( "click", e => {
    e.preventDefault();
    document.querySelector( "body > section > .card" ).classList.toggle( "flipped" )
  } )
} )
//test
-----Vous devez faire un formulaire conforme au modèle qui sauvegardera le rendu dans la base de données, il faut alors pouvoir trouver la liste des messages avec le formulaire. Choisissez votre framework, librairies ecc ..
