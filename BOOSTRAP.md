#BOOSTRAP
=========

*Framework pour concevoir et personnaliser des sites mobiles et réactifs, avec la boîte à

outils open source, front-end.

Avec un système de grille réactif, des composants préconstruits étendus et de puissants

plugins JavaScript, il est simple d'utilisation, il n'y a qu'à copié/coller et paramétrer

si besoin!*

Comment cela fonctionne
-----------------------
Le système de grille de Bootstrap utilise une série de conteneurs, de lignes et de colonnes

pour mettre en page et aligner le contenu. Il est construit avec flexbox et est entièrement

réactif. Voici un exemple et un regard approfondi sur la façon dont la grille se réunit.

Nouveau ou peu familier avec flexbox? Lisez ce guide flexbox CSS Tricks pour obtenir des 

extraits d’arrière-plan, de terminologie, de lignes directrices et de code.

... Une des trois colonnes
    Une des trois colonnes
    Une des trois colonnes
    Copie
    <div class="container">
      <div class="row">
        <div class="col-sm">
          One of three columns
        </div>
        <div class="col-sm">
          One of three columns
        </div>
        <div class="col-sm">
          One of three columns
        </div>
      </div>
    </div>...

L’exemple ci-dessus crée trois colonnes de largeur égale sur les petits, moyens, grands et

très grands appareils en utilisant nos classes de grille prédéfinis. Ces colonnes sont 

centrées dans la page avec le parent **..container**
                                      
Décomposer, voici comment cela fonctionne:


Les conteneurs fournissent un moyen de centrer et de tamponer horizontalement le contenu de

votre site. Utilisez-le pour une largeur de pixel réactive ou pour toutes les tailles de 

viewport et d’appareil..container.container-fluidwidth: 100%

Les lignes sont des emballages pour les colonnes. Chaque colonne a horizontale (appelée 

gouttière) pour contrôler l’espace entre eux. Ceci est ensuite contrecarré sur les lignes

avec des marges négatives. De cette façon, tout le contenu de vos colonnes est visuellement

aligné sur le côté gauche.paddingpadding

Dans une mise en page de grille, le contenu doit être placé dans des colonnes et seules les

colonnes peuvent être des enfants immédiats de lignes.

Grâce à flexbox, les colonnes de grille sans spécifié s’agencent automatiquement sous forme

de colonnes de largeur égale. Par exemple, quatre cas de volonté seront automatiquement 25%

de large à partir du petit point de rupture et vers le haut. Consultez la section colonnes 

de mise en page automatique pour plus d’exemples.width.col-sm

Les classes de colonne indiquent le nombre de colonnes que vous souhaitez utiliser sur les 

12 possibles par ligne. Donc, si vous voulez trois colonnes de largeur égale à travers, vous

pouvez utiliser ..col-4

Les colonnes sont définies en pourcentages, de sorte qu’elles sont toujours fluides et

dimensionnées par rapport à leur élément parent.width

Colonnes ont horizontale pour créer les gouttières entre les colonnes individuelles,

cependant, vous pouvez supprimer les rangées et des colonnes avec sur le 

.paddingmarginpadding.no-gutters.row

Pour rendre la grille réactive, il y a cinq points d’arrêt de grille, un pour chaque point

de ruptureréactif : tous les points d’arrêt (extra petits), petits, moyens, grands et extra

grands.

Les points d’arrêt de grille sont basés sur des requêtes multimédias de largeur minimale, ce

qui signifie qu’ils s’appliquent à ce point de rupture et à tous ceux qui sont au-dessus 

(p. ex., s’appliquent aux petits, moyens, grands et extra gros appareils, mais pas au 

premier point d’arrêt)..col-sm-4xs

Vous pouvez utiliser des classes de grille prédéfinis (comme) ou des mixines Sass pour un 

balisage plus sémantique..col-4

Soyez conscient des limitations et des bogues autour de flexbox,comme l’incapacité 

d’utiliser certains éléments HTML comme conteneurs flex.


<html lang="fr">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="styles.css">
  <title>Mon super cv</title>
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-3">
      <img src="../assets/img/lili.jpg" class="img-fluid" alt="Jsuis trop bonne">
    </div>
    <div class="col-9 border-purple">
      <h1>Infirmière en chirurgie pédiatrique</h1>
      <p>Infirmière diplômée d’Etat spécialisée en pédiatrie, j’ai toujours voulu travailler avec les enfants et œuvrer
        pour leur bien-être. C’est donc tout naturellement que je me suis orientée vers la pédiatrie ! Forte de mes
        différentes expériences professionnelle dans des hôpitaux pour enfants, je suis capable aujourd’hui de prendre
        en charge et d’administrer des soins à des nouveaux nés en situation d’urgence. Afin de me diversifier, je
        recherche aujourd’hui un poste dans un service de chirurgie pédiatrique. Sachez que j’adore travailler en équipe
        ! </p>
    </div>
  </div>
  <div class="row">
    <div class="col-3 border-orange">
      <h2>Infos personnelles</h2>
      <ul>
        <li>2, rue du Paradis, 75004 Paris</li>
        <li>Tél. : 06 24 00 00 01</li>
        <li>juliette@gmail.com</li>
        <li>Permis B, véhiculée</li>
        <li><a href="#">LinkedIn : Juliette Dacosta</a></li>
      </ul>
      <h2>Compétences</h2>
      <ul>
        <li>Prise en charge rapide des patients – Expert</li>
        <li>Gestes techniques et médicaux – Expert</li>
        <li>Microsoft office – niveau avancé</li>
      </ul>
      <h2>Qualités</h2>
      <ul>
        <li>L’esprit d’équipe</li>
        <li>La capacité d’écoute</li>
        <li>La rigueur</li>
      </ul>
    </div>
    <div class="col-9 border-blue align-content-between">
      <h2>Expériences professionnelles</h2>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
        <div class="col-3 border-purple">
          <h3>Infirmière pédiatrique</h3>
          <i>Juin 2015 – aujourd’hui</i>
          <p>Hôpital pour enfants Rousseau, Paris</p>
        </div>
        <div class="col-9 border-orange">
          <ul>
            <li>Membre de l’équipe affectée aux soins d’urgence</li>
            <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
            <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
          </ul>
        </div>
      </div>
      <div class="row">
      <div class="col-3 border-purple">
        <h3>Infirmière pédiatrique</h3>
        <i>Juin 2015 – aujourd’hui</i>
        <p>Hôpital pour enfants Rousseau, Paris</p>
      </div>
      <div class="col-9 border-orange">
        <ul>
          <li>Membre de l’équipe affectée aux soins d’urgence</li>
          <li>Elaboration de protocoles et de fiches de surveillance avec les médecins</li>
          <li>Encadrement des différentes catégories de stagiaires ( PDE, IDES, sage femme)</li>
        </ul>
      </div>
    </div>
    </div>
  </div>
  <div class="row border-black">
    <div class="col-5 offset-2 border-purple">
      <h2>Langues étrangères</h2>
      <ul>
        <li>Français – langue maternelle</li>
        <li>Anglais – courant</li>
      </ul>
    </div>
    <div class="col-5 border-orange">
      <h2>Langues étrangères</h2>
      <ul>
        <li>Français – langue maternelle</li>
        <li>Anglais – courant</li>
      </ul>
    </div>

  </div>
</div>
</body>
</html>




