#NGRX.
------

001 | NGRX - Introduction => https://youtu.be/sZp9A_uN_74
---------------------------------------------------------

002 | NGRX - Installation => https://youtu.be/nb-D3Zm_ipw
----------------------------------------------------------


003 | NGRX - Présentation du projet => https://youtu.be/gbrORy0-CBg
-------------------------------------------------------------------


004 | NGRX - Mise en place d'un store => https://youtu.be/Le5UmFzEpFI
---------------------------------------------------------------------


005 | NGRX - Utilisation du Store dans nos composants => https://youtu.be/bc9zrndtk7E
-------------------------------------------------------------------------------------


006 | NGRX - StoreDevTools && Redux DevTools => https://youtu.be/U0hdVoGq2Ho
----------------------------------------------------------------------------


007 | NGRX - createAction => https://youtu.be/MYz4VH8NTkA
---------------------------------------------------------


008 | NGRX - createReducer => https://youtu.be/PmF8htB1mYw
----------------------------------------------------------


