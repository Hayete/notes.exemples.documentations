#REACT
======
![img_5.png](img_5.png)

"Une bibliothèque légère pour créer des interfaces utilisateurs."

##001. React - Presentation => https://youtu.be/Bd06YvcdbmI
---------------------------------------------------------
Une bibliothèque légère pour créer des interfaces utilisateurs.

- chaque composant est lié à un état.
- Lorsque l'état est modifié react va mettre à jour le html.
- utilisateurs principaux : facebook, twitter, RB'NB, instagram, ...
- possibilité d'ajouté des librairies externes via react.
- jsx => surcouche de react
- UN composant gère UN état.

##002. React - Premier element => https://youtu.be/HCPBKUoZoHo
------------------------------------------------------------
- Copier les liens cdn.
  <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
  <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
  Les versions ci-dessus ne doivent être utilisées qu’à des fins de développement et ne sont pas adaptées à l’utilisation en production. 
  
  Les versions minifiées et optimisées pour la production sont disponibles ici :

  <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
  <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
  Pour charger une version spécifique de react et react-dom, il suffit de remplacer le 16 par la version souhaitée.

  Pourquoi utiliser l’attribut crossorigin ?
  Si vous chargez React depuis un CDN, nous vous recommandons de conserver l’attribut crossorigin :

  <script crossorigin src="..."></script>
  Nous vous conseillons également de vérifier que le CDN que vous utilisez définit bien l’en-tête HTTP Access-Control-Allow-Origin: * :

  Access-Control-Allow-Origin: *
  Cela améliore l’expérience de gestion des erreurs avec React 16 et des versions ultérieures.
- Ouvrir une page html, et coller les deux scripts.
- Ouvrir la console pour s'assurer que le DOM est bien chargé.
  
- Ouvrir un nouveau fichier js et le lier à html.

##TYPESCRIPT AVEC REACT
-----------------------  
  Pour démarrer un nouveau projet d’application Create React avec TypeScript,vous pouvez exécuter :
**npx create-react-app my-app --template typescript**

# or

**yarn create react-app my-app --template typescript**

Si vous avez déjà installé globalement via , nous vous recommandons de désinstaller le paquet en utilisant ou pour s’assurer que utilise
toujours la dernière version.

**create-react-app npm install -g create-react-app npm** 
**uninstall -g create-react-appyarn global remove create-react-appnpx**

Les installations globales de ne sont plus prises en charge.
**create-react-app**

Pour ajouter TypeScript à un projet d’application Create React existant, installez-le d’abord :
**npm install --save typescript @types/node @types/react @types/react-dom @types/jest**

# or

**yarn add typescript @types/node @types/react @types/react-dom @types/jest**

Ensuite, renommez n’importe quel fichier comme fichier TypeScript (p. ex. pour ) et redémarrer votre serveur de développement!
**src/index.js src/index.tsx**

Les erreurs de type s’afficheront dans la même console que celle de la génération.
Vous devrez corriger ces erreurs de type avant de poursuivre le développement ou de construire votre projet.
Pour une configuration avancée, [voir ici](https://create-react-app.dev/docs/advanced-configuration/)

##Dépannage

Si votre projet n’est pas créé avec TypeScript activé, npx peut utiliser une version mise en cache de .

Supprimer les versions précédemment installées avec ou (voir #6119).

**create-react-app npm uninstall -g** 
**create-react-app yarn global remove create-react-app**

Si vous utilisez actuellement create-react-app-typescript, consultez ce blog pour des instructions sur la façon de migrer vers Créer

React App.

Les énumérations et les espaces nominatifs constants ne sont pas pris en charge, vous pouvez en apprendre davantage sur les contraintes

liées à l’utilisation de Babel avec TypeScript [ici](https://babeljs.io/docs/en/babel-plugin-transform-typescript#caveats)

##003. React - Syntaxe JSX => https://youtu.be/_NdeKIm3QkQ
--------------------------------------------------------
- Pour utiliser cette syntaxe, on utilise un outil externe qui permet de compiler jsx en js natif.
- Babel/copier dans <script src = 'app.js' type = 'texte/babel'></script>
- Copier script puis coller dans html, juste avant le dernier script.
- Renommer app.js en app.jsx.
- <></> = fragment dans html, mais react ne le supporte pas donc, on rajoute 'react.fragment', on l'utilise
car react n'accepte qu'une seule racine par composant.
- Possibilité de copier/coller dans le convertisseur, pour vérifier le code en local.
- Aller dans référence de l'API : react-component
- cycle de vie du composant : monter/démonter
- les méthodes utilisables dans le composant : propriété des classes, propriétés d'instances.

##004. React - Declarer un composant => https://youtu.be/tQ8SP-YDae8
------------------------------------------------------------------
:small_red_triangle: Dans une classe, on initialise l’état local count à 0 en définissant this.state à { count: 0 } dans le constructeur :
````js
class Example extends React.Component {
constructor(props) {
super(props);
this.state = {
count: 0
};
}
````
:small_red_triangle: Dans une fonction composant, nous ne pouvons pas écrire ou lire this.state puisqu’il n’y a pas de this. Au lieu de ça, nous 
appelons directement le Hook useState dans notre composant :

import React, { useState } from 'react';

:small_red_triangle: function Example() {
// Déclare une nouvelle variable d'état, que nous appellerons « count »
const [count, setCount] = useState(0);
:small_red_triangle: Appeler useState, qu’est-ce que ça fait ? Ça déclare une « variable d’état ». Notre variable est appelée count mais nous aurions pu
l’appeler n’importe comment, par exemple banane. C’est un moyen de « préserver » des valeurs entre différents appels de fonctions.
useState est une nouvelle façon d’utiliser exactement les mêmes possibilités qu’offre this.state dans une classe. Normalement, 
les variables « disparaissent » quand la fonction s’achève mais les variables d’état sont préservées par React.

Q:small_red_triangle: u’est-ce qu’on passe à useState comme argument ? Le seul argument à passer au Hook useState() est l’état initial. Contrairement à 
ce qui se passe dans les classes, l’état local n’est pas obligatoirement un objet. Il peut s’agir d’un nombre ou d’une chaîne de
caractères si ça nous suffit. Dans notre exemple, nous voulons simplement le nombre de fois qu’un utilisateur a cliqué sur le 
bouton, nous passerons donc 0 comme état initial pour notre variable. (Si nous voulions stocker deux valeurs différentes dans 
l’état, nous appellerions useState() deux fois.)

:small_red_triangle: Que renvoie useState ? Elle renvoie une paire de valeurs : l’état actuel et une fonction pour le modifier. C’est pourquoi nous 
écrivons const [count, setCount] = useState(). C’est semblable à this.state.count et this.setState dans une classe, mais ici nous
les récupérons en même temps. Si vous n’êtes pas à l’aise avec la syntaxe que nous avons employée, nous y reviendrons en bas de 
cette page.

:small_red_triangle:Maintenant que nous savons ce que fait le Hook useState, notre exemple devrait commencer à être plus clair :

import React, { useState } from 'react';

````js
function Example() {
// Déclare une nouvelle variable d'état, que nous appellerons « count »
const [count, setCount] = useState(0);
````
:small_red_triangle: Nous déclarons une variable d‘état appelée count, et l’initialisons à 0. React se rappellera sa valeur entre deux affichages et
fournira la plus récente à notre fonction. Si nous voulons modifier la valeur de count, nous pouvons appeler setCount.

:small_red_triangle: Remarque

Vous vous demandez peut-être pourquoi useState n’est pas plutôt appelée createState ?

En fait, “create” ne serait pas tout à fait correct puisque l’état n’est créé qu’au premier affichage de notre composant. Les fois
suivantes, useState nous renvoie l’état actuel. Autrement, ce ne serait pas un état du tout ! Il y a aussi une raison pour laquelle 
les noms des Hooks commencent toujours par use. Nous découvrirons laquelle plus tard dans les Règles des Hooks.

Lire l’état
-----------
Quand nous voulons afficher la valeur actuelle de count dans une classe, nous récupérons la valeur de this.state.count :

  <p>Vous avez cliqué {this.state.count} fois</p>
Dans une fonction, nous pouvons directement utiliser count :

  <p>Vous avez cliqué {count} fois</p>
Mettre à jour l’état
Dans une classe, nous devons appeler this.setState() pour mettre à jour l’état count :

<button onClick={() => this.setState({ count: this.state.count + 1 })}>
Cliquez ici
</button>
Dans une fonction, nous récupérons directement setCount et count comme variables, nous n’avons donc pas besoin de this :

<button onClick={() => setCount(count + 1)}>
Cliquez ici
</button>
En résumé
Il est maintenant temps de récapituler ce que nous avons appris ligne par ligne et vérifier que nous avons bien compris.
````html
1:  import React, { useState } from 'react';
2:
3:  function Example() {
4:    const [count, setCount] = useState(0);
5:
6:    return (
7:      <div>
8:        <p>Vous avez cliqué {count} fois</p>
9:        <button onClick={() => setCount(count + 1)}>
10:         Cliquez ici
11:        </button>
12:      </div>
13:    );
14:  }
````
Ligne 1 : nous importons le Hook useState depuis React. Il nous permet d’utiliser un état local dans une fonction composant.
Ligne 4 : dans le composant Example, nous déclarons une nouvelle variable d’état en appelant le Hook useState. Il renvoie une paire
de valeurs que nous pouvons nommer à notre guise. Ici, nous appelons notre variable count puisqu’elle contient le nombre de clics
sur le bouton. Nous l’initialisons à zéro en passant 0 comme seul argument à useState. Le second élément renvoyé est une fonction.
Elle nous permet de modifier la variable count, nous l’appellerons donc setCount.
Ligne 9 : quand l’utilisateur clique, nous appelons setCount avec une nouvelle valeur. React rafraîchira le composant Example et 
lui passera la nouvelle valeur de count.
Ça fait peut-être beaucoup à digérer d’un coup. Ne vous pressez pas ! Si vous vous sentez un peu perdu·e, jetez un nouveau coup 
d’œil au code ci-dessus et essayez de le relire du début à la fin. Promis, une fois que vous essaierez « d’oublier » la manière 
dont fonctionne l’état local dans les classes, et que vous regarderez ce code avec un regard neuf, ça sera plus clair.

:small_red_triangle: Astuce : que signifient les crochets ?
Vous avez peut-être remarqué les crochets que nous utilisons lorsque nous déclarons une variable d’état :

const [count, setCount] = useState(0);
:small_red_triangle: Les noms utilisés dans la partie gauche ne font pas partie de l’API React. Vous pouvez nommer vos variables d’état comme ça vous 
chante :

const [fruit, setFruit] = useState('banane');
:small_red_triangle: Cette syntaxe Javascript est appelée « déstructuration positionnelle ». Ça signifie que nous créons deux nouvelles variables fruit 
et setFruit, avec fruit qui reçoit la première valeur renvoyée par useState, et setFruit qui reçoit la deuxième. C’est équivalent 
au code ci-dessous :

let fruitStateVariable = useState('banana'); // Renvoie une paire
let fruit = fruitStateVariable[0]; // Premier élément dans une paire
let setFruit = fruitStateVariable[1]; // Deuxième élément dans une paire
:small_red_triangle: Quand nous déclarons une variable d’état avec useState, ça renvoie une paire (un tableau avec deux éléments). Le premier élément 
est la valeur actuelle, et le deuxième est une fonction qui permet de la modifier. Utiliser [0] et [1] pour y accéder est un peu 
déconcertant puisqu’ils ont un sens spécifique. C’est pourquoi nous préférons plutôt utiliser la déstructuration positionnelle.

:small_red_triangle: Remarque

Vous vous demandez peut-être comment React sait à quel composant useState fait référence étant donné que nous ne lui passons plus
rien de similaire à this. Nous répondrons à cette question ainsi qu’à plein d’autres dans la section FAQ.

:small_red_triangle: Astuce : utiliser plusieurs variables d’état
Déclarer des variables d’état comme une paire de [quelquechose, setQuelquechose] est également pratique parce que ça nous permet de
donner des noms différents à des variables d’état différentes si nous voulons en utiliser plus d’une :
````html
function ExampleWithManyStates() {
// Déclarer plusieurs variables d'état !
const [age, setAge] = useState(42);
const [fruit, setFruit] = useState('banane');
const [todos, setTodos] = useState([{ text: 'Apprendre les Hooks' }]);
Dans le composant ci-dessus, nous avons age, fruit, et todos comme variables locales, et nous pouvons les modifier indépendamment 
les unes des autres :

function handleOrangeClick() {
// Similaire à this.setState({ fruit: 'orange' })
setFruit('orange');
}
````
:small_red_triangle: Utiliser plusieurs variables d’état n’est pas obligatoire. Les variables d’état peuvent tout à fait contenir des objets et des 
tableaux, vous pouvez donc toujours regrouper des données ensemble. Cependant, lorsque l’on modifie une variable d’état sa valeur 
est remplacée et non fusionnée, contrairement à this.setState dans les classes.

Découvrez les raisons de préférer séparer vos variables d’état dans la FAQ.

:small_red_triangle: Prochaines étapes
Dans cette section, vous avez appris à utiliser un des Hooks fournis par React, appelé useState. Nous y ferons parfois référence 
sous le terme « Hook d’état ». Il nous permet d’ajouter un état local à des fonctions composants React—ce qui est une première !

Nous en avons également appris un peu plus sur la nature des Hooks. Les Hooks sont des fonctions qui nous permettent de « nous
brancher » sur des fonctionnalités React depuis des fonctions composants. Leur noms commencent toujours par use, et il y a encore 
beaucoup de Hooks que nous n’avons pas encore vus !

Continuons maintenant en découvrant le prochain Hook : useEffect. Il vous permet de déclencher des effets de bord dans les 
composants, ce qui est similaire aux méthodes de cycle de vie dans les classes.

##005. React - Component lifecycle => https://youtu.be/OxbJrk2uy_s
----------------------------------------------------------------
"Le cycle de vie d'un composant"

On appelle ReactDOM.render() pour changer la sortie rendue :
````html
function tick() {
const element = (
<div>
<h1>Bonjour, monde !</h1>
<h2>Il est {new Date().toLocaleTimeString()}.</h2>
</div>
);
ReactDOM.render(
element,
document.getElementById('root')
);
}

setInterval(tick, 1000);
````

##006. React - Component state => https://youtu.be/-PVCxQM-bBI
------------------------------------------------------------ 
"On lui passe le paramétre state pour configurer."

:small_red_triangle: setState(updater, [callback])
setState() planifie des modifications à l’état local du composant, et indique à React que ce composant et ses enfants ont besoin 
d’être rafraîchis une fois l’état mis à jour. C’est en général ainsi qu’on met à jour l’interface utilisateur en réaction à des 
événements ou réponses réseau.

V:small_red_triangle: isualisez setState() comme une demande plutôt que comme une commande immédiate qui mettrait à jour le composant. Afin d’améliorer 
la performance perçue, React peut différer son traitement, pour ensuite mettre à jour plusieurs composants en une seule passe. 
React ne guarantit pas que les mises à jour d’état sont appliquées immédiatement.

:small_red_triangle: setState() ne met pas toujours immédiatement le composant à jour. Il peut regrouper les mises à jour voire les différer. En 
conséquence, lire la valeur de this.state juste après avoir appelé setState() est une mauvaise idée. Utilisez plutôt 
componentDidUpdate ou la fonction de rappel de setState (setState(updater, callback)), les deux bénéficiant d’une garantie de 
déclenchement après que la mise à jour aura été appliquée. Si vous avez besoin de mettre à jour l’état sur base de sa valeur 
précédente, lisez plus bas comment fonctionne l’argument updater.

:small_red_triangle: setState() causera toujours un rendu, à moins que shouldComponentUpdate() ne renvoie false. Si vous y utilisez des objets 
modifiables et que la logique de rendu conditionnel ne peut pas être implémentée dans shouldComponentUpdate(), appeler setState() 
seulement quand le nouvel état diffère du précédent évitera des rafraîchissements superflus.

|Le premier argument updater est une fonction dont la signature est :|
|--------------------------------------------------------------------|

(state, props) => stateChange
state est une référence à l’état local du composant au moment où cette modification est appliquée. Cet état ne devrait pas être 
modifié directement. Au lieu de ça, on représente les changements à apporter en construisant un nouvel objet basé sur les données 
entrantes de state et props. Par exemple, imaginons que nous voulions incrémenter une valeur dans l’état à raison de props.step :
````typescript
this.setState((state, props) => {
return {counter: state.counter + props.step};
});
````
:small_red_triangle: Tant le state que le props reçus par la fonction de mise à jour sont garantis à jour au moment de l’appel. La valeur de retour de 
la fonction est fusionnée (en surface, pas récursivement) avec state.

:small_red_triangle: Le second argument de setState() est une fonction de rappel optionnelle qui sera exécutée une fois que setState est terminé et le 
composant rafraîchi. D’une façon générale, nous vous recommandons plutôt d’utiliser componentDidUpdate() pour ce genre de besoin.

:small_red_triangle: Vous pouvez choisir de passer un objet au lieu d’une fonction comme premier argument à setState() :

setState(stateChange[, callback])
Ça procède à la fusion de surface de stateChange dans le nouvel état, par exemple pour ajuster la quantité d’une ligne de commande
dans un panier d’achats :

this.setState({quantity: 2})
Cette forme d’appel à setState() reste asynchrone, et des appels répétés au sein du même cycle pourraient être regroupés. Ainsi, 
si vous tentez d’incrémenter une quantité plus d’une fois dans le même cycle, vous obtiendrez l’équivalent de ceci :

Object.assign(
previousState,
{quantity: state.quantity + 1},
{quantity: state.quantity + 1},
...
)
:small_red_triangle: Les appels ultérieurs vont écraser les valeurs des appels précédents du même cycle, de sorte que la quantité ne sera réellement 
incrémentée qu’une fois. Lorsque l’état suivant dépend de l’état en vigueur, nous vous recommandons de toujours utiliser la forme 
fonctionnelle du paramètre updater :

this.setState((state) => {
return {quantity: state.quantity + 1};
});
Pour explorer ce sujet plus en détail, vous pouvez consulter :

Le guide État et cycle de vie
En profondeur : Quand et pourquoi les appels à setState() sont-ils regroupés ? (en anglais)
En profondeur : Pourquoi this.state ne déclenche-t-il pas une mise à jour immédiate ? (en anglais)
forceUpdate()
component.forceUpdate(callback)
Par défaut, lorsque l’état local ou les props de votre composant changent, ce dernier se rafraîchit. Si votre méthode render() 
dépend d’autres données, vous pouvez indiquer à React que le composant a besoin d’un rafraîchissement en appelant forceUpdate().

Appeler forceUpdate() déclenchera le render() du composant, en faisant l’impasse sur shouldComponentUpdate(). Ça déclenchera les 
méthodes usuelles de cycle de vie des composants enfants, y compris la méthode shouldComponentUpdate() de chaque enfant. React 
continuera à ne mettre à jour le DOM que si le balisage change.

De façon générale, vous devriez tout faire pour éviter de recourir à forceUpdate(), et faire que votre render() ne lise que 
this.props et this.state.

Propriétés de classes
defaultProps
defaultProps peut être définie comme propriété sur la classe du composant elle-même, pour définir les valeurs par défaut de props 
pour cette classe. On s’en sert pour les props undefined, mais pas pour celles à null. Par exemple :

class CustomButton extends React.Component {
// ...
}

CustomButton.defaultProps = {
color: 'blue'
};
Si props.color n’est pas fournie, elle sera définie par défaut à 'blue' :

render() {
return <CustomButton /> ; // props.color sera définie à 'blue'
}
Si props.color est définie à null, elle restera à null :

render() {
return <CustomButton color={null} /> ; // props.color reste à `null`
}
displayName
La chaîne de caractères displayName est utilisée dans les messages de débogage. La plupart du temps, vous n’avez pas besoin de la 
définir explicitement parce qu’elle est déduite du nom de la fonction ou classe qui définit le composant. Mais on peut vouloir la 
définir lorsqu’on veut afficher un nom différent pour des raisons de débogage ou lorsqu’on crée un composant d’ordre supérieur : 
vous trouverez plus de détails dans Enrober le nom d’affichage pour faciliter le débogage.

Propriétés d’instances
props
this.props contient les props définies par l’appelant de ce composant. Consultez Composants et props pour une introduction aux props.

Cas particulier : this.props.children est spéciale, généralement définie par les balises enfants dans l’expression JSX plutôt que 
dans la balise du composant lui-même.

state
L’état local contient des données spécifiques à ce composant, qui sont susceptibles d’évoluer avec le temps. C’est vous qui 
définissez l’état local, qui devrait être un objet JavaScript brut.

Lorsqu’une valeur n’est utilisée ni par l’affichage ni par le flux de données (par exemple, un ID de minuteur), vous n’avez pas à 
la mettre dans l’état local. Ce genre de valeurs peuvent être stockées comme champs de l’instance de composant.

Consultez État et cycle de vie pour de plus amples informations sur l’état local.

Ne modifiez jamais this.state directement, car appeler setState() par la suite risque d’écraser les modifications que vous auriez 
apportées. Traitez this.state comme s’il était immuable.

##007. React - Gérer les évènements => https://youtu.be/6TR1-9MU6J4
-----------------------------------------------------------------

Sur jsx on peut rajouter une écoute d'événement jsx 'onClick' = {} 

##008. React - Affichage conditionnel => https://youtu.be/W1LcnwIvjxQ
---------------------------------------------------------------------

##009. React - Lists key => https://youtu.be/Jd4OwxQlLiY
-------------------------------------------------------

##010. React - Faire remonter la donnée => https://youtu.be/r8CCKLIFPKk
-----------------------------------------------------------------------

##011. React - Create react app => https://youtu.be/FOk7T9D-5-o
---------------------------------------------------------------
npx create-react-app mon-app(et lui donner le nom de l'application)

npm run start

##012. React - Formulaire => https://youtu.be/DYISuMHXz6M
-----------------------------------------------------------

##013. React - Router => https://youtu.be/JSQbx5vKOOo
-----------------------------------------------------

Exo React => https://gitlab.com/wr-promo6/react-easy-peasy


##014. React - Axios http client => https://youtu.be/9o_xq_bIHSw
----------------------------------------------------------------

 **npm i axios** (pour requêter : copier puis coller sur le terminal dans le dossier de notre projet)

[axios npm](https://www.npmjs.com/package/axios)

const promise1 = new Promise((resolve, reject) => {
setTimeout(() => {
resolve('foo');
}, 300);
});

promise1.then((value) => {
console.log(value);
// expected output: "foo"
});

console.log(promise1);
// expected output: [object Promise]

##015. React - Axios http client => https://youtu.be/h_Z7DnsGC5w
----------------------------------------------------------------
NE PAS TOUCHER CODE POUR REACT EN JS 

const createByName = () => (name: "header");
function constructor(useElement: SVGUseElement, create: elementButton) {
Render(
<textPath>' '</textPath>,
<button disabled>sauvegarder</button>
)
}
console.log(name)
