1.1
===

[TYPESCRIPT1.1](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-1.html)

Meilleures règles de visibilité du module
TypeScript applique désormais strictement la visibilité des types dans les modules uniquement si l' --declarationindicateur est fourni. Ceci est très utile pour les scénarios angulaires, par exemple:
````typescript
module MyControllers {
interface ZooScope extends ng.IScope {
animals: Animal[];
}
export class ZooController {
// Used to be an error (cannot expose ZooScope), but now is only
// an error when trying to generate .d.ts files
constructor(public $scope: ZooScope) {}
/* more code */
}
}
````

[TYPESCRIPT1.3](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-3.html)

Protégé
Le nouveau protectedmodificateur dans les classes fonctionne comme il le fait dans des langages familiers tels que C ++, C # et Java. Un protectedmembre d'une classe n'est visible qu'à l'intérieur des sous-classes de la classe dans laquelle il est déclaré:
````typescript
class Thing {
protected doSomething() {
/* ... */
}
}
class MyThing extends Thing {
public myMethod() {
// OK, can access protected member from subclass
this.doSomething();
}
}
var t = new MyThing();
t.doSomething(); // Error, cannot call protected member from outside class
````

Types de tuples
Les types de tuples expriment un tableau dans lequel le type de certains éléments est connu, mais il n'est pas nécessaire que ce soit le 
même. Par exemple, vous souhaiterez peut-être représenter un tableau avec un stringà la position 0 et un numberà la position 1:

````typescript
// Declare a tuple type
var x: [string, number];
// Initialize it
x = ["hello", 10]; // OK
// Initialize it incorrectly
x = [10, "hello"]; // Error
Lors de l'accès à un élément avec un index connu, le type correct est récupéré:

console.log(x[0].substr(1)); // OK
console.log(x[1].substr(1)); // Error, 'number' does not have 'substr'
Notez que dans TypeScript 1.4, lors de l'accès à un élément en dehors de l'ensemble des indices connus, un type d'union est utilisé à la place:

x[3] = "world"; // OK
console.log(x[5].toString()); // OK, 'string' and 'number' both have toString
x[6] = true; // Error, boolean isn't number or string

````
[TYPESCRIPT1.4](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-4.html)

Types d'union
Aperçu
Les types d'union sont un moyen puissant d'exprimer une valeur qui peut être de plusieurs types. Par exemple, vous pouvez avoir une API pour exécuter un programme qui prend une ligne de commande comme a string, a string[]ou une fonction qui renvoie un string. Vous pouvez maintenant écrire:
````typescript
interface RunOptions {
program: string;
commandline: string[] | string | (() => string);
}
````
L'affectation aux types d'union fonctionne de manière très intuitive - tout ce que vous pourriez attribuer à l'un des membres du type d'union est attribuable à l'union:
````typescript
var opts: RunOptions = /* ... */;
opts.commandline = '-hello world'; // OK
opts.commandline = ['-hello', 'world']; // OK
opts.commandline = [42]; // Error, number is not string or string[]
````
Lors de la lecture à partir d'un type d'union, vous pouvez voir toutes les propriétés partagées par eux:
````typescript
if (opts.length === 0) {
// OK, string and string[] both have 'length' property
console.log("it's empty");
}
````
À l'aide de Type Guards, vous pouvez facilement travailler avec une variable de type union:
````typescript
function formatCommandline(c: string | string[]) {
if (typeof c === "string") {
return c.trim();
} else {
return c.join(" ");
}
}
````
Génériques plus stricts
Avec les types d'union capables de représenter un large éventail de scénarios de type, nous avons décidé d'améliorer la rigueur de certains appels génériques. Auparavant, un code comme celui-ci se compilait (étonnamment) sans erreur:
````typescript
function equal<T>(lhs: T, rhs: T): boolean {
return lhs === rhs;
}
// Previously: No error
// New behavior: Error, no best common type between 'string' and 'number'
var e = equal(42, "hello");
````
Avec les types d'union, vous pouvez désormais spécifier le comportement souhaité à la fois sur le site de déclaration de fonction et sur le site d'appel:
````typescript
// 'choose' function where types must match
function choose1<T>(a: T, b: T): T {
return Math.random() > 0.5 ? a : b;
}
var a = choose1("hello", 42); // Error
var b = choose1<string | number>("hello", 42); // OK
// 'choose' function where types need not match
function choose2<T, U>(a: T, b: U): T | U {
return Math.random() > 0.5 ? a : b;
}
var c = choose2("bar", "foo"); // OK, c: string
var d = choose2("hello", 42); // OK, d: string|number
````
Meilleur type d'inférence
Les types d'union permettent également une meilleure inférence de type dans les tableaux et autres endroits où vous pouvez avoir plusieurs types de valeurs dans une collection:
````typescript
var x = [1, "hello"]; // x: Array<string|number>
x[0] = "world"; // OK
x[0] = false; // Error, boolean is not string or number
let déclarations
En JavaScript, les vardéclarations sont «hissées» au sommet de leur portée englobante. Cela peut entraîner des bogues déroutants:

console.log(x); // meant to write 'y' here
/* later in the same block */
var x = "hello";
````
Le nouveau mot let- clé ES6 , désormais pris en charge dans TypeScript, déclare une variable avec une sémantique «bloc» plus intuitive. Une letvariable ne peut être référencée qu'après sa déclaration et porte sur le bloc syntaxique où elle est définie:
````typescript
if (foo) {
console.log(x); // Error, cannot refer to x before its declaration
let x = "hello";
} else {
console.log(x); // Error, x is not declared in this block
}
````
letest uniquement disponible lorsque vous ciblez ECMAScript 6 ( --target ES6).

const déclarations
L'autre nouveau type de déclaration ES6 pris en charge dans TypeScript est const. Une constvariable ne peut pas être affectée et doit être initialisée là où elle est déclarée. Ceci est utile pour les déclarations où vous ne souhaitez pas modifier la valeur après son initialisation:
````typescript
const halfPi = Math.PI / 2;
halfPi = 2; // Error, can't assign to a `const`
````
constest uniquement disponible lorsque vous ciblez ECMAScript 6 ( --target ES6).

Chaînes de modèle
TypeScript prend désormais en charge les chaînes de modèle ES6. Voici un moyen simple d'incorporer des expressions arbitraires dans des chaînes:
````typescript
var name = "TypeScript";
var greeting = `Hello, ${name}! Your name has ${name.length} characters`;
````
Lors de la compilation vers des cibles pré-ES6, la chaîne est décomposée:
````typescript
var name = "TypeScript!";
var greeting =
"Hello, " + name + "! Your name has " + name.length + " characters";
````
Protections de type
Un modèle courant en JavaScript consiste à utiliser typeofou instanceofà examiner le type d'une expression au moment de l'exécution. TypeScript comprend maintenant ces conditions et modifiera l'inférence de type en conséquence lorsqu'elle est utilisée dans un ifbloc.

Utilisation typeofpour tester une variable:
````typescript
var x: any = /* ... */;
if(typeof x === 'string') {
console.log(x.subtr(1)); // Error, 'subtr' does not exist on 'string'
}
// x is still any here
x.unknown(); // OK
````
Utilisation typeofavec les types d'union et else:
````typescript
var x: string | HTMLElement = /* ... */;
if(typeof x === 'string') {
// x is string here, as shown above
}
else {
// x is HTMLElement here
console.log(x.innerHTML);
}
````
Utilisation instanceofavec des classes et des types d'union:
````typescript
class Dog { woof() { } }
class Cat { meow() { } }
var pet: Dog|Cat = /* ... */;
if (pet instanceof Dog) {
pet.woof(); // OK
}
else {
pet.woof(); // Error
}
````
Alias ​​de type
Vous pouvez maintenant définir un alias pour un type à l'aide du typemot - clé:
````typescript
type PrimitiveArray = Array<string | number | boolean>;
type MyNumber = number;
type NgScope = ng.IScope;
type Callback = () => void;
````
Les alias de type sont exactement les mêmes que leurs types d'origine; ce sont simplement des noms alternatifs.

const enum (énumérations complètement incorporées)
Les énumérations sont très utiles, mais certains programmes n'ont pas réellement besoin du code généré et gagneraient à simplement insérer toutes les instances des membres enum avec leurs équivalents numériques. La nouvelle const enumdéclaration fonctionne comme une déclaration standard enumpour la sécurité des types, mais s'efface complètement au moment de la compilation.
````typescript
const enum Suit {
Clubs,
Diamonds,
Hearts,
Spades
}
var d = Suit.Diamonds;
````
Compile exactement:
````typescript
var d = 1;
````
TypeScript calculera également désormais les valeurs d'énumération lorsque cela est possible:
````typescript
enum MyFlags {
None = 0,
Neat = 1,
Cool = 2,
Awesome = 4,
Best = Neat | Cool | Awesome
}
var b = MyFlags.Best; // emits var b = 7;
````
-noEmitOnError option de ligne de commande
Le comportement par défaut du compilateur TypeScript est de continuer à émettre des fichiers .js en cas d'erreurs de type (par exemple, une tentative d'attribution de a stringà a number). Cela peut être indésirable sur les serveurs de build ou dans d'autres scénarios où seule la sortie d'une build «propre» est souhaitée. Le nouvel indicateur noEmitOnErrorempêche le compilateur d'émettre du code .js en cas d'erreur.

Il s'agit désormais de la valeur par défaut pour les projets MSBuild; cela permet à la construction incrémentielle de MSBuild de fonctionner comme prévu, car les sorties ne sont générées que sur des versions propres.

Noms des modules AMD
Par défaut, les modules AMD sont générés de manière anonyme. Cela peut conduire à des problèmes lorsque d'autres outils sont utilisés pour traiter les modules résultants comme un bundler (par exemple r.js).

La nouvelle amd-module namebalise permet de passer un nom de module optionnel au compilateur:
````typescript
//// [amdModule.ts]
///<amd-module name='NamedModule'/>
export class C {}
Cela entraînera l'attribution du nom NamedModuleau module dans le cadre de l'appel de l'AMD define:

//// [amdModule.js]
define("NamedModule", ["require", "exports"], function(require, exports) {
var C = (function() {
function C() {}
return C;
})();
exports.C = C;
});
````
[TYPESCRIPT1.5](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-5.html)

Modules ES6
TypeScript 1.5 prend en charge les modules ECMAScript 6 (ES6). Les modules ES6 sont en fait des modules externes TypeScript avec une nouvelle syntaxe: les modules ES6 sont des fichiers source chargés séparément qui peuvent éventuellement importer d'autres modules et fournir un certain nombre d'exportations accessibles de l'extérieur. Les modules ES6 comportent plusieurs nouvelles déclarations d'exportation et d'importation. Il est recommandé de mettre à jour les bibliothèques et applications TypeScript pour utiliser la nouvelle syntaxe, mais ce n'est pas obligatoire. La nouvelle syntaxe du module ES6 coexiste avec les constructions de module interne et externe d'origine de TypeScript et les constructions peuvent être mélangées et mises en correspondance à volonté.

Déclarations d'exportation
En plus de la prise en charge de TypeScript existante pour la décoration des déclarations avec export, les membres du module peuvent également être exportés à l'aide de déclarations d'exportation distinctes, en spécifiant éventuellement des noms différents pour les exportations à l'aide de asclauses.
````typescript
interface Stream { ... }
function writeToStream(stream: Stream, data: string) { ... }
export { Stream, writeToStream as write };  // writeToStream exported as write
````
Les déclarations d'importation peuvent également utiliser des asclauses pour spécifier différents noms locaux pour les importations. Par exemple:
````typescript
import { read, write, standardOutput as stdout } from "./inout";
var s = read(stdout);
write(stdout, s);
````
Comme alternative aux importations individuelles, une importation d'espace de noms peut être utilisée pour importer un module entier:
````typescript
import * as io from "./inout";
var s = io.read(io.standardOutput);
io.write(io.standardOutput, s);
````
Réexportation
En utilisant la fromclause, un module peut copier les exportations d'un module donné vers le module courant sans introduire de noms locaux.
````typescript
export { read, write, standardOutput as stdout } from "./inout";
````
export *peut être utilisé pour réexporter toutes les exportations d'un autre module. Ceci est utile pour créer des modules qui regroupent les exportations de plusieurs autres modules.
````typescript
export function transform(s: string): string { ... }
export * from "./mod1";
export * from "./mod2";
````
Exportation par défaut
Une déclaration d'exportation par défaut spécifie une expression qui devient l'exportation par défaut d'un module:
````typescript
export default class Greeter {
sayHello() {
console.log("Greetings!");
}
}
````
Qui à son tour peut être importé à l'aide des importations par défaut:
````typescript
import Greeter from "./greeter";
var g = new Greeter();
g.sayHello();
````
Importation nue
Une «importation nue» peut être utilisée pour importer un module uniquement pour ses effets secondaires.
````typescript
import "./polyfills";
````
Pour plus d'informations sur le module, veuillez consulter les spécifications de prise en charge du module ES6 .

Destructuration dans les déclarations et les cessions
TypeScript 1.5 ajoute la prise en charge des déclarations et affectations de déstructuration ES6.

Déclarations
Une déclaration de déstructuration introduit une ou plusieurs variables nommées et les initialise avec des valeurs extraites des propriétés d'un objet ou des éléments d'un tableau.

Par exemple, l'exemple suivant déclare les variables x, yet z, et les initialise à getSomeObject().x, getSomeObject().yet getSomeObject().zrespectivement:
````typescript
var { x, y, z } = getSomeObject();
````
Les déclarations de déstructuration fonctionnent également pour extraire des valeurs de tableaux:
````typescript
var [x, y, z = 10] = getSomeArray();
````
De même, la déstructuration peut être utilisée dans les déclarations de paramètres de fonction:
````typescript
function drawText({ text = "", location: [x, y] = [0, 0], bold = false }) {
// Draw text
}
// Call drawText with an object literal
var item = { text: "someText", location: [1, 2, 3], style: "italics" };
drawText(item);
````
Affectations
Les modèles de déstructuration peuvent également être utilisés dans les expressions d'affectation régulières. Par exemple, l'échange de deux variables peut être écrit comme une seule affectation de déstructuration:
````typescript
var x = 1;
var y = 2;
[x, y] = [y, x];
````
namespace mot-clé
TypeScript a utilisé le modulemot - clé pour définir à la fois «modules internes» et «modules externes»; cela a été un peu confus pour les développeurs novices dans TypeScript. Les «modules internes» sont plus proches de ce que la plupart des gens appelleraient un espace de noms; de même, les «modules externes» dans JS parlent en réalité ne sont plus que des modules maintenant.

Remarque: la syntaxe précédente définissant les modules internes est toujours prise en charge.

Avant :
````typescript
module Math {
export function add(x, y) { ... }
}
````
Après :
````typescript
namespace Math {
export function add(x, y) { ... }
}
````
letet constsupport
ES6 letet les constdéclarations sont désormais pris en charge lors du ciblage de ES3 et ES5.
````typescript
Const
const MAX = 100;
++MAX; // Error: The operand of an increment or decrement
//        operator cannot be a constant.
````
Étendue du bloc
````typescript
if (true) {
let a = 4;
// use a
} else {
let a = "string";
// use a
}
alert(a); // Error: a is not defined in this scope.
````
for..of support
TypeScript 1.5 ajoute la prise en charge d'ES6 pour..of boucles sur les baies pour ES3 / ES5 ainsi que la prise en charge complète des interfaces Iterator lors du ciblage d'ES6.

Exemple
Le compilateur TypeScript transpilera pour..des tableaux en JavaScript idiomatique ES3 / ES5 lors du ciblage de ces versions:
````typescript
for (var v of expr) {
}
````
sera émis comme:
````typescript
for (var _i = 0, _a = expr; _i < _a.length; _i++) {
var v = _a[_i];
}
````
Décorateurs
Les décorateurs TypeScript sont basés sur la proposition de décorateur ES7 .

Un décorateur c'est:

**une expression**
**qui évalue à une fonction**
**qui prend la cible, le nom et le descripteur de propriété comme arguments**
**et renvoie éventuellement un descripteur de propriété à installer sur l'objet cible**
**Pour plus d'informations, veuillez consulter la proposition des décorateurs.**

Exemple
Décorateurs readonlyet enumerable(false)seront appliqués à la propriété methodavant qu'elle ne soit installée sur la classe C. Cela permet au décorateur de modifier l'implémentation et, dans ce cas, d'augmenter le descripteur pour qu'il soit accessible en écriture: false et enumerable: false.
````typescript
class C {
@readonly
@enumerable(false)
method() { ... }
}
function readonly(target, key, descriptor) {
descriptor.writable = false;
}
function enumerable(value) {
return function (target, key, descriptor) {
descriptor.enumerable = value;
};
}
````
Propriétés calculées
L'initialisation d'un objet avec des propriétés dynamiques peut être un peu un fardeau. Prenons l'exemple suivant:
````typescript
type NeighborMap = { [name: string]: Node };
type Node = { name: string; neighbors: NeighborMap };
function makeNode(name: string, initialNeighbor: Node): Node {
var neighbors: NeighborMap = {};
neighbors[initialNeighbor.name] = initialNeighbor;
return { name: name, neighbors: neighbors };
}
````
Ici, nous devons créer une variable pour conserver la carte de voisinage afin de pouvoir l'initialiser. Avec TypeScript 1.5, nous pouvons laisser le compilateur faire le gros du travail:
````typescript
function makeNode(name: string, initialNeighbor: Node): Node {
return {
name: name,
neighbors: {
[initialNeighbor.name]: initialNeighbor,
},
};
}
````
Prise en charge UMDet Systemsortie du module
En plus des chargeurs de modules AMDet CommonJS, TypeScript prend désormais en charge les modules émetteurs UMD( Universal Module Definition ) et Systemles formats de module.

Utilisation :

tsc: module umd

et

tsc: système de modules

Le point de code Unicode s'échappe dans les chaînes
ES6 introduit des échappements qui permettent aux utilisateurs de représenter un point de code Unicode en utilisant un seul échappement.

À titre d'exemple, considérons la nécessité d'échapper à une chaîne contenant le caractère «𠮷». Dans UTF-16 / UCS2, «𠮷» est représenté comme une paire de substitution, ce qui signifie qu'il est codé à l'aide d'une paire d'unités de code 16 bits de valeurs, en particulier 0xD842et 0xDFB7. Auparavant, cela signifiait que vous deviez échapper au point de code en tant que "\uD842\uDFB7". Cela présente l'inconvénient majeur qu'il est difficile de discerner deux personnages indépendants d'une paire de substitution.

Avec les évasions codepoint de ES6, vous pouvez proprement représenter ce caractère exact dans les chaînes et les chaînes de modèle avec une évasion unique: "\u{20bb7}". TypeScript émettra la chaîne dans ES3 / ES5 en tant que "\uD842\uDFB7".

Chaînes de modèle marquées dans ES3 / ES5
Dans TypeScript 1.4, nous avons ajouté la prise en charge des chaînes de modèles pour toutes les cibles et des modèles balisés pour ES6 uniquement. Grâce à un travail considérable effectué par @ivogabe , nous avons comblé le fossé pour les modèles étiquetés dans ES3 et ES5.

Lorsque vous ciblez ES3 / ES5, le code suivant
````typescript
function oddRawStrings(strs: TemplateStringsArray, n1, n2) {
return strs.raw.filter((raw, index) => index % 2 === 1);
}
oddRawStrings`Hello \n${123} \t ${456}\n world`;
````
sera émis comme
````typescript
function oddRawStrings(strs, n1, n2) {
return strs.raw.filter(function (raw, index) {
return index % 2 === 1;
});
}
(_a = ["Hello \n", " \t ", "\n world"]),
(_a.raw = ["Hello \\n", " \\t ", "\\n world"]),
oddRawStrings(_a, 123, 456);
var _a;
````
Noms facultatifs des dépendances AMD
/// <amd-dependency path="x" />informe le compilateur d'une dépendance de module non TS qui doit être injectée dans l'appel require du module résultant; cependant, il n'y avait aucun moyen de consommer ce module dans le code TS.

La nouvelle amd-dependency namepropriété permet de passer un nom facultatif pour une dépendance amd:
````typescript
/// <amd-dependency path="legacy/moduleA" name="moduleA"/>
declare var moduleA: MyType;
moduleA.callStuff();
````
Code JS généré:
````typescript
define(["require", "exports", "legacy/moduleA"], function (
require,
exports,
moduleA
) {
moduleA.callStuff();
});
````
Accompagnement de projet à travers tsconfig.json
L'ajout d'un tsconfig.jsonfichier dans un répertoire indique que le répertoire est la racine d'un projet TypeScript. Le fichier tsconfig.json spécifie les fichiers racine et les options du compilateur nécessaires pour compiler le projet. Un projet est compilé de l'une des manières suivantes:

En appelant tsc sans fichiers d'entrée, auquel cas le compilateur recherche le fichier tsconfig.json en commençant dans le répertoire courant et en continuant dans la chaîne de répertoires parent.
En appelant tsc sans fichiers d'entrée et avec une option de ligne de commande -project (ou simplement -p) qui spécifie le chemin d'un répertoire contenant un fichier tsconfig.json.
Exemple
````typescript
{
"compilerOptions": {
"module": "commonjs",
"noImplicitAny": true,
"sourceMap": true
}
}
````
Voir la page wiki tsconfig.json pour plus de détails.

--rootDir option de ligne de commande
Option --outDirduplique la hiérarchie d'entrée dans la sortie. Le compilateur calcule la racine des fichiers d'entrée comme le chemin commun le plus long de tous les fichiers d'entrée; puis l'utilise pour répliquer toute sa sous-structure dans la sortie.

Parfois, cela n'est pas souhaitable, par exemple des entrées FolderA\FolderB\1.tset FolderA\FolderB\2.tsentraînerait une mise en miroir de la structure de sortie FolderA\FolderB\. Maintenant, si un nouveau fichier FolderA\3.tsest ajouté à l'entrée, la structure de sortie apparaîtra en miroir FolderA\.

--rootDir spécifie le répertoire d'entrée à mettre en miroir dans la sortie au lieu de le calculer.

--noEmitHelpers option de ligne de commande
Le compilateur TypeSript émet quelques helpers comme en __extendscas de besoin. Les helpers sont émis dans chaque fichier dans lequel ils sont référencés. Si vous souhaitez consolider tous les helpers au même endroit ou remplacer le comportement par défaut, utilisez --noEmitHelperspour indiquer au compilateur de ne pas les émettre.

--newLine option de ligne de commande
Par défaut, le caractère de nouvelle ligne de sortie est \r\nsur les systèmes Windows et \nsur les systèmes * nix. --newLineL'indicateur de ligne de commande permet de remplacer ce comportement et de spécifier le nouveau caractère de ligne à utiliser dans les fichiers de sortie générés.

--inlineSourceMapet inlineSourcesoptions de ligne de commande
--inlineSourceMapentraîne l'écriture en ligne des fichiers de mappage source dans les .jsfichiers générés plutôt que dans un .js.mapfichier indépendant . --inlineSourcespermet en outre d'incorporer le .tsfichier source dans le .jsfichier.
