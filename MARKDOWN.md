#MARKDOWN 
=========

:small_red_triangle: Markdown est un langage de balisage léger créé en 2004 par John Gruber avec l'aide d'Aaron Swartz1,2. Il a été créé dans
le but d'offrir une syntaxe facile à lire et à écrire. Un document balisé par Markdown peut être lu en l'état sans donner
l’impression d'avoir été balisé ou formaté par des instructions particulières.**

:small_red_triangle: Un document balisé par Markdown peut être converti en HTML, en PDF ou en d'autres formats.

*GUIDE DE SYNTAX
----------------
A utiliser n'importe où sur GitHub.com ou dans les fichiers texte.

*En-tête
--------
#TITRE TRES IMPORTANT
---------------------
##TITRE IMPORTANT
-----------------
###TITRE
--------
ETC,...
--------
*Liens
-----
:small_red_triangle: pour mettre un lien : [blabla] (http://blabla.fr).

:small_red_triangle: où (http://blabla.fr"blabla.fr") on l'ajoute pour être bien référencé.

:small_red_triangle: pour récupérer une image, aller sur la barre de navigation, faire inspecter, puis sur la ligne img, copier.

[![nom de l'image.fr]] (http://wwww.)+http./fr/im/now.png.jpg où autre.

:small_red_triangle: si on ajoute des crochets et l'htpp aprés, l'image devient cliquable.

:small_red_triangle: citation , pour mettre du code faire une tabulation puis coller le code.

:small_red_triangle: Lien automatique pour les URL

 (comme http://www.blabla.com/) sera automatiquement convertie en un lien cliquable.

*Espacement
-----------
D'une ligne à l'autre, il faut laisser un espace.

*Accentuation
-------------
*texte en italique*
_autre texte en italiqque_

**texte en gras**
__autre text en gras__

_ on **peut** les combinés _

*Listes
-------

*Non ordonnéés

* Item 1
* Item 2
  * Item 2a
  * Item 2b
*ordonnées

1. Item 1
2. Item 2
3. Item 3
   1. Item 3a
   2. Item 3b
*Images
-------
![GitHub Logo](/images/logo.png)

Format: ![Alt Text](url)

Liens

:black_medium_small_square: http://github.com - automatic!

:black_medium_small_square: [GitHub](http://github.com)

Blockquotes

:small_red_triangle: CITATION

> We're living the future so
> the present is our past.
Code en ligne
I think you should use an
`<addr>` element here instead.

*Image
------ 


![alt text](img/markdown_logo.png "Title Text")



![alt text1][logo]

:black_medium_small_square: [logo]: img/markdown_logo.png "TEXTE"

*Vidéo
------
Simple vidéo video :

![Sample Video](img/markdown_video.mp4)

simple clip audio :

![Sample Audio](img/markdown_audio.mp3)

*Mise en évidence de la syntaxe
------------------------------
*Voici un exemple de la façon d' utiliser la coloration syntaxique:

```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```

* Indenter le code de quatre espaces:

    function fancyAlert(arg) {
      if(arg) {
        $.facebox({div:'#foo'})
      }
    }
*Voici un exemple de code Python sans coloration syntaxique:

def foo():
    if not bar:
        return True

*Listes de tâches
----------------
- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item
Si vous incluez une liste de tâches dans le premier commentaire d'un problème,
vous obtiendrez un indicateur de progression pratique dans votre liste de problèmes.
Cela fonctionne également dans les demandes d'extraction!

*les tableaux
------------
:small_red_triangle: Vous pouvez créer des tableaux en assemblant une liste de mots et en les divisant par des tirets
-(pour la première ligne), puis en séparant chaque colonne par un tuyau |:

1er TITRE    | 2ème TITRE
------------ | -------------
Contient 1  | Contient 2
Contient 1en1er| Contient 2 en 2ème

| Titre 1       |     Titre 2     |        Titre 3 |
| :------------ | :-------------: | -------------: |
| Colonne       |     Colonne     |        Colonne |
| Alignée à     |   Alignée au    |      Alignée à |
| Gauche        |     Centre      |         Droite |

Deviendrait:

:small_red_triangle: Premier en-tête	Deuxième en-tête
Contenu de la cellule 1	Contenu de la cellule 2
Contenu dans la première colonne	Contenu dans la deuxième colonne
Références SHA
Toute référence au hachage SHA-1 d'un commit sera automatiquement convertie en lien vers ce commit sur GitHub.

:small_red_triangle: Pour centré un texte on utilise deux point aprés la barre |:

:small_red_triangle: Pour le mettre vers la droite on utilise le point virgule avant la barre ;|

*Barré
------
:small_red_triangle: Tout mot entouré de deux tildes (comme ~~this~~) apparaîtra barré.

*Emoji
------
:small_red_triangle: GitHub prend en charge les emoji !

:black_medium_small_square: https://www.webfx.com/tools/emoji-cheat-sheet/

*Code
-----

Pour écrire un morceau de code dans un texte, Markdown l’identifie au moyen du caractère appelé le Backtick ou

apostrophe inversée. Attention, à ne pas confondre avec les guillemets. Le marquage est donc constitué de 4 apostrophe

 inversée au début et à la fin du segment correspondant au code. C’est ainsi que vous pourrez incorporer directement le

  code source ou une commande logicielle dans le texte.

C’est le `code`.

Note

Attention cependant à ne pas écrire par mégarde un accent grave, par exemple : à. C’est ce qui risque d’arriver si vous

 insérez ce caractère devant une voyelle. Pour éviter ce problème, pensez à insérer une espace entre l’apostrophe

 inversée et la voyelle.

Si votre codage renferme déjà l’apostrophe inversée, vous devez précéder la zone de code de deux fois ce caractère.

Dans ce cas, Markdown n’interprétera pas l’apostrophe inversée comme une balise.

Pour voir une liste de toutes les images prises en charge, consultez la feuille de triche Emoji .

* essai
-------
//je les essayerais plus tard
First paragraph.
Another line in the same paragraph.
A third line in the same paragraph, but this time ending with two spaces.{space}{space}
A new line directly under the first paragraph.

Second paragraph.
Another line, this time ending with a backslash.\
A new line due to the previous backslash.

- This is an [inline-style link](https://www.google.com)
- This is a [link to a repository file in the same directory](index.md)
- This is a [relative link to a readme one directory higher](../README.md)
- This is a [link that also has title text](https://www.google.com "This link takes you to Google!")

Using header ID anchors:

- This links to [a section on a different Markdown page, using a "#" and the header ID](index.md#overview)
- This links to [a different section on the same page, using a "#" and the header ID](#header-ids-and-links)

Using references:

- This is a [reference-style link, see below][Arbitrary case-insensitive reference text]
- You can [use numbers for reference-style link definitions, see below][1]
- Or leave it empty and use the [link text itself][], see below.

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org/en-US/
[1]: https://slashdot.org
[link text itself]: https://www.reddit.com

- https://www.google.com
- https://www.google.com
- ftp://ftp.us.debian.org/debian/
- smb://foo/bar/baz
- irc://irc.freenode.net/
- http://localhost:3000
  [doc](#MARKDOWN https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/#:~:text=Markdown%20permet%20aussi%20d'ins%C3%A9rer,l'adresse%20URL%20entre%20parenth%C3%A8ses.
  )
  =========
