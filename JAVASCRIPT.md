#JAVASCRIPT 
==========

:small_red_triangle: Une fonction est un ensemble d'instructions, effectuant une tâche où calculant une valeur.

:small_red_triangle: Une fonction peut retourner des données où non.

:small_red_triangle: Pour utiliser une fonction, il est nécessaire de l'avoir définie, déclarer avant de l'utiliser.

![img_55.png](img_55.png)

//Fonction multiplyBy
--------------------
````js
function multiplyBy(nb, nb2){
    //instruction(s)
}
console.log(nb, nb);
console.log(nb2, nb2);
````

|Ces variables ne sont pas accessibles en dehors de cette fonction.|
|------------------------------------------------------------------|

//Return
--------
:small_red_triangle: Retourne tous types de données souhaitées(boulean, nombre, fonctions, où...)
````js
return nb * nb2;
return ('nb:', nb2)
multiplyBy(nb: 4, nb9: 2);
````

|Ce qui est entre () s'appelle 'argument'|
|----------------------------------------|

exemple;

|S'il n'y a que 2 arguments, on ne pourra utiliser que 2 arguments.|
|------------------------------------------------------------------|
````js
let produit = multiplyBy(nb: 42, nb2: 60);
console.log(produit:, produit);
````

|Donc le produit de la console est stockée dans produit.|
|-------------------------------------------------------|

|Appel de fonction = les différentes données entrées dans le multiplyBy|
|----------------------------------------------------------------------|

//Fonction checkValue
---------------------
````js
funtion checkValue(value: 30); true
function checkValue(value: 33); false
````
//Fonction value
----------------
````js
...function value(value value3 : number2){
    console.log(value >= value3);...

return : ok;
(pour retourner une donnée)
exemple :
let arr = [2, 3, 4, 9, 349, 49, 49];
````

1)
````js
...function afficheChaqueValeur(arr){
    console.log('Tableau:', arr);
    for let i = 0; i < arr.length; i++){
    console.log(arr[i];
}...
````

2)
````js
afficheChaqueValeur (arr);
afficheChaqueValeur (arr: ['Jean', 'Sam', 'Jo']);
````

3)
````js
function afiicheCHaqueValeur(arr){
    let nexArr = [];
    for(let i = 0; i< arr.length; i++){
    newArr.push(arr [i] * 2);
    }
}
return newArr;
console.log(arr, aficheChaqueValeur(arr));
````

|Avec ce code, il n'y aura qu'un seul endroit à vérifier, où à mettre à jour. Et qui va|
|influencé partout où cette fonction sera appelée.                                     |
|--------------------------------------------------------------------------------------|

//Fonction anonyme
------------------

:small_red_triangle: C'est une fonction qui ne porte pas de nom.

````js
let fn = function(){
return1;
};
fn();
console.log(fn()), fn());
````

|Peut importe le nombre de fois où elle est invoquée,  dans notre cas, elle affichera toujours 1.|
|------------------------------------------------------------------------------------------------|

//Fonction callback
-------------------
:small_red_triangle: C'est une fonction qui va être passée en argument, et qui va être invoquée par une autre fontion.

exemple:
````js
function maCallback(nb){
    return = nb * 3 ;
}
function maFoncgtion(nb : 6, maCallback);
    let var1 = nb * 16;
    return callback(var1);
}
let res = maFonction(nb: 6, maCallback);
function maCallback2(nb){
    return maCallback(nb: nb * 5);
}
console.log(res, maFonction(nb: 5, maCallback2));
````
// L'objet argument
-------------------
````js
function maFn(){
}
consle.log(arguments);
maFn(1, 2, 3, 4, 5, 6, "Jean", true, null);
````

|On peut ajouter un tableau dans notre(nos) argument(s), pour voir l'index, par ex.: |
|il peut y avoir aussi plusieurs arguments.                                          |
|------------------------------------------------------------------------------------|

````js
if(arguments[5] === "Jean"){
    alert('Vous êtes Jean');
}
````
Où une boucle
````js
for(let i = 0; 1 < arguments.length ; i++){
    console.log(arguments [i]);
}
````

// Déclarer un objet
--------------------

:small_red_triangle: JavaScript est basée sur des objets, on retrouve les objets presque partout en JavaScript.

Un objet est la représentation de données sous forme de clé valeur, donc chaque caractéristique est liée à un objet.

Objet = est appelé propriété

Et toutes les actions liées à un objet, est appelées "méthodes".

Les objets en JS peut-être mis en parallèle avec des objets réels.

exemple:
````js
{key : value, etc...}
````

| key = propriétés|
|-----------------|
````js
let phone = {},
marque :  'Samsung',
name : 'Galaxy Note',
largeur : 8 cm,
longueur : 16 cm,
````
|méthodes|
|--------|
````js
call : function(){
    console.log('call Sam');
}
send msg : function(to){
send msg : function('send sms to' + to);
},
console.log(phone);
console.log('marque:', phone, marque);
};

phone call();
phone send msg(to : 'Sam');
phone send msg(to : 'Tom');
phone send msg(to : 'Joe');

let phone2 {
    marque : 'Samsung',
};
console.log(phone2.largeur); indefined
où console.log(phone2.largeur, phone2 ['marque] );
````

// Accéder à l'objet courant dans une méthode (this)
---------------------------------------------
````js
let student = {
    name : 'Sam',
    class : 'Terminal',
    say : function (say){
        // Name : "BONJOUR"?
    console.log(this.name + ':' + say);
    }
};

student.say(say : "BONJOUR!);
 ````   


| this fait référence à l'objet courant |
| le point '.' est fait pour appeler    |
|---------------------------------------|

    
````js
let student2 ={
    name : 'Jean',
    classe : 'Seconde',
    say : function (say){
        Name : "BONJOUR"?

    console.log(this.name + ':' + say);

student.say(say : "BONJOUR");

student2.say(say : "BONJOUR + student.name);
    }
}
````

//Spread opérator
-----------------

spread = étalé
--------------

````js
let arr1 = ["Jean", "Sam", "Tom"];

let arr2 = ["Pauline", "Lise", "Eloise"];

let arr3 = [];

for(let i = 0; i < arr1.length; i++){

arr.push(arr1 [i]);
}
for(let i = 0; i < arr2.length; i++){

arr3.push(arr2 [i]);

console.log(arr3);
}
````
où, meilleur solution:
````js
let arr1 = ["Jean", "Sam", "Tom"];

let arr2 = ["Pauline", "Lise", "Eloise"];

let arr3 = [...arr1, ...arr2];
console.log(arr3);
````

 | /* Peut-être utilisé aussi aux objets non définis. */ |
 |-------------------------------------------------------|

 | /* On peut réécrire les propriétés. */                |
 |-------------------------------------------------------|
````js
let student default = {
    name :
    class :
};

let student = {
    ...student default,
};

let student = {...studentDefault, name ! "Sam"};

console.log(student name);

let student ={...studentDefault, presence = true};
````

 |/* Ne jamais copier un tableau car nous copierons la référence pas la valeur */|
 |-------------------------------------------------------------------------------|
````js
let arr1 = [1, 3];

let arr2 = [...arr1];

arr2.push(5);

console.log(arr1, arr2);
````

````js
let arr = [1];
let arr1 = [2];
let arr2 = [3];
let arr3 = [4];

let arr4 = [...arr, arr1, ...arr2, ...arr3];
console.log(arr4);
````

/* Si je rajoute des valeurs */
````js
let a, b, rest.
let arr = [1, "J", true, 'Bonjour'];
[a, b, ...rest] = arr;
console.log(a);
console.log(b);
console.log(rest);
````
/* Exemple avec constantes */
````js
const[a, b, c, ...rest] = [1, 2, 3, 4, 5];

console.log(a);
console.log(b);
console.log(c);
console.log(rest);
````
//Partie objet

/* On décompose par le nom de la propriété */
````js
let name;
({(name)} = {name ; "Jean"});
console.log('name' : name);

let name, age;
let student = {name : "Jean, age"};
({name, age}) = student;
console.log({'name, age' : name, age});
````

//Les méthodes de tableau à maîtriser
-------------------------------------

//.forEach()
//.find/.findIndex
//.map
//.reduc
//.filter
//.some()/.every

.forEach
--------

:small_red_triangle: Effectue une fonction pour chaque élément du tableau.

.find
-----

:small_red_triangle: Trouve un élément dans un tableauen fonction d'une condition en boulean, sauvegarde possible.

.findIndex
----------

:small_red_triangle: Va retrouver l'index de l'élément qui renvoie true.

.reduce
-------

:small_red_triangle: Permet de réduire un tableau à une certaine valeur où sous une autre forme.

.filter
-------

:small_red_triangle: Permet de filter les éléments d'un tableau selon une condition.

.some
-----

:small_red_triangle: Permet de retourner true , si une valeur correspond à une condition.

.every
------

:small_red_triangle: Permet de retourner true si toutes les valeurs respectent la(es) condition(s).

tableau avec valeur initial
---------------------------

.forEach
````js
let arr = [1, 4; 6, 8];
````

:small_red_triangle: Pour le traverser, on peut créer une boucle for, qui commence à 0, dans la limite et la 
largeur du tableau.On incrémente (i) à chaque tour et on utiiise (i) pour traverser le 
tableau.

````js
for(let i = 0; i < à arr.length, i++){
    arr [i];
}
````


:small_red_triangle: C'est une méthode pour pouvoir accèder à chaque valeur du tableau.
On l'a déjà vu dans la boucle for.

où
--
````js
let arr = [1, 4, 6, 8];
arr.forEach(function(value: number, index: number, array: number []){
console.log(index, value, array);
+});
````
:small_red_triangle: La méthode forEach ne retourne aucune données, 
exécute une fonction pour chaque élément de notre tableau.


.find
````js
let arr = ["Jean", "Sam", "Steve", "Tom",];
let callback = function(name, index){
    if(name.includes(searchElement: 'o')){
        return true;
    }else{
        returne false;
    }
};

let res =arr.find(callback);
console.log('res', res);
````
.findIndex

:small_red_triangle: La même chose que find mais celle-ci vous retourne aussi l'index.
Méthode beaucoup plus flexible au niveau de la donnée.

````js
let arr = ["Jean", "Sam", "Steve", "Tom",];
let callback = function(element, index){
};
    return element.length === 5;
let index = arr.findIndex(callback);
console.log(index, arr[index]);
````
.map

:small_red_triangle: tableau avec nombres:
````js
let arr = [1, 2, 4, 56, 78, 99, 123, 456];
callback = function(value, index){
    return value * 2;
};

let arr2 = arr.map(callback);
console.log(arr2);
````
:small_red_triangle: tableau avec objets

````js
let arr {
{age = 22};
{age = 17};
{age = 56};
};
let callback = function(value, index);
    console.log(value);
    return value.age;
};

let arr2 = arr.map(callback);
    console.log(arr2);
````
.reduce

:small_red_triangle: Je souhaite réduire tous les nombres en additionnant tous les nombres (accumulateur):
````js
let arr = [3, 4, 49, 230, 48, 123];
let accumulation = arr.reduce(function(acc; number,value: number, index: number){
    console.log(acc, value, index);
    acc = acc + value;
}, 0);
console.log(accumulation,arr);
````
.filter
````js
let arr = [3, 4, 49, 230, 48, 123];
let arrFilter = arr.filter (function(nb: number, index; number){
console.log(nb, index);
    if(nb % 2){
        return true;
    {else}
        retune false;
    }
});
console.log('array filer' : arrFiltred));
````
.some
````js
let arr = [3, 4, 49, 230, 48, 123];
let cond = arr.some(function(value; number, index; number){
console.log(value, index);
        return value > (choisir le nombre);
});
console.log(cond);
````
.every
````js
let cod = arr.every(value: number, index: number){
        return value > 0;
});
console.log('toutes les valeurs', cond);
````

//Déclarer une classe
---------------------
(modèles d'objets)
````js
- déclarer une classe

class User{
}
- déclarer une nouvelle instance

let user = new User();

- déclarer une autre nouvelle instance

let user = new User();
let user2 = new User();
User name = "Jean";
console.log(user, user2);
````
//Propriété par défault
-----------------------

assigné objet = instancié
````js
class user{
    name = "Jean";
    age = 22;
    presence = ;
}
let user = new User(); /*objet*/
let user2 = new User(); /*autre objet*/
    user name = "Sam";
console.log(user.name, user2.name);
````
//La fonction constructor
-------------------------

- Pour passer des paramétres lors de l'instentation (création d'un nouvel objet).
- Je ne suis pas obligé de la déclarer mais de l'insérer.
- Construction est réservé à la syntaxe de javascript.
- Pour personnaliser l'utilisateur(user) on ajoute 'this'.
- Propriété dynamique: qui changent en fonction des paramétres lors de la construction de l'objet.
````js
class user{
name = "Jean";
age = 22;
presence = ;
    constructor(name, age, presence: boolean = false){
        this.name = name;
        this.age = age;
        this.presence = presence;
    }
}
let user = new User(name : "Tom", 'age 27', présence : true); 
let user2 = new User(name : "Sam", 'age : 34');
````
//Ajouter des méthodes
----------------------

/*this fera référence à l'objet courant user*/
````js
class user{
name = "Jean";
age = 22;
presence = ;
    constructor(name, age, presence: boolean = false){
        this.name = name;
        this.age = age;
        this.presence = presence;
    isPresent(){
        returne.presence;
    }
    user.isPresent();
    user2.isPresent();
let user = new User(name : "Tom", age 27, présence : true);
let user2 = new User(name : "Sam", age : 34);
console.log(user, user.isPresent);
console.log(user, user2.isPresent);
````
//Héritage entre classe
-----------------------

- Class car extends va hérité des propriétés et des méthodes qui sont déclarés dans la class vehicle.

- Vehicle est considéré comme la classe parent lorsqu'on fait appel à constructor.

- super va appeler la méthode constructor.
````js
class vehicle
    constructor(name, wheels){
        this.name = name;
        this.wheels = wheels;
    }
    accelerate(){...}
    brake(){...}
    turn(){...}
class car{}
class motorBike{}
````
:small_red_triangle: Pour éviter de copier/coller on rajoute 'extends' pour hérité des propriétés 


//Propriétés et méthodes static
-------------------------------

- Elles ne sont pas partagés au niveau des instances.
- Elles sont dites "utilitaires".
- ex. static GRAVITY = 10;
- Les constantes sont en générale en majuscules pour être reconnues du premier coup d'oeil.
- On peut y accéder seuleument via la class.
- ex. static testTechnique(vehicle), elle prend véhicle en argument.
-  return vehicle.wheels > 0;
  
| math = random / round : les plus utilisés les autre servent généralement en 3D. |
|---------------------------------------------------------------------------------|

autre exemple
````js
class mathc{
    static, random(){
        return math.random();
    {
{
let math = new mathc();
    math random();
console.log(mathc.random);
````

### Sélectionner un évenement grâce à son 'id'
---------------------------------------------

fichier html
````html
/* <div id="sqr" classe= "square"></div>

<style>
    .square {
        width: 100px;
        height: 100px;
        background-color: red;
};
</style> */
````
### Sélectionner les évenements grâce à une(des) classe(s)
---------------------------------------------------------

documents.getElements(class Name: 'square');
console.log(squares);

### Sélectionner un où des éléments
----------------------------------

const square = document querySelector(selectors: '.square');
console.log(square);
id = sqr
/* Où grâce à son id */

(selectors: '#sqr');
 
/* Où */
(selectors: 'section');

/* Où */
querySelectorAll(selectors: 'div.square');
/* Pour plusieurs éléments (. pour recherher une classe dans un élément) # pour un id */

### Manipuler le style d'un élément
----------------------------------

/* écrire sur le js */

- il y a le rendu static et dynamique (voir exercices)

### Manipuler les classes d'un élément
-------------------------------------

- grossir puis rétrécir un objet

### Créer et ajouter au DOM un élément
-------------------------------------

- Pour ajouter du texte où pour ajouter du texte déjà existant. creatElement

- Si vous voulez insérer du texte html ; le navigateur va le considérer comme une chaîne de caractéres . Pour éviter cela : 
square.innerHTML = '<strong>Ok</strong>'
  il sera insérer comme un enfant de votre console.
  document.body.appenChils(square);
  console.log(square);
  
### Ecouter les événements du DOM
--------------------------------

addEventListener

### Function arrow
------------------

| NOTATION POUR ECRIRE UNE ARROW FONCTION |
| let arrow = ()=>{                       |
| };                                      |
|-----------------------------------------|

- Dans chaque fonction il y a un 'this'.
- 'This' est la référence dans lequel est éxécutée la fonction, que ce soit pour 
   une fonction classique où une fonction fléchée.
- le premier this fait référence à l'objet
- le deuxième fait référence à l'objet global
````js
funtion maFonction(nb){
    return 'ok'; 
    console.log('this');
}

let maFn = (nb) => {
    return nb;
    console.log('this);
},
maFonction(nb: 1); 
maFn(nb: 2);
````
/* Mes deux fonctions sont éxécutés dans la fonction globale(fenêtre). */

exemple :
````js
let user={
    name: 'Jean',
    notes: [12, 14, 15],
        get notes: function(){
            console.log(this); /* parent */
            this notes. forEach((notes: number) =>{
                console.log(this); /* enfant */
                    console.log(this.name + 'a eu' + notes);
        });
    }
}
````
/* 2ème méthode pour accéder aux objets */
````js
use.getNotes();
    console.log('For:', this);
    console.log(this.name + 'a eu' + notes);
    }, {name: "OK"};
````
/* Mais ne fonctionne pas pour toutes les méthodes */

| /n pour aller à la ligne |
|--------------------------|

###001. Javascript - Présentation </h3> : https://youtu.be/T73tbbSwqQM
---------------------------------------------------------------

Language D'interaction et d'animation le plus utilisé avec html et css.

###002. Javascript - Langage standardisé par ECMAScript : https://youtu.be/tdgLSZExer8
-------------------------------------------------------------------------------------

###003. Javascript - Polyfill : https://youtu.be/nkLicPD3rBc
---------------------------------------------------------
Internet explorer non compatible ? Utilisez un bout du code polyfill.

###004. Javascript - Transpiler : https://youtu.be/0_hFkfq0mO4
-----------------------------------------------------------
Est un type de compilateur, qui compile un autre code source en une autre langue de

programmation.

Babel et webpack sont des compilateurs, ex. traduire US2020 en US2015.


###005. Javascript - Les outils et framework en javascript : https://youtu.be/3VmGmzxblwM
--------------------------------------------------------------------------------------
Côté serveur : Node/js, réac, angular, trijs en 3D

###006. Javascript - Les boîtes ou projets qui utilisent Javascript : https://youtu.be/zNoyf3CqgMQ
-----------------------------------------------------------------------------------------------
     
###007. Javascript - Écrire du code javascript : https://youtu.be/bjMnN6o0euk
---------------------------------------------------------------------------
//Js
----
console.log('ok'));

Pour lier js à html:

| - Une ligne de code est une instruction, le point-virgule définit la fin de l'instruction,  | 
|   et qu'il peut passer à                                                                    | 
| - Un bloc de code est délimitée par une accolade ouvrante et fermante ; le bloc est tout ce | 
|   qu'il y a en dedans.                                                                      | 
| - les cases mémoires sont stockées pour être réutilisées.                                   | 
| - l'instructeur lit ligne par ligne.                                                        | 
| - *Nb. on ne peut pas déclarer 2 variables avec le même nom.                                | 
| - (* où / , etc,...) s'apellent des opérateurs.                                             |
|---------------------------------------------------------------------------------------------|


###008. Javascript - Instruction et bloc d'instructions : https://youtu.be/DT7rmUQAUzg
-----------------------------------------------------------------------------------     

###009. Javascript - Les variables et constantes : https://youtu.be/9iOxzsvDj74
----------------------------------------------------------------------------  
####Case mémoire
---------------

*variables
----------

peuvent changer de valeur en cours de script
let (pour déclarer une variable)
let= "défini";
let indéfini;
indéfini=22 (à ce niveau est défini)

*constantes
-----------

ne changent pas de valeur
const myConst= 12 ;
/* Exemple */
(indéfini) 2212 (myConst) ;
2212 ;
essayer avec/


###010. Javascript - Nommer vos variables : https://youtu.be/nBBynuhxn9A
--------------------------------------------------------------------- 
- règle n°1 ne pas reprendre les mots clés js.
- règle n°2 pas de majuscule au début.
- règle n°3 pas de chiffre au début (cela créer une erreur).
- règle n°4 nommer en rapport avec le contenu.
- règle n°5 pas trop long.

--nomenclature : lower case--

- camelCase, snakecase

###011. Javascript - Les types et le typage dynamique : https://youtu.be/Kxr1LC08wDM
--------------------------------------------------------------------------------- 
// Number
---------
let age = 12 ;

// String = chaîne de charactères
---------
let word = "Bonjour" ;

// Boolean
----------
let open = true ;
open = false ;

// Object
---------
const obj ={} ;

//Array
-------
console log.(typeof, age, age*3) ;

// Js
-----
console.log('ok');
let myName= "Jean";
myAge= 22;
const myConst= 12;
myAge * myConst;
myAge / myConst;

###012. Javascript - Opérateurs arithmétiques :  https://youtu.be/QAcwYlucAJc
-----------------------------------------------------------------------------

// L'addition
-------------

let add1 = 1;
let add2 = 2;
let somme = add1 + add2;
console.log('Addition', add1, add2, somme);

// La soustraction
------------------

let sub1 = 1;
let sub2 = 2;
let diff = sub1 - sub2;
console.log('Soustraction:', sub1, sub2, diff);

// La multiplication
--------------------

let multi1 = 1;
let multi2 = 2;
let produit = multi1 * multi2;
console.log('Multiplication:', multi1, multi2, produit);

// La division
--------------

let div1 = 1;
let div2 = 2;
let quotient = div1 / div2;
console.log('Division:', div1, div2, quotient);

// Puissance
------------

let puiss1 = 1;
let puiss2 = 2;
let result = puiss1 ** puiss2; //2^2
console.log('Puissance:', puiss1, puiss2, result);

// Incrémentation
-----------------

let inc = 0;
inc++;
console.log('Incrémentation1:', inc++);
/* vous renvoie avant l'incrémentation /
console.log('Incrémentation2:', ++inc);
/ vous renvoie après l'incrémentation */

// Décrémentation
-----------------

let dec = 0;
console.log('décrémentation1:', dec--);
/* vous renvoie avant la décrémentation */
console.log('décrémentation2:', --dec);
/* vous renvoie avant la décrémentation */

//Plus unaire
-------------

let unaire = "12"
console.log('Plus unaire:', +unaire);
/* + null 0, + false 0, + true 1 */


###013. Javascript - Opérateurs de comparaison : https://youtu.be/J3zkrd6Q338
-----------------------------------------------------------------------

//égalité simple ==
-------------------

let nb1 = 10;
let nb2 = 12;
console.log('égalité simple:', nb1==nb2);
/*où let nb2 = "10";  même si c'est une chaîne de caractères sa valeur est quand même égale à dix */

//Inégalité simple !=
---------------------

let nb3
let nb4
console.log('inégalité simple:', nb3 != nb4);
/* true s'affiche dans la console */'

//Egalité stricte ===
---------------------

let nb5 = 10;
let nb6 = 10;
/* donc true */
console.log('égalité stricte:', nb5 === nb6);
let nb5 = 10;
let nb6 = "10";
/* donc false /
/ contrairement à égalité simple égalité stricte ne traduit pas la chaîne de caractères donc "10" reste "10 */

//Inégalité stricte !==
-----------------------

let nb7 = 30;
let nb8 = 30;
console.log('inégalité strict:', nb7 !== nb8);

//Strictement supérieur >
-------------------------

let nb9 = 90;
let nb10 = 10;
console.log('strictement supérieur:', nb9 > nb10);

//Strictement inférieur <
-------------------------

let nb11 = 9;
let nb12 = 10;
console.log('strictement inférieur:', nb11 < nb12); /* false /
/ nb11 <= nb12 true /
/ let nb11 = 9;
let nb12 = 10;
(nb11 < nb12); true
*/

###014. Javascript - Opérateurs logiques : https://youtu.be/1jos0nCvsHw
--------------------------------------------------------------------

//Et logique
------------

let nbr1 = 2;
let nbr2 = 2;
let disabled = false;
let loading = true;
let name = "Hayete";
console.log('et logique:', nbr1 === nbr2 && name === "Hayete");
/* on peut ajouter autant d'expressions en rajoutant 2 esperluettes && /
/ si on change 2 en "2" , ce sera false car la chaîne de caractères a changé && 2 == "2"); true */

//Ou logique
------------

let price = 499;
let adult = true;
let city = "Paris";
console.log('ou logique:', price > 300 || adult || city === 'Paris');

//Non logique
-------------

let nullVar = null;
let indefinedVar =
let prod = false;
let count = 0;
let str = "";
console.log('non logique:', !nullVar, !indefinedVar, !prod, !count, !str);
/* !! str (permet d'écrire simplement au lieu de : str! = null && str ==undefined && str !="" */

//Mixed
-------

let nbr3 = 3;
let nbr4 = "4";
let shown = true;
console.log('mixed;', (true && true) || true);


###015. Javascript - Opérateurs d'affectation : https://youtu.be/i6b6GuyUyDQ
-------------------------------------------------------------------------

let nbs =1050;

//Affectation simple
--------------------

nb = 30;

//Affectation après addition
----------------------------

nb += 3;

//Affectation après soustraction
--------------------------------

nb -=  5; /* équivalent //nb = nb-5 */

//Affectation après multiplication
----------------------------------

const nb2 = 2;
nb = nb2;  / équivalent nb = nb *nb2 */

//Affectation après division
----------------------------

nb /= 3; /* nb = nb/3 */
nb /= 3;

//Affectation du reste
----------------------

nb %= 4; /* nb = nb % 4 /
/ % modulo permet de retourner une division euclédienne */



###016. Javascript - Strings : https://youtu.be/jrpNbWHTAzA
-------------------------------------------------------- 
CHAINES DE CARACTERES (stull)

//Syntax
--------

let str = 'mon text1'; /simple côte/
str = "mon text2"; /double guillemets/
str = mon text3; /* bcktique ? */

###017. Javascript - Strings : Où se documenter pour manipuler les strings => https://youtu.be/jrpNbWHTAzA
--------------------------------------------------------------------------------------------------------     

###018. Javascript - Strings : La longueur d'une string => https://youtu.be/xOM8SYe7gv0
------------------------------------------------------------------------------------   


//Echapper les caractères
-------------------------

str = 'C'est parti';
/* l'instructeur va penser que la chaîne de caractères s'arrête après le c,
donc pour éviter cela on rajoute un backslash \ */
str = "C'est "mardi";
str = "du texte avec un backslash\";
str = "du texte avec un /!\Attention/!\";
console.log(échappement: ', str');
str = "mon texte /n/n/n/r avec des lignes";
console.log(str);
/* avec /n met à la ligne, ici 3 fois dans l'exemple,/r retour chariot (espace en +) */

//Concaténation
---------------

/* ayant au moins 2 chaînes de caractères. */
let str2 = "Ma seconde de caractères";
console.log(str + str2 +/n/n texte3);
/* str (déjà existant) str2 (crée hors console) et texte3 créer dans console) */



###019. Javascript - Strings : Mettre en majuscule => https://youtu.be/suIn7mWKm7Y
------------------------------------------------------------------------------- 
(toUpperCase)
-------------

console str = "mon texte en minuscule";
console.log(str.toUpperCase(), "en minuscule".toUpperCase());
/* les parethèses servent à invoquer avec cette méthode. /
/ soit on invoque ex.str où directement le texte /
/ si on met les caractères en majuscules elles ne changent pas de valeur */


###020. Javascript - Strings : Mettre en minuscule => https://youtu.be/kDY4A9ExB9s
-------------------------------------------------------------------------------
(toLowerCase)
-------------

const str = "MAJUSCULE";
console.log(str.toLowerCase|)|;

###021. Javascript - Strings : Accéder à un caractère grâce à sa position => https://youtu.be/Ng2QAU7XnQ4
------------------------------------------------------------------------------------------------------

const str = "Jean";
console.log(str[0], str[3], str.str[lenght1]);
/* 0 = J de Jean et non 1 comme on pourrait le penser! */
/* 3 qui correspond à la lettre n car je connais la longueur du caractère, sinon j'utilise la 3ème méthode
pour le savoir en manière dynamique */


###022. Javascript - Strings : La position d'une string dans une string => https://youtu.be/hI9x9QoT8R8
----------------------------------------------------------------------------------------------------   
(indexOf)
---------

const str = "Bonjour Jean";
const search = "Jean";
console.log(str,search,str.indexOf(search, str.indexOf)("Sam"));
/* si la chaîne n'est pas présente ex.Sam on aura -1 dans la console */
Commence par une accolade et se termine par une accolade {}
-->pour déclarer une valeur let(var est encore utilisé mais déconseillé)
-->pour assigner une valeur

###023. Javascript - Array : Présentation : https://youtu.be/z4wDgRIKLXU
---------------------------------------------------------------------

Un tableau est une structure de données qui permet d'organiser des valeurs numérotés.
Il peut contenir tout tpe d'objets y compris d'autres tableaux.
Index = position dans le tableau, en javascript les valeurs commencent toujours par 0.
Il est préférable de stocker des valeurs qui ont le même type, où la même forme d'objets.
Voir documentation Mozilla.


###024. Javascript - Array : Créer un tableau : https://youtu.be/DbP41frBryo
-------------------------------------------------------------------------

const arr = [1, 2, 3]; /*pour sauvegarder dans une constante*/
console.log(arr);
console.log(arr, arr.length); / pour afficher la longueur du tableau */
/* exemple de tableaux avec différents objets */
const arr = ["Jean", null, 1, true, [2, "1", []]];
/* autre façon d'écrire un tableau */
const arr2 = new array(arr.length: 1);
new = nouveau mot clé, le reste est un tableau vide.
cosnt arr2 = new array(array.lenght.10).fill(valeur 10);
/* nous l'avons rempli /
/ où /
arr.fill(value.true);
/ pour créer un tableau et y insérer toutes les valeurs [1, 2, 3]
les espaces ne sont pas compter comme un caractère, Jean pense que c'est mieux de le faire. /
/ petite astuce, pour éviter de répéter X fois dans le code faire par exemple */
const arr2 = new array(array length: 50);
arr 2(value: 50);
/* où */
const arr2 = new array(items1, 2, "Jean");

###025. Javascript - Array : Accéder à une valeur grâce à son index : https://youtu.be/uUus0kBtNBA
----------------------------------------------------------------------------------------------- 
index = position de la valeur
const arr = ["Sam", "Jean"];
/* Sam = 0, Jean = 1 /
/ on utilise length si on a besoin de connaître une valeur (la place exact d'un élément (position)) */
console.log(arr[4-1]);
/* ceci est un exemple d'écriture en dur, non dynamique */

###026. Javascript - Array : Insérer une valeur dans le tableau : https://youtu.be/w_x4kFIIbig
------------------------------------------------------------------------------------------- 
const products = [les différents prix];
/* le s de products est important car il y a souvent plusieurs /
(click)
products.push(600);
(click)
products.push(400);
/ sauvegarde les précédentes données cliquer par l'utilisateur /
/ utilisez des données avec le même type sinon faire d'autres tableaux */
const name = [];
names.push("Jean");
names.push("Sam");
console.log(products, names);

###027. Javascript - Array : Supprimer ou remplacer une valeur avec la méthode " .splice() " : https://youtu.be/fUwqwxStLlw
------------------------------------------------------------------------------------------------------------------------     
const arr = [];
arr.push("Jean", "Sam", "Steve"); /* insérer des données */
arr.splice(star; donner la position en nbr, deletecount; donner le nbr);
console.log(arr);
arr.splice(start1, delete(count:1, items: "Jean2"));

###028. Javascript - Les conditions : if... else : https://youtu.be/7UxNwWb9wYQ
----------------------------------------------------------------------------  
/*console.log('if...else');
cons age = 19;
if(age > 18){
console.log('Vous êtes majeur ?');
}else
console.log('Vous êtes mineur ?');
}
*/
/* else if(age )= 13 && age <18){
console.log('Vous êtes un adolescent');
{else{
console.log('vous êtes mineur');
}
*/

| /* if=si {}=alors else=sinon ||=où &&=et */ |
|---------------------------------------------|
/* exemple:
let name = "Jean";
if(name === "Jean){
console.log('Vous êtes Jean');
}else{
console.log('Je ne vous connais pas');
}


|/* réaction/contre-réaction(s)|
|------------------------------|


/* exemple:
let name = "Steve";
if(name = "Jean" || name === "Sam"){
console.log('Vous êtes' + name + '!');
}else{
console.log('Je ne vous connais pas!');
}

|/* Si la première condition est réuni, les autres ne seront pas exécutés(si d'autres il y a) */|
|-----------------------------------------------------------------------------------------------|



###029. Javascript - Les conditions : Le ternaire : https://youtu.be/IvnGha-01mI
-----------------------------------------------------------------------------     
si la variable "présence est true"; 

alors j'affiche "Bonjour" dans la console;

sinon j'affiche "Bye";

let présence = true;
if(présence === true){
console.log("Bonjour);
}else}
conole.log("Bye);

| /* raccourci en 1 seule ligne */                     |
| console.log(présence === true) ? "Bonjour" : "Bye"); |
|-condition: si vrai si fausse                         |
|------------------------------------------------------|


- dans le ternaire :on est obligé d'utiliser les ()

###030. Javascript - Les conditions : Switch => https://youtu.be/2qDKA7RYWiE
---------------------------------------------------------------------------     


1er exemple
````js
let fruits = 'orange';
if(fruits === 'pommes'){
console.log('les pommes valent 1 € le kilo');
}else if(fruits === 'fraises){
console.log('les fraises valent 2 € le kilo');
}else if(fruits === 'bananes' || 'cerise'){
console.log('les bananes et les cerises coutent 3 €');
}else{('vous n'avez aucun fruits')
}
````
2ème exemple différent
````js
switch(fruits){
case "pommes";
console.log('les pommes valent 1 €');
break;
case "fraises";
console.log('les fraises valent 2 €');
break;
case "bananes";
case "cerises";
console.log('les bananes et les cerises coutent 3 €');
break;
default;
console.log('vous n'avez aucun fruits');
break;
}
````

| /* - Entre les instructions il faut mettre des breaks et finir par un break.       |
|    - Un rope est une portée d'accessibilité à la variable qui est définiedans le   |
|      blocpar des accollades. ex.console.log(message);                              |
|    - Si elle répéter plusieurs fois, et pour éviter les messages d'erreurs, on     |
|------------------------------------------------------------------------------------|


- ajouter des accollades dans les accollades pour les délimiter.
----------------------------------------------------------------

###031. Javascript - Les boucles : La boucle for => https://youtu.be/ErsQvQFha_k
-----------------------------------------------------------------------------
| /* for ([expression intitial]; [conditions]; [expression incrémentente où décrementente])  |
| {                                                                                          |
| instructions                                                                               |
|}  */                                                                                       |
|--------------------------------------------------------------------------------------------|



boucle        =>for
commencement  => let i = 0
condition     => i < 10
fin de boucle => i++

for(let i = 0; i >=0; i++){
console.log(i);
}
/* - i = 0, i = 1, etc...jusqu'à 10 */
for(let i = 10; i<=0; i--){
console.log(i);
}
/*  console.log('fin de la boucle'); /
/ on peut mettre une condition ex.if(i === 5){
break.
{  (donc nous avons cassé la boucle) */


###032. Javascript - Les boucles : L'instruction do... while => https://youtu.be/7bt1FiJi2dI
-----------------------------------------------------------------------------------------  
| faire tant que|
|---------------|

do{
instructions
}while (conditions)
let i 100;
do{
console.log'i);
i++;
}while(i < 10);
/* on peut rajouter une condition /
if(i === 5){
break;
}
/ attention à la boucle infini, elle risque de crascher l'ordinateur */


###033. Javascript - Les boucles : L'instruction while => https://youtu.be/ks82fA-XRS8
-----------------------------------------------------------------------------------
while(conditions){
instructions       
}


|/* itération = chaque fin de répétition */|
|------------------------------------------|

###034. Javascript - Les fonctions : Déclarer une fonction : https://youtu.be/-yoh_wnH2vI
--------------------------------------------------------------------------------------     

**Une fonction est un ensemble d'instructions, effectuant une tâche où calculant une valeur.**
**Une fonction peut retourner des données où non.**
**Pour utiliser une fonction, il est nécessaire de l'avoir définie, déclarer avant de l'utiliser.**

###035. Javascript - Les fonctions : Fonction anonyme : https://youtu.be/04rcYrOXECQ
---------------------------------------------------------------------------------     
c'est une fonction qui n'a pas de nom

###036. Javascript - Les fonctions : Fonction callback : https://youtu.be/mBnm6Q0YL8A
----------------------------------------------------------------------------------     
c'est une fonction qui appelle une autre fonction

###037. Javascript - Les fonctions : l'objet arguments : https://youtu.be/JAqmTzcQSq0
----------------------------------------------------------------------------------     
function myFn(){
  console.log(arguments);
}
myFn(1, 2, 3, 'Jean", true, ,null);
/*j'aurais pu mettre les arguments dans les parenthèses après myFn, mais c'est mieux de le faire séparément*/

###038. Javascript - Les objets : Déclarer un objet : https://youtu.be/1LdsB9NQAMQ
-------------------------------------------------------------------------------     
- Un objet est la représentation de données sous la forme de clé/valeur(key/value)
- Chaque(s) caractéristique(s) liée(s) à un objet est une propriétée(s)
- Toute(s) action(s) liée(s) à un objet est une méthode

###039. Javascript - Les objets : Accéder à l’objet courant dans une méthode : https://youtu.be/tx6Nlq9nZ5M
--------------------------------------------------------------------------------------------------------     
On utilise le this
###040. Javascript - Spread operator : https://youtu.be/eikmsLdPcoI
----------------------------------------------------------------     

###041. Javascript - Affecter par décomposition : https://youtu.be/haqPpV-R2dk
---------------------------------------------------------------------------     

###042. Javascript - Manipuler les tableaux : https://youtu.be/_5t58-u14pM
------------------------------------------------------------------------     

###043. Javascript - Manipuler les tableaux : La méthode "forEach" : https://youtu.be/KtMLePnFIKM
----------------------------------------------------------------------------------------------     

###044. Javascript - Manipuler les tableaux : La méthode " find" : https://youtu.be/3D08bhbL4Yg
--------------------------------------------------------------------------------------------     

###045. Javascript - Manipuler les tableaux : La méthode "findIndex" : https://youtu.be/alAGmFFJr90
------------------------------------------------------------------------------------------------     

###046. Javascript - Manipuler les tableaux : La méthode "map" : https://youtu.be/x3oKWsFza3I
------------------------------------------------------------------------------------------     

###047. Javascript - Manipuler les tableaux : La méthode "reduce" : https://youtu.be/L889udBc6xY
---------------------------------------------------------------------------------------------     

###048. Javascript - Manipuler les tableaux : La méthode "filter" : https://youtu.be/Ron3mEbSOjc
---------------------------------------------------------------------------------------------     

###049. Javascript - Manipuler les tableaux : La méthode "some" : https://youtu.be/L-6jocjp_h8
-------------------------------------------------------------------------------------------     

###050. Javascript - Manipuler les tableaux : La méthode "every" : https://youtu.be/bSpD20IJlf4
--------------------------------------------------------------------------------------------     

###051. Javascript - Les classes : Déclarer une classe : https://youtu.be/ACKxowbFEQw
----------------------------------------------------------------------------------     

###052. Javascript - Les classes : Propriété par défaut : https://youtu.be/9cMGW5PhXEw
-----------------------------------------------------------------------------------     

###053. Javascript - Les classes : La fonction "constructor()" : https://youtu.be/6TPIriZoxMQ
---------------------------------------------------------------------------------------------     

###054. Javascript - Les classes : Ajouter des méthodes : https://youtu.be/UPuWSSqxo6A
--------------------------------------------------------------------------------------     

###055. Javascript - Les classes : Héritage entre les classes : https://youtu.be/l_U6hLp07Xs
--------------------------------------------------------------------------------------------     

###056. Javascript - Les classes : Propriétés et méthodes "static" : https://youtu.be/PiKnvUTGE1A
-------------------------------------------------------------------------------------------------     

###057. Javascript - Manipuler le dom : S'assurer que le dom est chargé : https://youtu.be/EePywdhU1fk
------------------------------------------------------------------------------------------------------ 
- s'assurer que le DOM est chargé : créer une page html et une page JS puis les lier .
````html
- <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Javascript</title>

</head>
<body>

<h1>Javascript</h1>

    <script type= "text/javascript"></script>
        console.log('2');
    <script src= "index-2.js"></script>
</body>
</html>
````

- Mon navigateur lorsqu'il récupère ma page html, va éxécuter chaque ligne les unes aprés
  les autres.

- Tout ce qui se trouve dans la partie 'head' , va ralentir l'interprétation pour
  l'utilisateur donc le script se trouvera juste au dessus de la balise fermant 'body'.

- Dans le cas où le script se trouve dans 'head' et pour être sûre que le DOM est chargé :

<h1>javascript</h1>
    console.log(window);
    window = onload = 'function()'}
    console.log('4');
    console.log document.querySelector(selectors: 'h1')

- Dans le cas où il faudrait atteindre un sous-dossier par exemple :
<script type="src/app.js"></script>

###058. Javascript - Manipuler le dom : Sélectionner un élément grâce à son id : https://youtu.be/H1OO9ssjVqs
-------------------------------------------------------------------------------------------------------------     

###059. Javascript - Manipuler le dom : Sélectionner des éléments grâce à leurs classes : https://youtu.be/esFTGa7WLIU
----------------------------------------------------------------------------------------------------------------------    

###060. Javascript - Manipuler le dom : Sélectionner un ou des éléments : https://youtu.be/MYdFSsFooeM
------------------------------------------------------------------------------------------------------     

###061. Javascript - Manipuler le dom : Manipuler le style d'un élément : https://youtu.be/H_Lq125g42I
------------------------------------------------------------------------------------------------------     

###062. Javascript - Manipuler le dom : Manipuler les classes d'un élément : https://youtu.be/b5Th1rw_73Y
---------------------------------------------------------------------------------------------------------    

###063. Javascript - Manipuler le dom : Créer et ajouter au dom un élément : https://youtu.be/BQjKZZ2QtpQ
---------------------------------------------------------------------------------------------------------     

###064. Javascript - Manipuler le dom : Écouter les évènements du dom : https://youtu.be/X3HvOQx-hBQ
---------------------------------------------------------------------------------------------------     

###065. Javascript - Function arrow : https://youtu.be/sfGKjJUf6KA
------------------------------------------------------------------

Objets globaux
Cette partie référence tous les objets natifs standards JavaScript, avec leurs propriétés et méthodes.

Le terme « objets globaux » (ou objets natifs standards) ne doit pas ici être confondu avec l'objet global. Ici, « objets globaux » se réfère aux objets de portée globale. L'objet global lui-même peut être accédé en utilisant this dans la portée globale (uniquement lorsque le mode strict n'est pas utilisé, sinon, il renvoie undefined). En réalité, la portée globale consiste des propriétés de l'objet global (avec ses propriétés héritées, s'il en a).

Note : En mode strict, la portée globale représentée par this sera undefined.

Les autres objets de la portée globale sont créés par les scripts utilisateurs ou fournis par l'application hôte dans laquelle s'exécute JavaScript. Les objets mis à disposition par les navigateurs web sont documentés dans la référence API. Pour plus d'informations sur la distinction entre le DOM et JavaScript, voir l'aperçu des technologies JavaScript.

Objets globaux standards (par catégorie)
Propriétés - valeurs
Les propriétés globales renvoient une valeur simple, elles ne possèdent aucune propriété ou méthode :

Infinity
NaN
undefined
le littéral null
globalThis
Propriétés - fonctions
Les fonctions globales, appelées globalement (et non par rapport à un objet), renvoient directement leur résultat à l'objet appelant.

eval()
uneval()
isFinite()
isNaN()
parseFloat()
parseInt()
decodeURI()
decodeURIComponent()
encodeURI()
encodeURIComponent()
escape()
unescape()
Objets fondamentaux
Ces objets sont les objets fondamentaux de JavaScript. Parmi ces objets, on retrouve les objets génériques, les fonctions et les erreurs.

Object
Function
Boolean
Symbol
Error
EvalError
InternalError
RangeError
ReferenceError
StopIteration
SyntaxError
TypeError
URIError
Nombres et dates
Ces objets permettent de manipuler les nombres, dates et calculs mathématiques.

Number
BigInt
Math
Date
Manipulation de textes
Ces objets permettent de manipuler des chaînes de caractères.

String
RegExp
Collections indexées
Ces objets sont des collections ordonnées par un index. Cela inclut les tableaux (typés) et les objets semblables aux tableaux.

Array
Int8Array
Uint8Array
Uint8ClampedArray
Int16Array
Uint16Array
Int32Array
Uint32Array
Float32Array
Float64Array
BigInt64Array
BigUint64Array
Collections avec clefs
Ces objets représentent des collections d'objets avec clefs. Ils contiennent des éléments itérables, dans leur ordre d'insertion.

Map
Set
WeakMap
WeakSet
Données structurées
Ces objets permettent de représenter et de manipuler des tampons de données (buffers) et des données utilisant la notation JSON (JavaScript Object Notation).

ArrayBuffer
SharedArrayBuffer
Atomics
DataView
JSON
Objets de contrôle d'abstraction
Promise
Generator
GeneratorFunction
AsyncFunction
Introspection
Reflect
Proxy
Internationalisation
Ces objets ont été ajoutés à ECMAScript pour des traitements dépendants de particularités linguistiques. Ils possèdent leur propre spécification.

Intl
Intl.Collator
Intl.DateTimeFormat
Intl.ListFormat (en-US)
Intl.NumberFormat
Intl.PluralRules (en-US)
Intl.RelativeTimeFormat
Intl.Locale
WebAssembly
WebAssembly
WebAssembly.Module
WebAssembly.Instance
WebAssembly.Memory
WebAssembly.Table
WebAssembly.CompileError
WebAssembly.LinkError
WebAssembly.RuntimeError
Autres
arguments
