~~~~![img_65.png](img_65.png)

![img_66.png](img_66.png)

![img_67.png](img_67.png)

![img_68.png](img_68.png)

IL MANQUE let ET const JE NE SAIS PAS POURQUOI JE NE LES RETROUVE NUL PART !
---------------------------------------------------------------------------
DÉFINITION DES BALISES HTML5
----------------------------
Balise	Description
-------------------
<!--...-->	Pour un commentaire
<!DOCTYPE>	L'inclusion du DOCTYPE dans un document HTML assure que le navigateur interprétera la version HTML ainsi déclarée.
En HTML5 le doctype à déclarer est : <!DOCTYPE html>.

<a>	Utilisée pour les hyperliens. Attention : l'attribut "name" n'existe plus pour les liens.
Sequential link types (déclaration : "rel")
Link types
attributs

<abbr>	Pour une abréviation. Son attribut "title" ne doit être utilisé que pour l'extension de l'abréviation et rien d'autre.
attributs
exemple

<acronym>	Non utilisé

<address>	Généralement utilisée dans le conteneur <footer>, cette balise est utilisée à la place de la balise <p>,
les informations nécessaires pour contacter la ou les auteurs du document cité. Cela peut-être son nom ou un label suivi 
de son adresse email par-exemple ou adresse postale. Confusion possible : on utilisera la balise <p> pour donner
l'adresse postale d'une entreprise sans relation avec l'auteur du document s'y rapportant.
attributs
exemple

<applet>	Non utilisé

<area>	Définie une zone ou plusieurs zones pour une carte image. Cette balise s'utilise toujours avec la balise <map>.
attributs

<article>	Utilisé pour du contenu ayant son propre sens indépendamment du reste des autres éléments de la page,
ce contenu est distribuable et réutilisable. Cela peut-être un billet de forum, un article de journal, un gadget,
un commentaire d'utilisateur... La balise <article> peut avoir son propre header et footer. Confusion possible avec la 
balise <section> qui regroupe des éléments de thématique identique.
attributs
exemple

<aside>	Balise de structure supposant avoir un titre de type <h1>. Cette balise permet de regrouper des informations non
essentielles relatives au site Web. Lorsque la balise <aside> se trouve dans un article, son contenu se réfère à 
l'article lui même et non au site Web (par exemple des notes de pages, un glossaire ou tout élément relatifs à l'article).
Confusion : La sidebar d'une page n'a pas pour obligation d'être dans une balise <aside>.
attributs

<audio>	Pour lire un fichier audio.
attributs
exemple

<b>	Utilisé dans un paragraphe pour une partie de texte en gras, sémantiquement faible, on lui préférera la balise
<strong> pour donner de l'importance au texte.
attributs

<base>	Cette balise permet de définir l'URL de base (l'utilisation de cette balise est parfois controversée).
attributs

<basefont>	Non utilisé . (On peut tout de même encore la trouver dans l'intégration HTML4 de newsletters pour déclarer
des polices.)

<bdo>	Utilisé pour la direction d'un texte. Attention, l'attribut "dir" à une valeur sémantique différente pour cette
balise.
attributs

<bdi>	Utilisé pour la direction d'un texte.Attention, l'attribut "dir" à une valeur sémantique différente pour cette 
balise.
attributs

<big>	Non utilisé

<blockquote>	Pour une longue citation venant d'une source externe, l'adresse de la source peut-être indiquée dans
l'attribut "cite". Dans le cas d'un forum de discussion ou d'un blog, un utilisateur peut utiliser cette balise pour
citer le commentaire d'un autre utilisateur. Nous n'utiliserons pas cette balise pour un dialogue. Confusion possible
avec la balise <q>.
attributs
exemple

<body>	Partie principale du document HTML.
attributs

<br>	Utilisée dans un paragraphe, cette balise permet de créer un saut de ligne.
attributs

<button>	Pour un bouton. il peut être utilisé comme commande.
attributs

<canvas>	Pour créer un graphique avec la possibilité de l'animer avec du JavaScript.
attributs
exemple

<caption>	Pour le titre d'un tableau. Note : En HTML5, l'attribut "summary" de la balise "table' n'est plus valide et 
ne doit pas être utilisé.
attributs
exemple

<center>	Non utilisé

<cite>	Utilisée pour le titre d'une oeuvre, d'un document ou d'un évènement. Cette balise peut-être peut-être utilisée
conjointement avec la balise <q>. Cette balise ne peut être utilisée pour une personne.
attributs
exemple

<code>	Pour déclarer du code informatique. On peut l'utiliser à l'intérieur de la balise <pre> pour du texte pré
formaté. On recommande d'utiliser une classe avec le nom du langage informatique cité dans cette balise. On peut ajouter
un classe pour décrire le type de langage utilisé dans la balise exemple : <code class="language-javascript">.
Confusion possible avec la balise <samp>.
attributs
exemple

<col>	Utilisé pour créer des colonnes dans un tableau.
attributs

<colgroup>	Utilisé pour créer des groupes de colonnes dans un tableau.
attributs

<command>	Cette balise est utilisée comme un bouton, un radiobutton, ou un checkbox. Elle ne peut être visible qu'à
l'intérieur de la balise <menu>, dans le cas contraire, elle ne sera pas visible et utilisée comme raccourcis clavier.
Attention, l'attribut "title" à une valeur sémantique différente pour cette balise.
attributs

<datalist>	Pour une liste déroulante.
attributs

<dd>	Pour la description d'une définition, s'utilise avec la balise <dl>.
attributs

<del>	Utilisé pour indiquer qu'une partie de texte est supprimée, mais conservée pour en garder la trace, on la
stylisera en CSS. On peut utiliser cette balise conjointement avec la balise <ins> pour indiquer le nouveau texte qui
est à prendre en compte. Confusion possible avec la balise <s>qui indique qu'un texte n'est pas correct ou non pertinent.
attributs

<details>	Pour apporter des détails sur Widget, il peut être utilisé pour cacher/afficher des informations
complémentaires. Il peut être le conteneur de la balise <summary>.
attributs
exemple

<dfn>	Représente le terme d'une définition, on l'utilise dans un paragraphe ou une liste de définitions. On peut 
utiliser cette balise conjointement avec <abbr>. Dans la même section, il est possible de créer un lien pointant sur
cette définition, utile pour la création d'infobulle par exemple.
attributs
exemple

<dir>	Non utilisé

<div>	Balise sans valeur sémantique réelle, elle sert de conteneur pour une mise en forme en CSS. Cette balise doit
être utilisée en dernier recours, lorsqu'aucune autre balise ne peut convenir.
attributs

<dl>	Pour une liste de définitions. Confusion possible : cette balise ne peut être utilisée pour créer des dialogues,
les balises <p> restant appropriées.
attributs

<dt>	Pour déclarer une définition, s'utilise avec la balise <dl>.
attributs

<em>	Utilisé dans un paragraphe pour mettre une partie de texte en emphase.
attributs

<embed>	Utilisé pour du contenu externe et interactif ou pour un plug-in.
attributs
exemple

<fieldset>	Pour regrouper des éléments d'un formulaire.
attributs

<figcaption>	Utilisée dans le conteneur <figure>, avant ou après le contenu, cette balise permet d'écrire une légende
ou une description.
attributs
exemple

<figure>	Utilisée pour regrouper un ou plusieurs médias ( illustrations, diagrammes, exemples de code, photos...)
attachés au document sans pour autant en suivre le flux. Les images s'y trouvant n'ont pas obligation d'être inséré dans
une balise <p> et dans le cas d'une liste d'images, on peut omettre les listes à puce. On utilisera la balise figcaption
pour décrire les médias utilisés. Confusion : cette balise ne doit être utilisée dès qu'il y a une image dans un article,
on peut aussi la confondre avec la balise <aside>.
attributs
exemple

<font>	Non utilisé

<footer>	Regroupe des informations de bas de page dans une section ou un article. Cette balise permet d'ajouter des
liens de navigation sans utilisation de la balise <nav>.
attributs
exemple

<form>	Pour un formulaire.
attributs

<frame>	Non utilisé

<frameset>	Non utilisé

<h1> à <h6>	Utilisés pour la hiérarchisation des titres.
attributs

<head>	Pour les informations d'en tête du document HTML.
attributs

<header>	Pour l'entête d'une section et/ou d'une page, cette balise est utile pour une introduction et/ou des éléments
de navigation. Cette balise peut-être utilisée dans la balise <section> et dans la balise <article>.
attributs
exemple

<hgroup>	Non utilisé - (Regroupe des titres de niveau Hn dans la balise <head> à noter : cette balise a été supprimée
puis réintégrée puis re-supprimée).
attributs

<hr>	Permets de créer une ligne de séparation.
attributs

<html>	Déclaration du document HTML. On lui rajoutera l'attribut lang pour déclarer la langue utilisée.
attributs

<i>	Utilisée dans un paragraphe pour indiquer qu'une partie du texte est différent du reste. Il ne faut pas utiliser
cette balise lorsque <b>, <cite>,<dfn>,<em>,<q>,<small> ou <strong> peuvent être utilisées. Note : Certains analyseurs
de site nous disent que cette balise est obsolète, ce qui est totalement faux.
attributs

<iframe>	Pour créer une sous-fenêtre.
attributs

<img>	Pour déclarer une image.
attributs

<input>	Pour un champ de texte.
attributs

<ins>	Pour insérer un nouveau texte dans un document révisé. On peut utiliser cette balise avec <del> qui permet
d'indiquer qu'une portion de texte n'est plus valable.
attributs

<keygen>	L'élément keygen représente un contrôle de générateur de clé stockée dans "keystore local".
attributs

<kbd>	Lorsque le texte est utilisé pour définir des actions au clavier.
attributs

<label>	Étiquette utilisée comme titre d'une commande.
attributs

<legend>	Titre du fieldset auquel il se rapporte.
attributs

<li>	Item d'une liste ordonnée ou à puce.
attributs

<link>	Permets de lier une ressource externe à la page HTML. Attention l'attribut "title" à une valeur sémantique
différente.
attributs

<main>	Pour déclarer "LE" contenu important d’une page HTML, cette balise ne peut donc être utilisée qu’une fois par
page -> ce qui veut dire que le header principal ainsi que le footer principal sont des parties différentes. 
Les balises <article> , <aside> , <footer> , <header> ou <nav> ne peuvent pas contenir la balise <main>, par contre,
on peut mettre ces balises à l'intérieur de <main>, là, il n'y a pas de soucis.
exemple

<map>	Utilisée pour créer une carte d'image avec des zones réactives. L'attribut "name" est obligatoirement requis.
attributs

<mark>	Pour marquer du texte, par exemple surligner un résultat de recherche. On stylisera cette balise en CSS.
attributs

<menu>	Pour une liste de commande. Confusion possible avec la balise <nav>.
attributs

<meta>	Permets d'ajouter des métas à la page HTML.
attributs

<meter>	Pour les mesures.
attributs

<nav>	Pour regrouper des liens qu'ils soient internes à la page ou pour des pages liées. Il est recommandé, mais non
obligatoire d'utiliser les listes à puce pour lister les liens.
attributs
exemple

<noframes>	Non utilisé

<noscript>	Utilisée pour indiquer un message dans le cas où JavaScript serait désactivé.
attributs

<object>	Pour déclarer un objet.
attributs

<ol>	Utilisé pour les listes ordonnées.
attributs

<optgroup>	Pour grouper des informations dans une liste déroulante.
attributs

<option>	Pour déclarer un item dans une liste déroulante. Peut-être utilisé comme "commands".
attributs

<output>	Représente le résultat d'un calcul.
attributs

<p>	Définis un paragraphe contenant une ou plusieurs phrases. Cette balise ne doit pas être utilisée si une autre
balise est mieux indiquée.
attributs

<param>	Pour paramétrer un objet.
attributs

<pre>	Ecrire un texte préformaté. L'utilisation de la balise <p> n'est pas obligatoire. Peut-être lui même le conteneur
de la balise <code>.
attributs
exemple

<progress>	Pour une barre de progression.
attributs

<q>	Utilisée pour une citation courte provenant d'une ressource externe. Si l'on connait l'URL de la source de la 
citation, on pourra l'indiquer grâce à l'attribut "cite". Dans tous les autres cas, on utilise les guillemets sans 
balise spécifique. On peut aussi utiliser la balise <cite> pour citer un auteur. Confusion possible avec la balise
<blockquote>.
attributs
exemple

<rp>	Utilisé en annotations ruby pour définir ce qui est à montrer aux navigateurs ne supportant les éléments ruby.
attributs

<rt>	Pour expliquer des annotations en Ruby.
attributs

<ruby>	Utilisé pour des annotations en Ruby.
attributs

<s>	Utilisé pour identifier une partie de texte qui n'est pas correct ou non pertinent. Confusion possible avec la
balise <del>qui permet d'indiquer qu'un texte doit être remplacé ou supprimé.
attributs

<samp>	Utilisée dans un paragraphe pour écrire un échantillon de code. Confusion possible avec la balise <code>.
attributs

<script>	Pour ajouter un script internet ou externe.
attributs

<section>	Utilisée pour regrouper des éléments différents, mais partageant la même thématique Cette balise est le plus
souvent utilisé avec un header.
attributs

<select>	Pour une liste déroulante.
attributs

<small>	
(Attention la balise a été redéfinie, elle ne sert plus à minimiser un texte).
La balise small est utilisée comme contenu relatif mais non essentiel. On utilisera pour déclarer un copyright,
des disclaimers, des mises en garde...
attributs

<source>	Utilisée dans les balises <video> et <audio> pour indiquer l'url et le type des médias.
attributs

<span>	Utilisée pour mettre en style une portion de texte qui se différencie des autres. Cette balise ne doit pas être
utilisée si une autre balise de formatage de texte convient mieux.
attributs

<strike>	Non utilisé, la balise del remplace cette balise.

<strong>	Utilisé dans un paragraphe pour mettre un texte en gras.
attributs

<style>	Permets de définir un style dans le document HTML. Attention, l'attribut title à une valeur sémantique
différente pour cette balise.
attributs

<sub>	Déclarer un indice : Petit caractère placé en bas et à droite d'un autre caractère.
attributs

<summary>	Généralement utilisé à l'intérieur du conteneur <detail>, il permet de décrire le sommaire, la légende ou le
titre d'un élément.
attributs

<sup>	Déclarer un exposant : petit caractère placé en haut et à droite d'un autre caractère.
attributs

<table>	Pour un tableau. Note : En HTML5, l'attribut summary utilisé en accessibilité n'est plus valide et ne doit pas
être utilisé : la balise <caption> devient indispensable.
attributs

<tbody>	Utilisé pour le corps d'un tableau.
attributs

<td>	Pour déclarer une cellule dans un tableau.
attributs

<textarea>	Pour a champ de saisi.
attributs

<tfoot>	Utilisé pour le pied de page d'un tableau.
attributs

<th>	Pour déclarer une cellule dans l'entête d'un tableau.
attributs

<thead>	Utilisé pour l'entête d'un tableau.
attributs

<time>	Pour déclarer une date ou une heure.
attributs

<title>	Pour le titre de la page HTML en cours de lecture.
attributs

<tr>	Pour déclarer un champ dans un tableau.
attributs

<track>	Barre pour indiquer une portion de temps.
attributs

<tt>	Non utilisé

<u>	Non utilisé, cette balise créée une confusion avec la balise <a> au niveau des styles

<ul>	Pour créer une liste à puces.
attributs

<var>	Pour déclarer une variable.
attributs

<video>	Pour lire une video.
attributs

<wbr>	Balise non fermante à utiliser à l'intérieur d'un mot long pour forcer un retour à la ligne.
attributs
exemple
 

<xmp>	Non utilisé

[LIEN DU DOCUMENT](https://jaetheme.com/balises-html5/#liste-balises-html5)
