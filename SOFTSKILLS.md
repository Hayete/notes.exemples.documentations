#SOFTSKILLS
-----------

![img_69.png](img_69.png)

Parlons #softskills ! Apprenants de la #GEN, il faut les valoriser car elles intéressent de plus en plus le
#recruteurs notamment dans la #tech. D'après un étude Les Echos / Monster, les plus appréciées des managers 
sont :
- L’adaptabilité / agilité
- L’esprit d’équipe
- La rigueur / l’organisation
- La motivation, la passion
- L’empathie / l’écoute
  Dans ce nouvel épisode de #CultureTech, Julien Bouret, coach et co-auteur de plusieurs ouvrages sur le sujet, nous dit tout sur ces fameuses compétences comportementales !
