#AUTHENTIFICATION-JWT
--------------------

001 | JWT - Présentation => https://youtu.be/HQVXZXp8xFk
---------------------------------------------------------
Système d'authentifiquation de l'utilisateur
JSON WEB TOKEN
RFC 7519
Il permet l'échange sécurisé de jeton, entre plusieurs parties, pour la sécurité numériques à l'aide de données.

002 | JWT - Structure d'un token => https://youtu.be/uSnCMqGVgJM
----------------------------------------------------------------
Liste utilisable par langage(js)
liste de klain name
![img_38.png](img_38.png)
la clé aléatoire est à garder précieusement.
Nous pouvons changer de clé aléatoire.
Nous pouvons aussi traduire le code écrit sur la page sur le site BASE 4.

003 | JWT - Utilisation => https://youtu.be/lF3QFfty5Qo
-------------------------------------------------------
Après avoir renseigné l'email et le mot de passe, l'utilisateur devra validé par un lien depuis sa boite mail.
Nous ne noterons pas à cet endroit des données sensibles car les données sont facilement traduites et donc exploitables au détriment d'autrui.

004 | JWT - Installation de la librairie jsonwebtoken => https://youtu.be/2PND2uYqB7M
-------------------------------------------------------------------------------------
JWT=>LIBRAIRIE=>scroller jusqu'à JS=>prendre celle du milieu=>tout en bas, clique droit sur github=>DOCUMENTATION=>npm install jsonwebtoken
puis vérifier la bonne installation

005 | JWT - Préparation de l'api => https://youtu.be/gRCZ0_Yv-ss
-----------------------------------------------------------------


006 | JWT - Générer un token => https://youtu.be/seTGzX0JCNY
-------------------------------------------------------------


007 | JWT - Protéger les routes grâce à un middleware => https://youtu.be/GihxLF2T3Ug
-------------------------------------------------------------------------------------


008 | JWT - Déconnecter l'utilisateur côté client => https://youtu.be/iE6xizvM2-Q
---------------------------------------------------------------------------------
