La clean architecture, c'est quoi ? 
-----------------------------------
Le forum PHP 2019 organisé par l’AFUP s’est tenu les 24 et 25 octobre dernier à Paris. Comme chaque année il a rassemblé les acteurs de l’écosystème PHP,

autour de conférences sur des sujets d’actualité de cette technologie. Les équipes d’Adimeo ont eu la chance de pouvoir y assister.

L’une des conférences présentées durant l’événement abordait le concept de « Clean Architecture », présenté par un excellent speaker du nom de Nicolas

De Boose, actuellement CTO de Beelance.io.

Nicolas a souhaité faire un retour d’expérience sur l’implémentation d’une architecture logicielle nous venant d’un certain Robert C. Martin, « Uncle Bob »

de son surnom. Ingénieur logiciel de renom, il a réalisé des ouvrages très célèbres tel que « Clean Code», « Clean Coder », mais surtout le livre dans

lequel il détaille cette architecture… « Clean Architecture ».


Qu'est ce que la clean architecture ?
-------------------------------------
La Clean Architecture vise à réduire les dépendances de votre logique métier avec les services que vous consommez (API, Base de données, Framework,

Librairies tierces), pour maintenir une application stable au cours de ses évolutions, de ses tests mais également lors de changements ou mises à 

jour des ressources externes.

![](file:///C:/Users/totos/Downloads/CleanArchitecture.webp)

Le schéma ci-dessus résume brièvement l’organisation et le fonctionnement d’une clean architecture. On distingue une découpe selon différentes couches.

Il faut comprendre que plus on s’approche du centre du cercle plus votre logiciel devient de haut niveau. Les couches internes ne doivent pas être 

conscientes des couches externes, afin de ne pas être impactées.

Pourquoi passer à une clean architecture ?
---------------------------------------------
mvc-clean-architecture-1

Dans sa conférence, Nicolas prend pour exemple une architecture à laquelle on pense en général immédiatement : 

MVC. MVC pour « Model », « View », « Controller » est implémenté dans la plupart des projets, il n’est pas très complexe 

et permet tout de même de bien segmenter les couches applicatives. Le « Controller » est maître et se charge de capter la requête,

la traiter tout en se servant des données fournies par le modèle, pour enfin injecter les données voulues dans la vue.

Mais vous le savez très bien, au fur et à mesure notre application gagne en complexité. De nombreux cas spécifiques nous font barrage

et nous obligent à changer ce cheminement, si bien que la belle architecture que l’on avait soigneusement mise en place initialement

à tendance à perdre son sens…  C’est dans ces moments qu’on souhaiterait une architecture un peu plus segmentée et abstraite.

![](![img_25.png](img_25.png))

**Les règles à suivre lorsque l’on implémente une clean architecture**

Lors de l’implémentation de cette architecture il existe des règles, ayant toutes pour maître-mot « l’indépendance ».

La logique que vous implémentez doit :
---------------------------------------

- Être indépendante des frameworks : les frameworks et librairies doivent être des outils, sans pour autant vous contraindre.
  
- Être testable indépendamment : les tests doivent pouvoir être réalisés sans éléments externes (interface utilisateur, base de données ...)
  
- Être indépendante de l’interface utilisateur : l’interface utilisateur doit pouvoir changer de forme (console, interface web ...)
  
- Être indépendante de la base de données : il doit être possible de changer de SGBD.
  
- Être indépendante de tout service ou système externe : en résumé elle ne doit pas avoir conscience de ce qui l’entoure.


Pistes d’implémentation de la clean architecture
Dans sa conférence Nicolas met en avant trois points-clés :



1. Divide and keep control
   -----------------------
Si on souhaite par exemple utiliser une librairie d’envoi d’emails, on décide par facilité de faire appel à celle-ci directement dans nos
 
« controllers » ou « services ». Que se passe-t-il alors ?

Nous devenons dépendants de cette librairie. Cela empire si nous voulons refaire appel à la librairie à un autre endroit dans l’application

ou si la librairie évolue ou doit être changée. Et il y aura d’autant plus de code à modifier...

La solution ? Passer par des « ports » (interfaces) et « adapters » (classes implémentant ces interfaces). En clair, les couches internes

se servent d’interfaces pour abstraire les couches externes et utilisent des classes implémentant ces interfaces.

De ce fait on ne dépend plus du service utilisé mais des interfaces qui nous appartiennent.



2. Keep it where it belong
   -----------------------

Tout d’abord il faut prendre en considération que le coeur de notre application est constitué de « use cases » 

(cas d'utilisation de logique métier).

Ces « use cases » reçoivent des requêtes, les traitent et exposent la réponse. Puisqu’ils nous appartiennent, nous ne souhaitons 

pas qu’ils dépendent de la classe d’affichage de la réponse.

L’idée est donc de recevoir une requête déclenchant notre traitement métier (géré par le « controller » et/ou le 

« service » du « use case »), puis exposer les données de réponse à travers un « port ». Ainsi la classe chargée de récupérer les données 

(le « presenter ») importe peu et a la possibilité de changer. Passer par une interface pour le « presenter » permet de respecter notre 

abstraction pour le destinataire de la réponse.

3. Simplify the view
   ------------------
Par simplifier la vue, on entend adapter les données qui lui sont transmises. On souhaite la décharger au maximum de logique.

L’idée ici est de passer par une classe chargée de contenir nos données formatées pour l’affichage, le «View Model » .

Dans le point précédent nous faisions référence au « Presenter » chargé de récupérer les données de réponse.

Ce dernier va implémenter un « View Model » afin de transmettre à la vue un objet « à son image ».

Qu’en est-il des tests ?

Le travail mis en œuvre précédemment rend les tests plus naturels et simples. Le fait de limiter les dépendances permet de tester

couche par couche, sans inclure de code externe. Résultat, on teste uniquement l’essentiel : notre code, les dépendances

sont facilement remplaçables et peuvent donc laisser rapidement place à des « mocks objects » (simulation d’objets réels de manière contrôlée).

Conclusion
----------
La clean architecture apporte de nombreux avantages ; l'objectif principal étant l’indépendance de votre code par rapport aux technologies qui l’entourent (Framework, UI, BdD, librairie externe). Il devient testable facilement et précisément. L’organisation peut s’avérer complexe de par ses abstractions, il est donc important de tout de même faire preuve de pragmatisme, point également abordé dans une conférence du forum PHP, par Frédéric Bouchery et synthétisé dans notre article "Qu'est-ce que le développement pragmatique ? Retour sur le Forum PHP 2019".

Cet article aborde en surface la notion de clean architecture, pour plus d’informations je vous conseille de regarder la conférence de Nicolas De Boose ici :

![](![img_26.png](img_26.png))
