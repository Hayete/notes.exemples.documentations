API
===
cd client => npm start

cd server => node server.js

| Résilience = Une application web fonctionnera toujours même s'il y a des erreurs. |
|-----------------------------------------------------------------------------------


##001. API - Présentation => https://youtu.be/qPSx0CHKbUM
----------------------------------------------------------

![img_8.png](img_8.png)

:small_red_triangle: Application Programming Interface (Interface de Programmation Applicative) :small_red_triangle:

:small_red_triangle: L'API est chargée d'éxécuter les bonnes fonctions et les bonnes méthodes via l'interface. :small_red_triangle:

:small_red_triangle: Une API est une interface qui permet à deux programmes de communiqué entre eux. :small_red_triangle:

:small_red_triangle: Une API peut être constitué de classes de méthodes de fonctions de constantes etc,...qui va être fourni à notre programme. :small_red_triangle:

     
##002. API - Hypertext Transfert Protocol (HTTP) => https://youtu.be/jLyLpQQjemg
--------------------------------------------------------------------------------
:small_red_triangle: Protocole qui permet de transmettre des documents hypermédias comme le html. :small_red_triangle:

:small_red_triangle: Il a été concu initialement pour communiqué entre un navigateur web et un serveur web. :small_red_triangle:

:black_medium_small_square: [DOCUMENTATION MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)

:black_medium_small_square: [DOCUMENTATION W3C](https://www.w3.org/Protocols/)

![img_10.png](img_10.png)

:black_medium_small_square: [DOCUMENTATION HTTP](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) 

:small_red_triangle: PROTOCOLES

![img_11.png](img_11.png)

##003. API - Logiciel pour tester une api => https://youtu.be/tTbCkY0oIaA
-------------------------------------------------------------------------
- voir insomnia.md

- POSTMAN

- Afin de faire des requêtes http

- Dans paramétre renseigner quel partie du CRUD utiliser(get/post/put/delete/patch/option/head)

- Indiquez pour quel url http on demande la requête

- Puis choisir le type du body(json, html, etc,...)

- Entrer les informations puis send(envoyer)

- Pour tester une API nous pouvons aller par exemple sur les API du gouvernement

- curl pour chercher, les derniers chiffre/lettre sont des query

- pour donner une limite de recherche ajouter &limit=et entrer le chiffre choisi

- coller puis copier l'adresse http aprés GET

     
##004. API - Première api en NodeJs natif => https://youtu.be/bVPb6rYVp1E
-------------------------------------------------------------------------

:black_medium_small_square: [DOCUMENTATION GUIDE NODE.JS](https://nodejs.org/en/docs/guides/)

:black_medium_small_square: [DOCUMENTATION](https://nodejs.org/dist/latest-v14.x/docs/api/)


##005. API - Présentation et installation de ExpressJs => https://youtu.be/Mupb9Ku7r3w
--------------------------------------------------------------------------------------
Applications Web

:small_red_triangle: Express est une infrastructure d'applications Web Node.js minimaliste

et flexible qui fournit un

ensemble de fonctionnalités robuste pour les applications Web et mobiles.

Express apporte une couche fine de fonctionnalités d'application Web fondamentales, sans masquer

les fonctionnalités de Node.js.  


:small_red_triangle: En supposant que Node.js est déjà installé, créez un répertoire pour

héberger votre application et faites-en votre répertoire de travail.


$ mkdir myapp
$ cd myapp

:small_red_triangle: Utilisez la commande npm init afin de créer un fichier package.json pour 

votre application. Pour plus d’informations sur le fonctionnement du fichier package.json, voir

Specifics of npm’s package.json handling.


$ npm init

:small_red_triangle: Cette commande vous invite à fournir un certain nombre d’informations,

telles que le nom et la version de votre application. Pour le moment, vous pouvez simplement 

appuyer sur la touche RETURN pour accepter les valeurs par défaut, à l’exception de ce qui suit :


entry point: (index.js)

:small_red_triangle: Entrez app.js ou un nom de votre choix pour le fichier principal. Si vous

souhaitez que le nom soit index.js, appuyez sur la touche RETURN pour accepter le nom de fichier

par défaut suggéré.

:small_red_triangle: Installez ensuite Express dans le répertoire myapp, puis sauvegardez-le 

dans la liste des dépendances. Par exemple :


$ npm install express --save

:small_red_triangle: Pour installer Express de façon temporaire et ne pas l’ajouter à la liste 
des dépendances, omettez l’option --save :


$ npm install express

:small_red_triangle: Les modules Node.js installés à l’aide de l’option --save sont ajoutés à la

liste des dépendances dependencies, dans le fichier package.json. Par défaut, depuis la version

5.0, npm install <package-name> ajoute automatiquement le module Node.js à la liste des

dépendances. Par la suite, l’exécution de npm install dans le répertoire de l’application

installera automatiquement les modules présents dans la liste des dépendances.

:small_red_triangle: Créer gitignore: tapez dedans : /node_nodules et /.idea où autre pour 

éviter  de les copier sur git.

##006. API - Créer un server http avec ExpressJs => https://youtu.be/zf3vSx5t5IU
--------------------------------------------------------------------------------


:small_red_triangle: Premièrement, créez un répertoire appelé myapp, rendez-vous dedans et 

exécutez la commande npm init. Ensuite, installez express en tant que dépendance en suivant les

instructions du guide d’installation.

Dans le répertoire myapp, créez un fichier appelé app.js et ajoutez le code suivant :

````js
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
res.send('Hello World!')
})

app.listen(port, () => {
console.log(`Example app listening at http://localhost:${port}`)
})
````
:small_red_triangle: L’application démarre un serveur et écoute le port 3000 à la recherche de

connexions. L’application répond “Hello World!” aux demandes adressées à l’URL racine (/) ou à

la route racine. Pour tous les autres chemins d’accès, elle répondra par 404 Not Found.

Les objets req (demande) et res (réponse) sont exactement les mêmes que ceux que Node fournit,

vous pouvez donc appeler req.pipe(), req.on('data', callback) et tout autre objet sans recourir

à Express.

Exécutez l’application avec la commande suivante :


$ node app.js

Puis chargez http://localhost:3000/ dans un navigateur pour consulter le résultat.
````typescript
// 1. Récupérer la librairie express
const express = require("express");

// 2. Créer un serveur
const server = express();

// 4. Indiquer une route
server.get("/", (request, response) => {

// 5. On renvoie une réponse
response.send("<p>Hello World</p>");

});

// 6. Ajouter 2ème route (renvoyer cette fois un tableau dans la constante users)
server.get("/users", (request, response) => {
const users = [];
response.send({users});
})

// 7. Ajouter une route post
server.post("/users", (request, response) => {
response.send({message: "User saved", user: "Sam"});
})

// 3. Activer le server, prend 1er paramètre le port et une fonction callback
server.listen(8080, () => {
console.log("Server running on http://localhost:8080");
});
````

##007. API - Paramètre de route et contenue de la requête => https://youtu.be/RRYAlnX0vGU
-----------------------------------------------------------------------------------------

:small_red_triangle: Générateur d’applications Express

Utilisez l’outil de générateur d’applications, express, pour créer rapidement un squelette 

d’application.

:small_red_triangle: Installez express à l’aide de la commande suivante :


$ npm install express-generator -g

:small_red_triangle: Affichez les options de commande à l’aide de l’option -h :


$ express -h

:black_medium_small_square: Usage: express [options](dir)

Options:

    -h, --help          output usage information
        --version       output the version number
    -e, --ejs           add ejs engine support
        --hbs           add handlebars engine support
        --pug           add pug engine support
    -H, --hogan         add hogan.js engine support
        --no-view       generate without view engine
    -v, --view <engine> add view <engine> support (ejs|hbs|hjs|jade|pug|twig|vash) (defaults to jade)
    -c, --css <engine>  add stylesheet <engine> support (less|stylus|compass|sass) (defaults to plain css)
        --git           add .gitignore
    -f, --force         force on non-empty directory

:small_red_triangle: Par exemple, ce code crée une application Express nomée myapp. 

L’application sera crée dans le dossier myapp, lui meme placé dans le repertoir de travail

courant. Le moteur de vue sera 

configuré avec Pug:


$ express --view=pug myapp

create : myapp

create : myapp/package.json

create : myapp/app.js

create : myapp/public

create : myapp/public/javascript

create : myapp/public/images

create : myapp/routes

create : myapp/routes/index.js

create : myapp/routes/users.js

create : myapp/public/stylesheets

create : myapp/public/stylesheets/style.css

create : myapp/views

create : myapp/views/index.pug

create : myapp/views/layout.pug

create : myapp/views/error.pug

create : myapp/bin

create : myapp/bin/www

:small_red_triangle: Ensuite, installez les dépendances :

$ cd myapp
$ npm install

:small_red_triangle: Sous MacOS ou Linux, exécutez l’application à l’aide de la commande 

suivante :

$ DEBUG=myapp:* npm start

:small_red_triangle: Sous Windows, utilisez la commande suivante :

> set DEBUG=myapp:* & npm start

:small_red_triangle: Ensuite, chargez 

:black_medium_small_square: ‘http://hôte_local:3000/’ dans votre navigateur pour accéder à 

l’application.

L’application générée possède la structure de répertoire suivante :


.
├── app.js

├── bin

│   └── www

├── package.json

├── public

│   ├── images

│   ├── javascript

│   └── stylesheets

│       └── style.css

├── routes

│   ├── index.js

│   └── users.js

└── views

├── error.pug

├── index.pug

└── layout.pug

7 directories, 9 files

:small_red_triangle: La structure d’application créée par le générateur est l’une des nombreuses

manières possibles de structurer les applications Express. Vous avez toute latitude pour

l’utiliser ou la modifier en fonction de vos besoins.

:small_red_triangle: Routage de base

Routage fait référence à la détermination de la façon dont une application répond à un nœud final

spécifique, c’est-à-dire un URI (ou chemin) et une méthode de requête HTTP (GET, POST, etc.).

Chaque route peut avoir une ou plusieurs fonctions de gestionnaire, qui sont exécutées lorsque

la route est mise en correspondance.

La définition de la route utilise la structure suivante :


- app.METHOD(PATH, HANDLER)

Où :

- app est une instance d’express.

- METHOD est une méthode de demande HTTP.

- PATH est un chemin sur le serveur.

- HANDLER est la fonction exécutée lorsque la route est mise en correspondance.


:small_red_triangle: Ce tutoriel suppose qu’une instance d’express appelée app soit créée et que le serveur soit en

cours d’exécution. Si vous ne savez pas créer et démarrer une application, reportez-vous à

l’exemple Hello world.

Les exemples suivants illustrent la définition de routes simples.

:small_red_triangle: Réponse Hello World! sur la page d’accueil :

````js
app.get('/', function (req, res) {
res.send('Hello World!');
});
````
:small_red_triangle: Réponse à une demande POST sur la route racine (/), sur la page d’accueil de l’application :

````js
app.post('/', function (req, res) {
res.send('Got a POST request');
});
````
:small_red_triangle: Réponse à une demande PUT sur la route /user :

````js
app.put('/user', function (req, res) {
res.send('Got a PUT request at /user');
});
````
:small_red_triangle: Réponse à une demande DELETE sur la route /user :

````js
app.delete('/user', function (req, res) {
res.send('Got a DELETE request at /user');
});
````
:small_red_triangle: Servir des fichiers statiques dans Express

Pour servir des fichiers statiques tels que les images, les fichiers CSS et les fichiers 

JavaScript, utilisez la fonction de logiciel intermédiaire intégré express.static dans Express.

Passez le nom du répertoire qui contient les actifs statiques dans la fonction de logiciel

intermédiaire express.static afin de commencer à servir les fichiers directement. Par exemple,

utilisez le code suivant pour servir des images, des fichiers CSS et des fichiers JavaScript

dans un répertoire nommé public :


app.use(express.static('public'));

:small_red_triangle: Maintenant, vous pouvez charger les fichiers qui sont dans le répertoire public :


:black_medium_small_square: http://localhost:3000/images/kitten.jpg

:black_medium_small_square: http://localhost:3000/css/style.css

:black_medium_small_square: http://localhost:3000/js/app.js

:black_medium_small_square: http://localhost:3000/images/bg.png

:black_medium_small_square: http://localhost:3000/hello.html

:small_red_triangle: Express recherche les fichiers relatifs au répertoire statique, donc le nom

du répertoire statique ne fait pas partie de l'URL.

Pour utiliser plusieurs répertoires statiques actifs, utilisez la fonction middleware express.

static plusieurs fois :

app.use(express.static('public'));

app.use(express.static('files'));

:small_red_triangle: Express recherche les fichiers dans l’ordre dans lequel vous avez établi les répertoires statiques avec la fonction middleware express.static.

:small_red_triangle:Pour créer un préfixe de chemin d’accès virtuel (dans lequel le chemin 

 d’accès n’existe pas vraiment dans le système de fichiers) pour les fichiers qui sont servis

 par la fonction express.static, indiquez un chemin de montage pour le répertoire statique, 

 comme démontré ci-dessous :

app.use('/static', express.static('public'));

:small_red_triangle: Maintenant, vous pouvez charger les fichiers qui sont dans le répertoire 

public à partir du préfixe de chemin d’accès /static.

:black_medium_small_square: http://localhost:3000/static/images/kitten.jpg

:black_medium_small_square: http://localhost:3000/static/css/style.css

:black_medium_small_square: http://localhost:3000/static/js/app.js

:black_medium_small_square: http://localhost:3000/static/images/bg.png

:black_medium_small_square: http://localhost:3000/static/hello.html

:small_red_triangle: Cependant, le chemin d’accès que vous fournissez à la fonction express.

static est en rapport avec le répertoire à partir duquel vous lancez votre processus node. Si

vous exécutez l’application express à partir d’un autre répertoire, il est plus sûr d’utiliser

le chemin d’accès absolu que vous voulez servir :
````typescript
app.use('/static', express.static(__dirname + '/public'));

const express = require("express");
const server = express();

// 2. Parser le JSON donc les payloads/body
server.use(express.json());

// GET
server.get("/", (request, response) => {
response.send("<p>Hello World</p>");

});


// GET /users
server.get("/users", (request, response) => {
const users = [];
response.send({users});
});


// POST /users
server.post("/users", (request, response) => {

// 1. Accéder body de la request
console.log(request.body.user);
response.send({message: "User saved", user: "Sam", req: request.body.user});
});

// 4. PUT /users/:id
server.put("/users/:id", (request, response) => {
response.send({message: "User updated successfully", user: request.params.id, data: request.body});
});

// DELETE /users/:id

// 3. Supprimer utilisateur par rapport à son id
server.delete("/users/:id", (request, response) => {
console.log(request.params);
response.send({message: "User successfully deleted", user: request.params.id});
});


server.listen(8080, () => {
console.log("Server running on http://localhost:8080");
});
````
##008. API - Gérer les erreurs et changer le status de la réponse => https://youtu.be/8tr82LyH46Q
-------------------------------------------------------------------------------------------------
 ![img_9.png](img_10.png)
````typescript
 // POST /users
 // 2. Modifier status
 server.post("/users", (request, response) => {
 console.log(request.body.user);

// Vérifications
if (request.body.email) {

} else {

}

// Logique
response.statusCode = 404;
response.send({message: "User saved", user: "Sam", req: request.body.user});
});



// DELETE /users/:id

// 1. Supprimer user et modifier status
server.delete("/users/:id", (request, response) => {
console.log(request.params);

// Vérifie que l'id correspond à un utilisateur dans la base de données

const userExist = false;
if (userExist === false) {
response.status(404).send('User not found : ' + request.params.id);
}

response.status(201).send({message: "User successfully deleted", user: request.params.id});
});
````

EXERCICE 1
----------

@Elèves exercice sur les API
Exercice : Créer un server http avec express

Ajouter les routes suivantes :
- GET /users : Récupère la liste de tous les utilisateurs 
  Aucune donnée envoyé avec la requête
  Retourne un tableau d'utilisateurs

- POST /users : Enregistre un utilisateur avec les données que je t'envoie 
  Envoyez dans la requête une donnée au format JSON : firstName, lastName, email
  Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur enregistré !"

- GET /users/{id} : Récupère les informations d'un utilisateur en fonction de son "id" 
  Aucune donnée envoyé avec la requête 
  Retourne un objet avec les propriétés : firstName, lastName, email

- PUT /users/{id} : Mets à jour l'utilisateur en fonction de son id avec les données que je t'envoie
  Envoyez dans la requête une donnée au format JSON : firstName, lastName 
  Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur mis à jour !"

- DELETE /users/{id} : Supprime l'utilisateur en fonction de son id
  Aucune donnée envoyé avec la requête 
  Retourne un message "Utilisateur supprimé"

Nous n'avons pas de base de données lié à notre api pour l'instant et n'oubliez pas que le protocol http est stateless il ne partage pas d'état avec d'autre requête. C'est à dire, chaque requête est gérer indépendamment les unes des autres.

Attention : Le "{id}" est la représentation d'un paramètre dans l'url, écrivez le paramètre dans une route de la bonne manière.

````typescript

const express = require("express");

const server = express();
const PORT = 8080;

server.use(express.json());


/* - GET /users : Récupère la liste de tous les utilisateurs
Aucune donnée envoyé avec la requête
Retourne un tableau d'utilisateurs
*/

server.get("/users", (request, response) => {
const users = [{id: 1, firstName: "Hayete"}];
response.send(users);
});

/*
- POST /users : Enregistre un utilisateur avec les données que je t'envoie
  Envoyez dans la requête une donnée au format JSON : firstName, lastName, email
  Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur enregistré !"
  */

server.post("/users", (request, response) => {
console.log(request.body);
response.send({message: "Users has been saved!", data: request.body});
});

/*
- GET /users/{id} : Récupère les informations d'un utilisateur en fonction de son "id"
  Aucune donnée envoyé avec la requête
  Retourne un objet avec les propriétés : firstName, lastName, email
  */

server.get("/users/:id", (request, response) => {
const users = [
{firstName: "Sam", lastName: "Bernard", email: "sam.bernard@gmail.com"},
{firstName: "John", lastName: "Swow", email: "john.snow@gmail.com"}
];

    const user = users[request.params.id];

    if (user == null) {
        response.status(404).send({message: "User not found!", userId: request.params.id});
    }

    response.send(user);
});


/*
- PUT /users/{id} : Mets à jour l'utilisateur en fonction de son id avec les données que je t'envoie
  Envoyez dans la requête une donnée au format JSON : firstName, lastName
  Retourne un objet contenant les informations que vous lui avez envoyé et un message disant "Utilisateur mis à jour !"
  */

server.put("/users/:id", (request, response) => {

    if (!request.body.firstName) {
        response.status(400).send("firstName missing!");
    }
    const id = request.params.id;
    // La logique pour mettre à jour l'utilisateur

    const user = {firstName: "Fiona", LastName: "Pat"}; // On récupère l'utilisateur en fonction de son id dans une base de donnée

    // Vérification que l'utilisateur a été trouvé
    if (user == null) {
        response.status(404).send({message: "User not found!", userId: request.params.id});
    }

    // Met à jour l'utilisateur récupéré
    user.firstName = request.body.firstName;
    user.lastName = request.body.firstName;
    user.email = request.body.email;

    response.send({message: "User has been updated!", id, user});
});

/*
- DELETE /users/{id} : Supprime l'utilisateur en fonction de son id
  Aucune donnée envoyé avec la requête
  Retourne un message "Utilisateur supprimé"
  */

server.delete("/users/:id", (request, response) => {

    const id = request.params.id;
    const user = null;

    // user.delete();

    response.send({message: "User has been deleted."});
});

server.listen(8080, "127.0.0.1", function () {
console.log("Server is listening at http://localhost:" + PORT)
});
````

ChalomEllezam — 08/04/2021
[packge.json](https://www.npmjs.com/package/jsonwebtoken
npm
jsonwebtoken
JSON Web Token implementation (symmetric and asymmetric)

ChalomEllezam — 13/04/2021
[port](https://www.speedguide.net/port.php?port=8081)
SpeedGuide
Port 8081 (tcp/udp)
Port 8081 tcp/udp information, assignments, application use and known security risks.
[sectigostore](https://sectigostore.com/blog/port-443-everything-you-need-to-know-about-https-443/)
InfoSec Insights
Lumena Mukherjee
Port 443 — Everything You Need to Know About HTTPS 443 | InfoSec In...
Eliminate insecure connection warnings by using an SSL/TLS certificate that serves your site via HTTPS. Port 443 is what makes that possible. Here's how.



