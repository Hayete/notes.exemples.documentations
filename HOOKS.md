#Introduction aux Hooks
------------------------

Les Hooks sont arrivés avec React 16.8. Ils vous permettent de bénéficier d’un état local et d’autres fonctionnalités de
React sans avoir à écrire une classe.

**Appelez les Hooks uniquement au niveau racine**

N’appelez pas de Hooks à l’intérieur de boucles, de code conditionnel ou de fonctions imbriquées. Au lieu de ça, utilisez seulement les Hooks au niveau racine de votre fonction React. En suivant cette règle, vous garantissez que les Hooks sont appelés dans le même ordre à chaque affichage du composant. C’est ce qui permet à React de garantir le bon état des Hooks entre plusieurs appels à useState et useEffect. (Si vous êtes curieux·se, nous expliquerons ça en détail plus bas.)
Appelez les Hooks uniquement depuis des fonctions React

**N’appelez pas les Hooks depuis des fonctions JavaScript classiques. Vous pouvez en revanche :**

    ✅ Appeler les Hooks depuis des fonctions composants React.
    ✅ Appeler les Hooks depuis des Hooks personnalisés (nous aborderons le sujet dans la prochaine page).

En suivant cette règle, vous garantissez que toute la logique d’état d’un composant est clairement identifiable dans son code source.

| useState=utilisé l'état |
|-------------------------|
````jsx
import React, { useState } from 'react';

function Example() {
// Déclare une nouvelle variable d'état, qu’on va appeler « count »  const [count, setCount] = useState(0);
return (
<div>
<p>Vous avez cliqué {count} fois</p>
<button onClick={() => setCount(count + 1)}>
Cliquez ici
</button>
</div>
);
}
````
##Hook d’effet
---------------

Vous avez surement déjà réalisé un chargement de données distantes, des abonnements ou des modifications manuelles sur

le DOM depuis un composant React. Nous appelons ces opérations « effets de bord » (ou effets pour faire court) parce

qu’elles peuvent affecter d’autres composants et ne peuvent être réalisées pendant l’affichage.

Le Hook d’effet, useEffect, permet aux fonctions composants de gérer des effets de bord. Il joue le même rôle que 

componentDidMount, componentDidUpdate, et componentWillUnmount dans les classes React, mais au travers d’une API unique.

(Nous verrons des exemples comparant useEffect à ces méthodes dans Utiliser le Hook d’effet .)

**Par exemple, ce composant change le titre du document après que React a mis à jour le DOM :**

````jsx
import React, { useState, useEffect } from 'react';
function Example() {
const [count, setCount] = useState(0);

// Équivalent à componentDidMount plus componentDidUpdate :  useEffect(() => {    // Mettre à jour le titre du document en utilisant l'API du navigateur    document.title = `Vous avez cliqué ${count} fois`;  });
return (
<div>
<p>Vous avez cliqué {count} fois</p>
<button onClick={() => setCount(count + 1)}>
Cliquez ici
</button>
</div>
);
}
````
Lorsque vous appelez useEffect, vous dites à React de lancer votre fonction d’« effet » après qu’il a mis à jour le DOM.

Les effets étant déclarés au sein du composant, ils ont accès à ses props et son état. Par défaut, React exécute les

effets après chaque affichage, y compris le premier. (Nous aborderons plus en détails la comparaison avec le cycle de

vie des classes dans Utiliser le Hook d’effet.)

Les effets peuvent aussi préciser comment les « nettoyer » en renvoyant une fonction. Par exemple, ce composant utilise

**un effet pour s’abonner au statut de connexion d’un ami, et se nettoie en résiliant l’abonnement :**
````jsx
import React, { useState, useEffect } from 'react';

function FriendStatus(props) {
const [isOnline, setIsOnline] = useState(null);

function handleStatusChange(status) {
setIsOnline(status.isOnline);
}

useEffect(() => {    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);    return () => {      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);    };  });
if (isOnline === null) {
return 'Chargement...';
}
return isOnline ? 'En ligne' : 'Hors-ligne';
}
````
Dans cet exemple, React nous désabonnerait de notre ChatAPI quand le composant est démonté, mais aussi juste avant de

relancer l’effet suite à un nouvel affichage. (Le cas échéant, vous pouvez dire à React de sauter le ré-abonnement si

la props.friend.id passée à ChatAPI n’a pas changé.)

**Tout comme avec useState, vous pouvez utiliser plus d’un seul effet dans un composant :**
````jsx
function FriendStatusWithCounter(props) {
const [count, setCount] = useState(0);
useEffect(() => {    document.title = `Vous avez cliqué ${count} fois`;
});

const [isOnline, setIsOnline] = useState(null);
useEffect(() => {    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
return () => {
ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
};
});

function handleStatusChange(status) {
setIsOnline(status.isOnline);
}
// ...
````
Les Hooks vous permettent d’organiser les effets de bord dans un composant en rassemblant leurs parties (telles que l’abonnement et le désabonnement), plutôt que de vous forcer à les répartir dans les méthodes de cycle de vie.
