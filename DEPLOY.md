#DEPLOY
-------

001 | Deploy - Introduction => https://youtu.be/gKrRykIApPM
-----------------------------------------------------------


002 | Deploy - Créer un serveur => https://youtu.be/xp7Fgkw8Fm4
---------------------------------------------------------------


003 | Deploy - Nom de domaine => https://youtu.be/zqDfkYIGlOg
-------------------------------------------------------------


004 | Deploy - Connexion au server via ssh et le terminal => https://youtu.be/diYFGRWK0vA
-----------------------------------------------------------------------------------------


005 | Deploy - Les commandes indispensables du terminal => https://youtu.be/tNtlxPLWlFc
---------------------------------------------------------------------------------------


006 | Deploy - Configurer les variables d'environnement => https://youtu.be/2CaT5HkGtuQ
---------------------------------------------------------------------------------------


007 | Deploy - Présentation gitignore => https://youtu.be/ZfsYz4nOU44
---------------------------------------------------------------------


008 | Deploy - Récupérer le projet dans le serveur => https://youtu.be/A-sBEE9RGGU
----------------------------------------------------------------------------------


009 | Deploy - Installer nodejs sur le serveur => https://youtu.be/ebye-WBjMf4
------------------------------------------------------------------------------


010 | Deploy - Installer le gestionnaire de process PM2 => https://youtu.be/Rx1A4zabK0U
---------------------------------------------------------------------------------------


011 | Deploy - Installer NGINX sur le server => https://youtu.be/2Kbj1tZv_bs
----------------------------------------------------------------------------


012 | Deploy - Configuration de notre premier server block => https://youtu.be/pyPhP_Ok7Vc
------------------------------------------------------------------------------------------


013 | Deploy - Reverse proxy => https://youtu.be/N5WZX6yfrRI
------------------------------------------------------------
