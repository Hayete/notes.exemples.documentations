
![img_13.png](img_13.png)

Définition :
------------

:small_red_triangle: Une méthode Agile est une approche itérative et collaborative, capable de prendre en compte les besoins

initiaux du client et ceux liés aux évolutions.

:small_red_triangle: Le terme « agile » fait référence à la capacité.

Principes de base:
------------------

:small_red_triangle: La méthode Agile se base sur un cycle de développement qui porte le client au centre. Le client est impliqué

dans la réalisation du début à la fin du projet. Grâce à la méthode agile le demandeur obtient une meilleure visibilité de la

gestion des travaux qu’avec une méthode classique.

L’implication du client dans le processus permet à l’équipe d’obtenir un feedback régulier afin d’appliquer directement les

changements nécessaires.

Cette méthode vise à accélérer le développement d’un logiciel. De plus, elle assure la réalisation d’un logiciel fonctionnel tout

au long de la durée de sa création.

Le principe de base consiste à proposer une version minimale du logiciel puis à intégrer des fonctionnalités supplémentaires à

cette base, par processus itératif. Le processus itératif regroupe une séquence d’instructions à répéter autant de fois que

possible, selon le besoin. En ce qui concerne la réalisation d’un logiciel, 

![](Capture d’écran 2021-02-21 170544)
les instructions à répéter sont les suivantes :
-----------------------------------------------

**Les tests unitaires à la fin de chaque itération**

**Le développement de l’application web**

**L’intégration**

**La relecture et amélioration des codes**

:small_red_triangle: La méthode agile nommée Manifeste Agile repose sur quatre grands principes :

*COLLABORATION : Communication et cohésion d’équipe passent avant les outils et les processus.
--------------
*EQUIPE : Le privilège de la relation équipe/client est mis en avant plutôt que la négociation contractuelle.
---------
*APPLICATION : Préférer une application bien construite à une documentation détaillée.
-------------
*ACCEPTATION : Le choix de l’acceptation du changement et de la flexibilité au détriment d’un plan rigide.
-------------

En effet le changement de contexte et les modifications interviennent dans le processus suite aux demandes du client qui feront

évoluer le projet plus rapidement.

:small_red_triangle: Tous les jours à horaire régulier un « stand up meeting » est réalisé. Lors de ce bref point quotidien

d’environs chaque membre de l’équipe va :
-----------------------------------------
**relater les faits actuels**

**partager les actions à prévoir pour le lendemain**

**exprimer les obstacles rencontrés et les difficultés encourues**

:small_red_triangle: Zoom sur les principales méthodes agiles

En effet, lorsque l’on emploie le terme « méthode agile » au singulier on parle d’un concept. Cependant il existe plusieurs

méthodes agiles qui se différencient les unes des autres.

:small_red_triangle: Scrum

Aujourd’hui « Scrum » est la méthode agile la plus populaire. Ce terme signifie « mêlée » au rugby. La méthode scrum s’appuie

sur des « sprints » qui sont des espaces temps assez courts pouvant aller de quelques heures jusqu’à un mois. Généralement et

de préférence un sprint s’étend sur deux semaines. À la fin de chaque sprint, l’équipe présente ce qu’elle a ajouté au produit.

Scrum regroupe trois acteurs :
------------------------------

:small_red_triangle: <<Le Product Owner>> (ou « Directeur de produit ») : il communique les objectifs premiers des clients et

utilisateurs finaux, coordonne l’implication des utilisateurs et des parties prenantes, et se coordonne lui-même avec les autres

product owners pour assurer une cohérence.

:small_red_triangle: Le Scrum Master : membre de l’équipe, il a pour but d’optimiser la capacité de production de l’équipe. 

Pour se faire, le scrum master aide l’équipe à travailler de façon autonome tout en s’améliorant d’avantage.

:small_red_triangle: L’équipe opérationnelle (qui regroupe idéalement moins de dix personnes) : la particularité d’une équipe

scrum est qu’elle est dépourvue de toute hiérarchie interne. Une équipe scrum est auto-organisée.

D’autres termes sont à connaître pour comprendre la méthode scrum:
------------------------------------------------------------------

**Le product backlog (carnet du produit) : ce document contient les exigences initiales dressées puis hiérarchisées avec le**

**client en début de projet. Néanmoins il va évoluer tout au long de la durée du projet, en fonction des divers besoins du client.**

**Le sprint backlog (carnet de sprint) : en chaque début de sprint, l’équipe définit un but. Puis lors de la réunion de sprint,**

:small_red_triangle: l’équipe de développement choisit les éléments du carnet à réaliser. L’ensemble de ces éléments constitue

alors le sprint backlog.

*User story : ce terme désigne les fonctionnalités décrites par le client.
------------
*La mêlée (scrum) : c’est une réunion d’avancement organisée de manière quotidienne durant le sprint.
-----------------

:small_red_triangle: EXtreme Programming (XP)
                     

Cette méthode très réactive destinée à des petits ou moyens projets, permet de réduire les coûts du changement. Dans cette

méthode, le client pilote le projet grâce à des cycles itératifs d’une à deux semaines. C’est lui qui choisit les fonctionnalités

à implémenter. Il transmet ses exigences à l’équipe sous forme de scénario susceptible d’être implémenté en une itération.

Dans la méthode XP l’équipe se décompose en binômes qui revoient régulièrement les codes. On parle de responsabilité collective

du code, le but étant que chaque développeur soit capable d’intervenir n’importe où dans la structure interne du logiciel.

Auquel s’ajoutent des tests automatisés mis en place afin de vérifier chacune des fonctionnalités demandées par le client et de

garantir une bonne qualité du produit.

:small_red_triangle: Rapid Application Development (RAD)
                     

Cette méthode agile est la plus ancienne de toutes les autres méthodes agiles. Elle se base sur un cycle de développement allant

de 90 à 120 jours, incluant cinq phases, à savoir :

**L’initialisation (préparation de l’organisation et plan de communication)**

**Le cadrage (analyse et expression des exigences)**

**Le design (conception et modélisation)**

**La construction (réalisation et prototypage qui représentent environ 50% du projet)**

**Le contrôle final de qualité par site pilote**

:small_red_triangle: On peut également citer d’autres méthodes agiles comme Rational Unified Process (RUP), 

:small_red_triangle: Feature Driven Development (FDD) ou 

:small_red_triangle: Dynamic systems development method (DSDM).

Les points forts de la méthode agile :
--------------------------------------

**L’équipe fait peu de hors sujet car cette méthode assure une bonne et constante communication entre le client et l’entreprise**

**La documentation est réduite, ainsi l’efficacité en terme de productivité est en augmentation**

**La collaboration avec le client s’effectue de façon quotidienne**

**Une version fonctionnelle du logiciel est livrée fréquemment**

**La recherche constante de l’excellence technique : des tests sont réalisés en continu**

**Le résultat est percevable petit à petit, ce qui permet d’éviter les mauvaises surprises**

**En clair, la méthodologie Agile est une méthode moderne, favorisant un gain de productivité non négligeable, et la baisse des**

**coûts de production.**

[DOCUMENTATION](https://www.bing.com/search?q=m%C3%A9thode+agile+d%C3%A9finition&cvid=24d5659abfa04e83a8f88d20c2b6439c&pglt=547&FORM=ANNTA1&PC=HCTS)


>Dans le domaine de la gestion de projets informatiques, les méthodes agiles privilégient la collaboration entre des équipes
> 
> professionnelles et leurs clients, le dialogue entre toutes les parties prenantes (clients, utilisateurs, développeurs et
> 
> prestataires) et la souplesse en cours de réalisation. Les clients sont impliqués au maximum pour permettre une plus grande
> 
> réactivité par rapport aux méthodes traditionnelles de gestion. 
