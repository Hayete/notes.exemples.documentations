#GITLAB
=======

GitLab est un logiciel libre de forge basé sur git proposant les fonctionnalités de wiki, un 

système de suivi des bugs, l’intégration continue et la livraison continue. Développé par 

GitLab Inc et créé par Dmitriy Zaporozhets et par Valery Sizov, le logiciel est utilisé par

plusieurs grandes entreprises informatiques incluant IBM, Sony, le centre de recherche de

Jülich, la NASA, Alibaba, Oracle, Invincea, O’Reilly Media, Leibniz Rechenzentrum, le CERN3,

4,5, European XFEL, la GNOME Foundation, Boeing, Autodata, SpaceX6 et Altares. 

##UTILISATION
-------------

- sauvegarde de fichiers, documents, etc..., générés sur Webstorm,

- travail collaboratif,

- mise sur le web du travail,

##MANIPULATION
--------------

- installer Gitlab,

- créer un nouveau projet,

- le nommmer, cocher éventuellement la case "readme",

- cloner,

- ajouter des branches si necéssaire (donc recloner),

- inviter éventuellement des membres,

- aller sur webstorm pour plus d'explication pour la suite.