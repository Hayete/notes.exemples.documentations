# TYPESCRIPT
=============
https://dev.to/wpreble1/typescript-with-react-functional-components-4c69

//(tsc <filename>.ts à essayer)

**TypeScript est un langage orienté objet open source développé et maintenu par Microsoft, sous licence Apache 2. Il s'agit d'un sur-**

**ensemble typé de Javascript qui se compile en JavaScript brut. TypeScript a été développé sous Anders Hejlsberg, qui a également dirigé**

**la création du langage C #. TypeScript a été publié pour la première fois en octobre 2012.**

- superset du langage JavaScript (microsoft)
- se compile en JS facilement
- facile à intégrer à un projet nouveau où existant
- développement de grosses applications


001.presentation node.js
--------------------------

[COURS 1](https://youtu.be/8Ak786GELow)

- est un moteur d'éxecution du langage JavaScript
- Node JS nous donne accès au développement d'application d'API côté serveur
- grâce à node JS il est possible d'éxecuter du javascript
- node JS est basé sur le moteur V8 chrome
- installation avec typescript.org
- paquet de management (npm) gestionnaire de paquet
- il y a une documentation sur node JS
- node JS côté serveur (le code qui y est développé par le serveur n'est utilisable que par le serveur
- typescript est un langage de programmation qui va être traduit en javascript


002.presentation npm(NODE PACKAGE MANAGER)
--------------------

[COURS 2](https://youtu.be/N-RpiBQF4Zg)
- chronique de notre paquet management
- un paquet est un programme développé en JS et qui est mis en avant sur la plateforme
  npm pour la communauté JS
- dans npm on peut installer des paquets, des frameworks, etc...
- on peut les installer, gérer, créer nous-mêmes des paquets et les publiés
- il y a plus d'1 million de paquets angular, react, JS...

003.presentation typescript
---------------------------

[COURS 3](https://youtu.be/a114c2kEgLU)
- est un langage de programmation développé par microsoft
- gratuit et open source
- va rajouter certaines fonctionnalités à JS (typage, class, etc...)
- Permet d'améliorer le code, et la sécurité du code afin de le rendre plus lisible et plus propre
- transpiller est un logiciel qui permet de traduire un code en code cible, en l'occurence ici typescript en javascript(ex. npm tsc)

**ECRITURE EN TYPESCRIPT**

````typescript
let var1: boulean = true;
````
**TRADUCTION EN JAVASCRIPT**

````javascript
var var1 = true;
````
:small_red_triangle: typescript tsc index.ts

:small_red_triangle: / dans typescript = enfant

:small_red_triangle: npm --h où help

004.configurer un package node.js
--------------------------------

[COURS 4](https://youtu.be/s3WTu1jJgIc)

- créer un fichier html
- aller sur le tableau de commande
- npm afin d'y insérer des informations
- package.json va résumer toutes les informations à l'intérieur
- ajout du package.json relatif au paquet
- grâce à npm le paquet est à jour et publiable
- possibilité de rajouter un code provenant de la librairie npm
- ex. aller sur le terminal et y ajouter i express
- c'est une dépendance (la dépence peut aussi avoir une où des dépendance(s))
- lock = c'est pour consulter toutes les dépendances 
- script npm : run, build, test
- on ne partage jamais nos modules(publication) seuleument index.html, package.json et package-lock.json
- les commandes sont : start, build, run (npm run build)par exemple

005.basic types 
---------------
  
[COURS 5](https://youtu.be/WlyIWGLzHo4)

| : pour typé |
|-------------|

**ECRITURE EN JAVASCRIPT**

````javascript
let a = 3; // dynamic type(typage dynamique)
a = "2"; //l'interpreteur va penser que c'est une string, ce qui peut poser un problème de cohérence
````

- typage basic

:small_red_triangle: Pour typé et lui définir un seul type mettre : aprés le a.

exemple : 
**ECRITURE EN TYPESCRIPT**

````typescript
let a: number = 3;
a = "3"; //l'interpreteur m'a me signidié une erreur car j'ai "typé" a en tant que number
a = 4; // est exact, et c'est valable pour tous (string, array etc...)
````
autre exemple:

````typescript
let b: number | string | boolean | array = 3;
````

:small_red_triangle: Si on essaie de lui insérer un autre genre de caractère, on aura un message d'erreur.

- typage avec plusieurs sortes


let nb: /number | string | = 3;


- typage avec

````typescript
let arr: number[];
arr = [1, 2, 3]; (exact) (faux)arr.push ("ok");
````

où alors faire :

````typescript
let arr: string [];
arr = [];
arr.push (["ok"]);
````

| ? (optionnel s'il est ajouté aprés un objet) |
|----------------------------------------------|

006.functions
-------------

[COURS 6](https://youtu.be/0iGLn7h73Hs)

exemple :

````typescript
function createUser(name, string, age : number, adress?);
  {name : string, age : number, adress : string & number}{
  return{
    name,
    age,
    adresse
  },
}
createUser(name : 'Jean, age : 12, adress : 12 'Bld');
````
007.enums
---------

[COURS 7](https://youtu.be/o__NKdLOq8g)

:small_red_triangle: énémuré une liste de constante :

````typescript
- enumDirection{
  TOP = "123";
  BOTTOM = "327";
  RIGHT = "567";
  LEFT = "234";
  }
````
- notre variable ne peut avoir qu'une constante énumérés de même nom

let userDirection: Direction = Direction.top;

- ouvrir la console et utilisé :
  
  
| tsc pour transpiller | 
|----------------------|
  


008.tuples
----------

[COURS 8](https://youtu.be/_p4Jn5tDNSU)

:small_red_triangle: Permet de typé une variable avec un tableau d'éléments fini.

````typescript
let arr: [number, string] = [12, "ok"];
````

009.literal types
-----------------

[COURS 9](https://youtu.be/8pexkpGCe5A)

:small_red_triangle: Type de caractéres déjà défini dans une variable où fonction :

let transition: 'ease in' | 'ease out' ;

transition = (soit l'un soit l'autre, mais pas les deux, ni aucun autre !)

void = rien

````typescript
function transforme X (transition: string) : void {
}
transform X(transition: "remplir");
````

010.types assertions
--------------------

[COURS 10](https://youtu.be/Osl7mcX0jbs)

:small_red_triangle: Pour déclarer à l'interpreteur que je sais exactement ce que c'est avec :

| as | 
|----|

où

| <> |
|----|

````typescript
let input: unknow = "24";
let var: string;

var1 = input as string;

où :

var1 = <string> input; //pour casser l'erreur et que l'instructeur accepte les données

où :

var1 = (input as any);
````

:small_red_triangle: Dans le cas où l'on connaît plus que typescript sur les données.

011.interfaces
--------------
[COURS 11](https://youtu.be/fJQZGKtMGTw)

:small_red_triangle: mot-clé : 

|interface|
|---------|

````typescript
interface IVehicule {
  marque: string;
  model: string | number;
  km: number;
  carburant: number;
  move(): boolean;
  color?: string;
}
let car: IVehicule;
car = {
  marque: "PEUGEOT".
  model: 307 "RX".
  km: 130000;
  carburant: essence;
  move: function(){
    return "true";
    }
  color: "grey";
};
````

autre exemple :

````typescript
interface MyFn{
(): boolean;
}
let MyFn: MyFn = function(){
  return true;
};
````

012.class
---------

[COURS 12](https://youtu.be/u3FkhyrHJCs)

- créer une class natif
- il faut mettre la propriété avant de l'enregistrer
- on peut ajouter des mots clés pour l'héritage avec typescript

````typescript
class Car {
    
   public model: string;// c'est un mot clé pour pouvoir accéder avec partout à tous les model(il existe aussi en private pour protéger notre code)
    
  constructor(model: string) {
      this.model = model
  }
}

let car = new Car(model: "RS3");
````  
- avec private nous devrons utilisé :

move (){
  console.log(this.model + 'moves');
car.move();

- pour accéder à l'objet

- il existe aussi protected

013.parameter properties
------------------------

[COURS 13](https://youtu.be/iwC6cAd2y4U)

[DIFFERENTES CLASSES EN TYPESCRIPT](http://typescriptlang.org/docs/handbook/2/classes.html)

````typescript

class Person {
  constructor(public name: string, public age: number) {
  }
};
let person: Person = new Person( name: 'Jean', age: 23);

console.log(typeof person, person.name,);

(transpiller)
````
:small_red_triangle: Terminal local

| tsc index.ts  | //pour transpiller du typescript en javascript
| node index.js | // pour executer du javascript 
|---------------|


014.import export modules
-------------------------

[COURS 14](https://youtu.be/u6hSND2wNcw)

:small_red_triangle: Depuis Ecma 2015, on peut dévelloper des modules, ce sont des blocks d'environnement qui sont 

étanches dans lesquelles on va avoir des variables, des fonctions, etc, ...et qui ne seront pas

utilisables ailleurs.

:small_red_triangle: **Typescript utilise également cette notion d'import/export.**

````typescript
interface ICar{
  model: string;
  km: number;
}
class Car implements  ICar{
  constructor public model: (string, public km: number){}
}
export{car, ICar};
export default car;

import car from './car'; (tout)

import * as fromCar from './car'; (une partie)

let car = nex fromCar.Car(model : R33, km : 3133);
(on stipule ce que l'on voudrait importé)
````

015.ts config
-------------

[COURS 15](https://youtu.be/gUeO7Vg3GWU)

| ts tsc => help                                                     |
| stipule les différentes commandes et comment les utilisés          |
| pour nettoyer le terminal on utilise "clear"                       |
| ts tsc -w(watch-pour écouter les modifications du fichier index.ts)|
| ts tsc --init                                                      |
|--------------------------------------------------------------------|

001. Typescript - Correction - Basic Types : https://youtu.be/5esacLP2Gqc
002. Typescript - Correction - Functions : https://youtu.be/4BmVoVAdnKs
003. Typescript - Correction - Enums : https://youtu.be/JO0IqnJ10mQ
004. Typescript - Correction - Literal types : https://youtu.be/a4Vnnxql19c
005. Typescript - Correction - Types assertions : https://youtu.be/3d0kIo1NqYM
006. Typescript - Correction - Interfaces : https://youtu.be/sX8GrX3UKFE
007. Typescript - Correction - Class : https://youtu.be/LgMYBsSVMzk
008. Typescript - Correction - Parameters properties : https://youtu.be/4oNNMmwSjvs
009. Typescript - Correction - Import et export modules : https://youtu.be/5oT7BaXR8CM

:small_red_triangle: Une petite doc sympathique pour typescript.
Ils expliquent comment l'encapsulation fonctionne et d'autre concept

[ENCAPSULATION](https://www.tutorialsteacher.com/typescript/data-modifiers)

TypeScript - Modificateurs de données
----------

Dans la programmation orientée objet, le concept d '«encapsulation» est utilisé pour rendre les membres de classe publics ou privés,

c'est-à-dire qu'une classe peut contrôler la visibilité de ses données membres. Cela se fait à l'aide de modificateurs d'accès.

Il existe trois types de modificateurs d'accès dans TypeScript: public, privé et protégé.

Publique
--------

Par défaut, tous les membres d'une classe dans TypeScript sont publics. Tous les membres du public peuvent être consultés n'importe où sans aucune restriction.
````typescript
class Employee {
public empCode: string;
empName: string;
}

let emp = new Employee();
emp.empCode = 123;
emp.empName = "Swati";
````
Dans l'exemple ci-dessus, empCodeet empNamesont déclarés comme publics. Ainsi, ils peuvent être accessibles en dehors de la classe en utilisant un objet de la classe.

Veuillez noter qu'aucun modificateur n'est appliqué auparavant empName, car TypeScript traite les propriétés et les méthodes comme publiques par défaut si aucun modificateur ne leur est appliqué.

privé
-----

Le modificateur d'accès privé garantit que les membres de la classe ne sont visibles que pour cette classe et ne sont pas accessibles en dehors de la classe contenant.
````typescript
Exemple: privé Copie
class Employee {
private empCode: number;
empName: string;
}

let emp = new Employee();
emp.empCode = 123; // Compiler Error
emp.empName = "Swati";//OK
````
Dans l'exemple ci-dessus, nous avons marqué le membre empCodecomme privé. Par conséquent, lorsque nous créons un objet empet essayons

d'accéder au emp.empCodemembre, cela donnera une erreur.

protégé
--------

Le modificateur d'accès protégé est similaire au modificateur d'accès privé, sauf que les membres protégés sont accessibles à l'aide

de leurs classes dérivantes.
````typescript
Exemple: protégé Copie
class Employee {
public empName: string;
protected empCode: number;

    constructor(name: string, code: number){
        this.empName = name;
        this.empCode = code;
    }
}

class SalesEmployee extends Employee{
private department: string;

    constructor(name: string, code: number, department: string) {
        super(name, code);
        this.department = department;
    }
}

let emp = new SalesEmployee("John Smith", 123, "Sales");
empObj.empCode; //Compiler Error
````
Dans l'exemple ci-dessus, nous avons une classe Employée avec deux membres, empNamepropriété publique et propriété protégée empCode.

Nous créons une sous SalesEmployee- classe qui s'étend à partir de la classe parente Employee. Si nous essayons d'accéder au membre 

protégé depuis l'extérieur de la classe emp.empCode, nous obtenons l'erreur de compilation suivante:

erreur TS2445: la propriété 'empCode' est protégée et accessible uniquement dans la classe 'Employee' et ses sous-classes.

En plus des modificateurs d'accès, TypeScript fournit deux autres mots-clés: readOnly et static. Découvrez-les ensuite.
