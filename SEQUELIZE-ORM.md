SEQUILIZE-ORM
=============

**ORM = Object Relationnal Mapping**

````typescript
const sequelize = new Sequelize({
database: 'superpizza',
dialect: 'mysql',
host: 'localhost',
username: 'root',
password: '',
});
````

001. Sequelize - Introduction => https://youtu.be/hyV7A6mScpo
     --------------------------------------------------------

Permet la persistence des données pour pouvoir les créer, manipuler, supprimer et récupérer. 

002. Sequelize - orm => https://youtu.be/5ZHTB3k5lbA
     -----------------------------------------------

▪ Sequelize est un ORM: Object-Relational Mapping.
▪ C'est un ensemble de classes permettant de manipuler les tables de bases de données relationelles.
▪ Interface d'accès à la base de données qui donne l'illusion de ne plus travailler avec des requêtes SQL, mais de
manipuler des objets.

![img_32.png](img_32.png)

003. Sequelize - sequelize => https://youtu.be/3UujbFFSUX0
     -----------------------------------------------------

[documentation sequilize](http://sequelize.org/)

▪ ORM Nodejs basé sur les promesses.
Aller sur la version V6 de Sequelize.
Getting started

npm install --save sequelize

Installer le Driver MySQL

npm install --save mysql2

004. Sequelize - installation => https://youtu.be/w6_vEJ3JYV4
     --------------------------------------------------------

````js
     const express = require("express");
     const cors = require("cors");
     const {Sequelize} = require("sequelize");

// Créer la connexion à MySQL
const sequelize = new Sequelize("app", "root", "motdepasseMySQLWorkBench", {
host: "127.0.0.1",
dialect: "mysql"

}); // ("base de donnée", "utilisateur", "mdp", {options})

// Tester la connection à MySQL
sequelize.authenticate().then(() => {
console.log("Connection successfull");
}).catch((error) => {
console.error("Unable to connect to the database", error);
});
control + C
node server.js
````
005. Sequelize - first connection => https://youtu.be/XO12rXPA5Pw
     ------------------------------------------------------------
     
     ![img_33.png](img_33.png)

````js
const express = require("express");
const cors = require("cors");
const {Sequelize} = require("sequelize");

// Créer la connexion à MySQL
const sequelize = new Sequelize("app", "root", "motdepasseMySQLWorkBench", {
host: "127.0.0.1",
dialect: "mysql"

}); // ("base de donnée", "utilisateur", "mdp", {options})

// Tester la connection à MySQL
sequelize.authenticate().then(() => {
console.log("Connection successfull");
}).catch((error) => {
console.error("Unable to connect to the database", error);
});
````
*rafraichir
````js
control + C
node server.js
````
006. Sequelize - définir le model user => https://youtu.be/p-vwZ2zpuIU
     -----------------------------------------------------------------

````js
     // Déclarer le model User

const User = sequelize.define("User",
{
firstname: {
type: DataTypes. STRING, // Ajouter DataTypes dans: const {Sequelize, DataTypes} = require("sequelize");
allowNull: false
},
lastname: {
type: DataTypes. STRING,
allowNull: false
},
email: {
type: DataTypes.STRING,
allowNull: false,
unique: true
},
note: {
type: DataTypes.INTEGER,
allowNull: false,
defaultValue: 0
},
}, {
sequelize,
tableName: "users"
createdAt: false, // pour désactiver car s'ajoute automatiquement
updatedAt: false, // désactiver
});
````
 
007. Sequelize - synchronisation des modeles => https://youtu.be/oRKgDPYVndc
     ------------------------------------------------------------------------

   *synchroniser le modèle user
````js
   // Rajouter une nouvelle colonne depuis l'api: FONCTIONNE AVEC L'OPTION ALTER
   presence: {
   type: DataTypes.STRING,
   allowNull: true

// Synchroniser le model User

User.sync({
alter: true // Vérifie l'état actuel de la table
})
.then(() => {
console.log("Sync User success");
})
.catch((error) => {
console.log("Sync User error :", error);
});
````
*synchroniser tous les modèles
````js
// Synchroniser tout les models

sequelize.sync({
alter:true
})
.then(() => {
console.log("Sync User success");
})
.catch((error) => {
console.log("Sync User error :", error);
});
````
     
008. Sequelize - enregistrer un utilisateur => https://youtu.be/OInF6uqRkLc
     ----------------------------------------------------------------------

*Aller sur la méthode POST
server.post("/users", (request, response) => {
console.log(request.body);
````js
    User.create({
        firstname: request.body.firstName,
        lastname: request.body.lastName,
        email: request.body.email
    })then((user) => {
        response.send({message: "Users has been saved!", user});
    }).catch((err) => {
        response.statusCode = 404;
        response.send({message: "User cannot be saved", err});
    });

});
````
*Sur insomnia
**Ajouter et modifier les users dans POST. Ca enregistrera directement dans Workbench.**

009. Sequelize - récupéré les utilisateurs => https://youtu.be/o9UFktSzKpQ
     ---------------------------------------------------------------------

*Aller sur la méthode get
server.get("/users", (request, response) => {
````js
    User.findAll()
        .then((users) => {
        response.send(users);
    }).catch((err) => {
        response.statusCode = 404;
        response.send("List not found : ", + err);
    });

});
````
▫ Selectionner uniquement certains attributs et ajouter une limite
````js
server.get("/users", (request, response) => {
// Recherche
User.findAll()
    
        // Réaction
        .then((users) => {
            attributes: ["id", "firstname"] // Selectionner attributs
            limit: 2 // Va limiter à 2 utilisateurs
            where: {
                firstname: "Steve" // Selectionner uniquement ces utilisateurs
            }
        response.send(users);
    }).catch((err) => {
        response.statusCode = 404;
        response.send("List not found : ", + err);
    });

});
````
010. Sequelize - trouver un utilisateur grace à sa clé primaire => https://youtu.be/LqYS2sPjJHM
     ------------------------------------------------------------------------------------------

*Aller sur la méthode get by id
````js
server.get("/users/:id", (request, response) => {
User.findByPk(request.params.id)
.then((user) => {
console.log(user);
if(!!user) { // n'est pas égal à null
response.send(user);
} else {
response.statusCode = 404;
response.send({message: "No users have been found"});
}
})
.catch((err) => {
response.statusCode = 404;
response.send("An error has occured");
});
````

011. Sequelize - concept clé étrangère => https://youtu.be/CfSBPHxHAbA
     -----------------------------------------------------------------

![img_34.png](img_34.png)

▪ MySQL prend en charge les clés étrangères qui permettent le croisement de données liées entre les tables et les
contraintes de clés étrangères qui aident à maintenir la cohérence des données associées.


Table parent : Contient des valeurs de la colonne initiale.

Table enfant : Avec des valeurs de colonnes qui référencient les valeurs de la colonne parent.

▪ Une contrainte de clé étrangère est défini sur la table enfant.


La premiere table users: Stock la liste d'utilisateur, qui sera la table parent de la relation.

La seconde table posts:  Stock la clé étrangère, ou Foreign Key. Aura en valeur l'id de l'utilisateur auquel
le post est associé sur la table users.

🔸 La clé étrangère users id sera associée à une contrainte qui maintient la cohérence des données associées.

▫ LISTE DES CONTRAINTES
-----------------------

▪ RESTRICT : Empêche la suppresion d'un utilisateur tant qu'il existe une clé étrangère faisant référence à cet utilisateur.

Pour pouvoir supprimer les utilisateurs, il faudra d'abord supprimer tout les enregistrements de la table post qui auront l'id de l'utilisateur

supprimé en tant que valeur de la colonne users id.

▪ CASCADE : Si on supprime un utilisateur, tout les articles associés à l'utilisateur seront supprimés en cascade.

Ni les utilisateurs, ni les articles dont il est l'auteur, ne seront encore présents après la suppression de l'utilisateur.

▪ SET NULL : Si on supprime un utilisateur, tout les articles qui lui sont associés vont être mis à jour.

Leur valeur pour la colonne users id passera à null. Les posts ne seront pas supprimés mais n'auront plus d'auteur.

▪ NO ACTION : Equivalent de RESTRICT. Le serveur MySQL rejette l'opération de supression

de mise à jour de la table parent s'il existe une valeur de clé étrangère associée dans la table référencée.

▪ SET DEFAULT : SET la valeur de users id avec la valeur par défaut de la colonne  users id.

🔸 Ces contraintes sont associées à des actions:

▪ ON UPDATE : Permet de déterminer le comportement de MySQL en cas d'une modification d'une référence.

▪ ON DELETE : Permet de déterminer le comportement de MySQL en cas d'une suppression d'une référence.

012. Sequelize - has many => https://youtu.be/jb0OfjXLwXQ
     -----------------------------------------------------

▫ Déclarer le modèle Post
````js
// Déclarer le modèle Post, columns: title, text

const Post = sequelize.define("Post", {
title: {
type: DataTypes.STRING,
allowNull: false,
},
text: {
type: DataTypes.TEXT,
allowNull: false,
}
}, {
sequelize,
tableName: "posts",
createdAt: false,
updatedAt: false,
});
````
▫ Définir la relation: User has many Post (peut avoir plusieurs)

````js
// Définir la relation: User has many Post (peut avoir plusieurs)

User.hasMany(Post, {
as: "posts",
foreignKey: "fk_author_id",
sourceKey: "id",
constraints: true,
onDelete: "cascade",
});
````
▫ Récupérer le Post utilisateur

````js
server.get("/users", (request, response) => {
User.findAll()
// Récupérer les Posts des utilisateurs
include: [{model: Post, as: "posts"}]
.then((users) => {
response.send(users);
}).catch((err) => {
response.statusCode = 404;
response.send("List not found : ", +err);
});

});    
````
     
013. Sequelize - manipuler les relations => https://youtu.be/x_oKE4DPVfI
     -------------------------------------------------------------------

````js
User.findByPk(request.params.id)
.then( async (user) => {

            if (!!user) { 
                
                // Créer un nouveau post à chaque requête
              
                const post = await Post.create({
                    title: "Title",
                    text: "Text"
                });

                // L'associé à l'utilisateur
              
                await user.addPost(post);
                const posts = await user.getPosts();

                response.send({user, posts});
            } else {
                response.statusCode = 404;
                response.send({message: "No users have been found"});
            }
        })
        .catch((err) => {
            response.statusCode = 404;
            response.send("An error has occured");
        });

````
014. Sequelize - corrections => https://youtu.be/zL-gXLlwWAI
     -------------------------------------------------------
PK = Primary Key

[lien Coraline](https://gitlab.com/Coraline.I/api-create-nodejs/-/blob/master/express-api/server.js)

![img_54.png](img_54.png)
