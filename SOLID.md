#SOLID
------

**En programmation orientée objet, SOLID est un acronyme mnémonique qui regroupe cinq principes de conception destinés**

**à produire des architectures logicielles plus compréhensibles, flexibles et maintenables. Les principes sont un**

**sous-ensemble de nombreux principes promus par l'ingénieur logiciel et instructeur américain Robert C. Martin.**

**Bien qu'ils s'appliquent à toute conception orientée objet, les principes SOLID peuvent également former une philosophie**

**de base pour des méthodologies telles que le développement agile ou le développement de logiciels adaptatifs.**

**La théorie des principes SOLID a été introduite par Martin dans son article Design Principles and Design Patterns de 2000,**

**bien que l'acronyme SOLID ait été introduit plus tard par Michael Feathers1.**

Principes
---------
:small_red_triangle: **Responsabilité unique (Single responsibility principle)**

une classe, une fonction ou une méthode doit avoir une et une seule responsabilité

:small_red_triangle: **Ouvert/fermé (Open/closed principle)**

une entité applicative (class, fonction, module ...) doit être fermée à la modification directe mais ouverte à l'extension

:small_red_triangle: **Substitution de Liskov (Liskov substitution principle)**

une instance de type T doit pouvoir être remplacée par une instance de type G, tel que G sous-type de T,

sans que cela ne modifie la cohérence du programme

:small_red_triangle: **Ségrégation des interfaces (Interface segregation principle)**

préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale

:small_red_triangle: **Inversion des dépendances (Dependency inversion principle)**

il faut dépendre des abstractions, pas des implémentations
