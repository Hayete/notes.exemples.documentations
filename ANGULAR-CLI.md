#ANGULAR CLI
============

![img.png](img.png)


:black_medium_small_square: [CLIQUER ICI POUR DOCUMENTATION](https://github.com/angular/angular-cli)

:black_medium_small_square: [SUPER SITE](https://scotch.io/courses/build-your-first-angular-website/introduction)

##024. Angular - Effectuer des requêtes http à notre api => https://youtu.be/lfYgoIRwSag
--------------------------------------------------------------------------------------

:small_red_triangle: Pour bien comprendre reprendre la correction de l'exercice sur angular
Comment effectuer des requêtes http sur notre api distant où sur une api open-source.

:small_red_triangle: Dans le terminal :

*api cd express-api-correction
* express-api-correction node server.js
  (Server is listenning at http://localhost : 8080)
  (ps. 8080 est le localhost de notre ordinateur)

:small_red_triangle: Puis dans Insomnia :

GET Users => l'api me retourne tous les utilisateurs

:small_red_triangle: app module.ts

````typescript
import {BrowserModule} from '@angular/plateforme-browser';
import {NgModule} from '@angular/core';

import {AppRouting} from  './app.routing';
import {COMPONENTS, AppComponent} from "./component";

import {HTTPClientModule} from "@angular/common/http";

@NgModule({
  declarations:[
    ...COMPONENTS
  ],
imports : [
  BrowserModule,
  AppRouting,
  HTTPClientModule
],
providers : [],
Boostrap : [AppComponent]
});

expert : class AppModule {}

````
:small_red_triangle: collaborators.components.ts

````typescript
import {Component, OnInit} from '@angular/core';

@Component{
    selector: 'app-collaborators';
    TemplateUrl: './collaborators.component.html';
    styleUrl: ['.collaborators.component.scss']
}
export class CollaboratorsComponent Inplements OnInit {
    constructor(private http: HttpClient){}//injecter un service
  public ngOnInit(){
        this.http.get(id: 'http://localhost: 8080/users') suscribe(next : this(res : object) => Network
    //C'est pour décrire ma souscription
    //On peut vérifier dans la console =>network la requête http
    
  }
}
````
:small_red_triangle: Quand les localhost sont différents installez le cors( npm install cors), puis mettre à jour le serveur.
:black_medium_small_square: [DOCUMENTATION ET INSTALLATION CORS](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS)

La méthode GET

La méthode GET est utilisée par le navigateur pour demander au serveur de renvoyer une certaine ressource. "Hé le serveur, je veux cette ressource." Dans ce cas, le navigateur envoie un corps vide. Du coup, si un formulaire est envoyé avec cette méthode, les données envoyées au serveur sont ajoutées à l'URL.

Considérons le formulaire suivant :

````html
<form action="http://foo.com" method="get">
  <div>
    <label for="say">Quel salut voulez-vous adresser ?</label>
    <input name="say" id="say" value="Hi">
  </div>
  <div>
    <label for="to">À qui voulez‑vous l'adresser ?</label>
    <input name="to" value="Mom">
  </div>
  <div>
    <button>Send my greetings</button>
  </div>
</form>
```` 

Comme nous avons utilisé la méthode GET, vous verrez l'URL www.foo.com/?say=Hi&to=Mom apparaître dans la barre du navigateur quand vous soumettez le formulaire.

##025. Angular - Envoyer des données au serveur avec la méthode post => https://youtu.be/2LfchNU6m3A
----------------------------------------------------------------------------------------------------
La méthode POST

La méthode POST est un peu différente.C'est la méthode que le navigateur utilise pour demander au serveur une réponse prenant en compte les données contenues dans le corps de la requête HTTP : « Hé serveur ! vois ces données et renvoie-moi le résultat approprié ». Si un formulaire est envoyé avec cette méthode, les données sont ajoutées au corps de la requête HTTP.

Exemple :

c'est le même formulaire que celui que nous avons vu pour GET ci‑dessus, mais avec post comme 

valeur de l'attribut  method.

````html
<form action="http://foo.com" method="post">
  <div>
    <label for="say">Quel salut voulez‑vous adresser ?</label>
    <input name="say" id="say" value="Hi">
  </div>
  <div>
    <label for="to">À qui voulez‑vous l'adresser ?</label>
    <input name="to" value="Mom">
  </div>
  <div>
    <button>Send my greetings</button>
  </div>
</form>
````
Quand le formulaire est soumis avec la méthode POST, aucune donnée n'est ajoutée à l'URL et la requête HTTP ressemble à ceci, les données incorporées au corps de requête :

POST / HTTP/1.1
Host: foo.com
Content-Type: application/x-www-form-urlencoded
Content-Length: 13

say=Hi&to=Mom
:small_red_triangle: La CLI Angular crée, gère, construit et teste vos projets Angular. 

:small_red_triangle: L'objectif de DevKit est de fournir un large ensemble de bibliothèques 

pouvant être utilisées pour gérer, développer, déployer et analyser notre code.

:small_red_triangle: Pour démarrer localement,  il faut suivre ces instructions:

1) créez une fourchette de ce dépôt .
2) Clonez sur l'ordinateur local en utilisant git.
3 s'assurer que Node JS est installé. 
4) s'assurer que vous avez installé yarn ; npm install --global yarn
5) Exécuter yarn(sans arguments) à partir de la racine de notre clone de ce projet pour installer
  les dépendances.

:small_red_triangle: Construction et installation de la CLI
  
 - Pour créer une version globale:

|npm install -g @ angular / cli|
|------------------------------|

:small_red_triangle: pour obtenir une installation globale de la dernière version de la CLI.

Ensuite, l'exécution d'une ngcommande dans l'exemple de projet trouvera et utilisera 

automatiquement la version locale de l'interface de ligne de commande.

:small_red_triangle: Remarque: Si vous testez ng update, sachez que l'installation de toutes les

archives tar mettra également à jour le framework ( @angular/core) vers la dernière version.

Dans ce cas, installez simplement la CLI seule avec 

|npm install -D ${CLI_REPO}/dist/_angular_cli.tgz|
|------------------------------------------------|

, de cette façon le reste du projet reste à mettre à niveau avec ng update.

:small_red_triangle: Débogage

Pour déboguer un appel de la CLI, créez et installez la CLI pour un exemple de projet , puis

exécutez la ngcommande souhaitée en tant que:

|node --inspect-brk node_modules / .bin / ng ...|
|-----------------------------------------------|

Cela déclenchera un point d'arrêt au démarrage de la CLI. Vous pouvez vous y connecter en 

utilisant les mécanismes pris en charge pour votre IDE, mais l'option la plus simple consiste à

ouvrir Chrome sur chrome: // inspectez, puis cliquez sur le inspect lien de la 

node_modules/.bin/ngcible Node.

:small_red_triangle: Malheureusement, les require()autres fichiers de l'interface de ligne de

commande sont dynamiquement en cours d'exécution, de sorte que le débogueur n'est pas au courant

de tous les fichiers de code source à l'avance. Par conséquent, il est difficile de placer des

points d'arrêt sur les fichiers avant que la CLI les charge. La solution de contournement la plus

simple consiste à utiliser l' debugger;instruction pour arrêter l'exécution dans le fichier qui

vous intéresse, puis vous devriez pouvoir vous déplacer et définir des points d'arrêt comme prévu


:small_red_triangle: Il existe deux suites de tests différentes qui peuvent être exécutées localement:

- Tests unitaires

Exécutez tous les tests: 

|yarn bazel test //packages/...|
|------------------------------|

Exécutez un sous-ensemble des tests, utilisez l'exemple de cible Bazel complet:

|yarn bazel test //packages/schematics/angular:angular_test|
|----------------------------------------------------------|

Pour une liste complète des cibles de test, utilisez la requête Bazel suivante: 

|yarn bazel query "tests(//packages/...)"|
|----------------------------------------|

Vous pouvez trouver plus d'informations sur le débogage des tests avec Bazel dans la documentation.

- Tests de bout en bout

Courir: 

|node tests/legacy-cli/run_e2e.js|
|--------------------------------|

Exécutez un sous-ensemble des tests: 

|node tests/legacy-cli/run_e2e.js              |
|tests/legacy-cli/e2e/tests/i18n/ivy-localize-*|
|----------------------------------------------|

Lors de l'exécution des commandes de débogage, Node s'arrêtera et attendra qu'un débogueur se

connecte. Vous pouvez attacher votre IDE au débogueur pour vous arrêter sur les points d'arrêt

et parcourir le code. Consultez également Utilisation spécifique de l'EDI pour une histoire

de débogage plus simple.

Lors du débogage d'un test spécifique, modifiez describe()ou it()vers fdescribe() et fit()pour

concentrer l'exécution sur ce seul test. Cela gardera la sortie propre et accélérera 

l'exécution en n'exécutant pas de tests non pertinents.

Utilisation spécifique de l'IDE

Quelques conseils supplémentaires pour développer dans des IDE spécifiques.

Intellij IDEA / WebStorm

Pour charger le projet dans les produits Intellij, il suffit Opendu dossier du référentiel. Ne

le faites pas Import Project , car cela écrasera la configuration existante.

Une fois ouvert, l'éditeur devrait détecter automatiquement les configurations d'exécution

dans l'espace de travail. Utilisez le menu déroulant pour choisir celui à exécuter, puis 

cliquez sur le Run bouton pour le démarrer. Lors de l'exécution d'une cible de débogage,

assurez-vous de cliquer sur l' Debugicône pour attacher automatiquement le débogueur (si vous

cliquez sur Run, Node attendra éternellement qu'un débogueur se connecte).

Configurations d'exécution d'Intellij IDEA

Créer de nouveaux packages

Ajouter un package à ce référentiel signifie exécuter deux commandes distinctes:

schematics devkit:

|package PACKAGE_NAME|
|--------------------|

 Cela mettra à jour le .monorepofichier et créera les fichiers de base pour le nouveau package

 (package.json, src / index, etc.).

|devkit-admin templates|
|----------------------|

Cela mettra à jour le README et tous les autres fichiers de modèle qui pourraient avoir changé

lors de l'ajout d'un nouveau package.

Pour les packages privés, vous devrez ajouter une "private": trueclé à votre package.json 

manuellement. Cela nécessitera de réexécuter le script d'administration du modèle.

026. Angular - Gérer nos requêtes grâce aux services => https://youtu.be/QOfnBat7Q6Y
------------------------------------------------------------------------------------

[CLIQUER ICI POUR DOCUMENTATION](https://github.com/angular/angular-cli)


