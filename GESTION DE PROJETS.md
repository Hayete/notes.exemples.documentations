#GESTION DE PROJETS
-------------------
001 | Gestion de projet - Les méthodes dev => https://youtu.be/uUbL9QIlyug
--------------------------------------------------------------------------
###CYCLE EN V
--------------
![img_35.png](img_35.png)

###METHODE AGILE
----------------

![img_37.png](img_37.png)

002 | Gestion de projet - Team agile => https://youtu.be/Oz8UI8YAI_o
--------------------------------------------------------------------
Trello est un outil de gestion de projet en ligne, lancé en septembre 2011 et inspiré par la méthode Kanban de Toyota.
Il repose sur une organisation des projets en planches listant des cartes, chacune représentant des tâches. Wikipédia
Date de sortie initiale : 2011
Lancement : septembre 2011
Propriétaire : Atlassian
Type de site : Outil de gestion de projet
Langue : Multilingue (23 langues)

003 | Gestion de projet - Utilisation de Trello => https://youtu.be/XK8nT4C75SE
-------------------------------------------------------------------------------
backlog?
installation de trello:

Net consult=>Promo 5=> Mon premier projet=> cocher: visible par les membres de l'équipe=> Tableau=> Ajouter une liste

Tableau
-------

- To do
  *ajouter une carte : nommer Fonctionnalité 1
  *possibilité de faire une description(est compatible avec le markdown) enregistrer
  *on peut mettre un commentaire en mettant un @ pour un(où des) membre de la team
- To Do this sprint
- Doing
- Done/To test
- To test by scrumMaster
- Validated

Pour ajouter une liste entre deux listes, cliquer entre les deux et entrer le nom de la liste.
On peut ajouter des cartes dans les listes déplaçables etc

Etiquette
---------
Pour assigner par couleur, telle activité à telle personne.
Pour rechercher par exemple une activité en particulier, cliquer sur la couleur.

Check-list
----------
notification de la liste des choses à faire

exemple :
je suis user et je veux visualiser le formulaire d'inscription

donc on va cocher :
FRONT/FORMULAIRE

On peut ajouter des chek-list et les cocher pour les envoyer.
Si on a besoin d'envoyer des fichiers, on va les chercher dans l'ordinateur où autre, selectionner et envoyer.

Cartes
------
comment lier les cartes entre elles :

Dans le formulaire(chek-list) on va ajouter un élément button :

on avait créer :

(input)Nom
(input)Prénom
(input)Email
(input)Message
Submit button

Puis créer une carte appelée "Submit button"

Pour lier aller sur pièce jointe=>trello=>ajouter l'url => puis cliquer sur le lien créer pour se déplacer.

On peut déplacer une liste en cliquant dessus et en la déplaçant.

En allant sur la carte on peut copier la carte, déplacer, etc
