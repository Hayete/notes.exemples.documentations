CLEAN ARCHITECTURE
------------------
[LIEN VIDEO DE COURS](https://youtu.be/r_FQLW5DFfs)

:small_red_triangle: **Est un processes qui nous permet de coder avec méthode et rigueur notre programme et de vraiment comprendre le travaille qu'on réalise.**

:small_red_triangle: **C'est un rassemblement de processes qui permet à ce que notre code reste lisible à pour toutes les parties prenantes.**

1) Ouvrir un nouveau projet(gitab puis cloner) ne pas oublier de mettre l'énoncé dans le **readme**.
   
2) Aller sur l'IDE(en l'occurence webstorm) et npm install pour avoir le package json(j'ai réussi avec npm install --yes)

3) yarn add -D typescript (dossier typescript)

4) yarn add -D tsc (compileur pour typescript)

5) tsc --init (s'il y a un message d'erreur taper simplement tsc init)

6) créer un dossier : src, puis un sous-dossier : domain, puis sous-dossier de domain : entities, puis encore sous-dossier d'entitiies : User.ts

7) dans le sous-dossier User.ts :

````typescript
export class user {
  name: string;
  
  constructor(
      name: string.
    ) {
      this.name = name;
  }
}
````
8) tsc --init (pour voir si tout vas bien et qu'il n'y a pas eu d'erreur)

9) installer un moteur de test pour un rapide feedback yarn add -D jest @types/jest ts-jest

10) pour instancier et configurer automatiquement jest sur le projet : jest init puis on tombe sur une commande line interface(cli)

11) on obtient les tests (sauf pour module) 

12) sous src (je ne sais pas comment il l'a eu automatiquement) il y aura un sous-dossier _test_, puis un sous-dossier de celui-ci : user.unit.test.ts

13) 
  ````typescript
    
import {user} from "../domaine/entities/User"; 
describe(name 'User unit testing', fn() => {
    if (name 'Sould create a user', fn() => {
        const user = new User (name 'Barack');
        expect(user.name).toEqual(expected 'Barack');
  })
})
    
````
14) test running sur run(réponse : jest n'est pas capable de lire de la donnée il lui faut un compilateur)

15) jest --init (réponses aux questions : yes, no, node, no, v8, no)

16) dans le fichier test.config.js : 
````js
module.exports = {
    preset: 'ts-jest',
}
````
17) run dans run

18) Pour matérialiser une action ouvrir un dossier : usercases

19) sous-dossier d'usercases : CreateUser.ts
````typescript
export class CreateUser{
    execute(){
}
}

````
20) créer un dossier models sous entities puis un fichier Usercase.ts 
````typescript
export interface Usecase<Trequest, Tresponse> { //on type en ts avec les chevrons
  execute(request: Trequest): Promise<Tresponse>;
}
````
21) dans CreateUser.ts
````typescript
import{Usercase} from "../domaine/models/Usercase";
import {user} from "../domaine/entities/User";

export class CreateUser implements Usecase<string, User> {
    async execute(name: string) {
        return newUser (name);
    }
}
````
22) dans user.unit.test.ts
````typescript
import {user} from "../domaine/entities/User";
describe(name 'User unit testing', fn() => {
 if (name 'Sould create a user', fn async () => {
  const createuser = new await CreateUser.execute (name: 'Barack');
  expect(createUser.name).toEqual(expected 'Barack');
})
})
````
23) run pour test

[LIEN VERS Partie 2](https://youtu.be/LkE_qha0GKE)
-------------------------------------------------
