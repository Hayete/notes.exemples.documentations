#ANGULAR
========

![img_6.png](img_6.png)

[DOCUMENTATION](https://fr.wikipedia.org/wiki/AngularJS)

##000- arrow-function--le-mot-clé-this--loprateur-new--https://youtube6n4hz9jpcew
----------------------------------------------------------------------------

| Le premier 'this' fait référence à l'objet, le second à l'objet global (fenêtre).|
|----------------------------------------------------------------------------------|

 NOTATION POUR ÉCRIRE UNE ARROW FUNCTION
````js
 let arrow = () => { 

 };                                      
````

![img_21.png](img_21.png)

###001.Angular-Culture :  https://youtu.be/AFyW-uGxHlc
--------------------------------------------------------- 
- Framework JS permet de développer des singles pages d'application côté client.
- acronyme SPA: DEVELOP ACROSS ALL PLATFORMS.
- angular js/ MISCOEVERY en 2010.
- elle perd le js à partir de la version 2 angular.
- google soutient et sponsorise angular.
- utilisez typescript avec angular.
- execution sur l'ordinateur où téléphone de l'utilisateur.
- une page est une petite coquille html et tout le reste est du js.
- l'ordinateur où autre du client n'a pas accès à notre base de données.
- notre application single page doit communiquer avec un serveur, via une API et c'est ce serveur là qui va se 
  charger d'enregistrer où de lire des données où réaliser des calculs qui ne sont pas possible par
  l'ordinateur où le téléphone de l'utilisateur.
- angular est utilisé pour des projets d'application web.
- interface angular :
- dashboard afin de récupérer des données et les afficher sur une interface.
- peut-être téléchargeable.
- inconvénient :
- site web sur du contenu.
- il existe des applications qui ne sont pas capable d'exécuter ce script-là.
- le référencement, certains moteurs de recherche ne sont pas capable d'exécuter ce script.
- (site web PHB) pour le dynamiser c'est quasiment impossible, car le fichier est très lourd.


###002. Angular - Setup : https://youtu.be/Hhg5iKRv9pw
------------------------------------------------------ 
Installation :
- angular mp install -g angular/cli
- puis commande ng
- ng new appl
- routing? y pour yes
- quel format de style = SASS (préf. de Jean)
- cd pour rentrer dans le dossier

###003. Angular - Presentation de la structure du projet :  https://youtu.be/S6IHK2iFZcs
----------------------------------------------------------------------------------------     
- e2e = test endtwend = test côté navigateur
-  informations relatives aux projets
- pour arrêter le processus : contrôle + C
- ing serve
- ng run build
###004. Angular - Install bootstrap : https://youtu.be/g-z_LSE7EBM
------------------------------------------------------------------     
- installer boostrap -> documentation boostrap
- download -npm install boostrap 

###005. Angular - Notre premier composant : https://youtu.be/KEqbZzY9H2Y
------------------------------------------------------------------------ 
composant appl -> fichier.composant, pour séparer les fichiers composant : clic droit sur app ->new directory

- componments
  
- app
  
- shift pour sélectionner tout les fichiers et les déplacer dans app

- Un component est une class qu'on export et qui est annoté d'une directive.

- Créer un nouveau dossier components appelé home.

-Créer 3 fichiers: home.component.ts, home.component.html et home.component.scss.

Dans home.component.ts, taper:
````typescript
import {Component} from '@angular/core';

@Component({
selector: 'app-home',
templateUrl: './home.component.html',
styleUrls: ['./home.component.scss']
})
export class HomeComponent {

}
````
- Ouvrir app.module.ts et ajouter:

-  En dessous des autres imports
````typescript
import {HomeComponent} from './components/home/home.component';
````
-  Dans déclarations
HomeComponent
Grâce au selector (dans home.components.ts), on peut sélectionner app-home et le coller dans un autre
component.html.
<app-home></app-home>
Toujours un component lorsqu'on le crée.

On peut aussi créer un component grâce au Terminal.

Commande:ng generate component namecomponent
Raccourcis: ng g c.
Le composant sera automatiquement déclaré.

###006. Angular - Interpolation property binding : https://youtu.be/WvqbK-_L160
------------------------------------------------------------------------------- 
Interpollation = c'est le fait d'afficher le contenu d'une variable dans le html

C'est le fait d'afficher le contenu d'une variable dans le html.
🔻 AppComponent.html
// Syntaxe

<h1>HTML</h1>

{{ title }}

// On va placer title dans le h1.

<h1>{{ title }}</h1>
🔻 AppComponent.ts

````typescript
export class AppComponent {
    title: string = "Interpolation et Property Binding";
}
````
// Créer une fonction dans la partie export du dossier component.ts qui va retourner title.
````typescript
getTitle(): string {
return this.title;
}
````
🔻 AppComponent.html

<h1>{{ title }}</h1>

<h2>{{ getTitle() }}</h2>

Le Title sera affiché dans la page internet. C'est donc l'interpolation.
🔻 AppComponent.html

<h1>{{ title }}</h1>

<h2>{{ getTitle() }}</h2>

<h2>[innerText]="title"</h2> // [] signifie qu'on va le lier à une variable.

C'est le Property Binding.


Lier le style d'un élément html
🔻 AppComponent.html


<div [style.backgroundColor]="bgColor"></div>
🔻 AppComponent.ts
bgColor:string = "red";


Lier un objet
🔻 AppComponent.html


<div [style.backgroundColor]="bgColor">{{ user.name }}</div>

🔻 AppComponent.ts

````typescript
bgColor:string = "red";
user: {name: "string"} = {name: "Jean"};
````
###007. Angular - Event binding : https://youtu.be/27ZaLRwBsOc
-------------------------------------------------------------- 
C'est la liaison des évenements dans Angular.
Pour mettre à jour le html, il va écouter toutes les modifications qui sont faites dans le composant, et pour lui dire de mettre
à jour des actions, on peut lui lier des evenements.

###008. Angular - Two way data binding : https://youtu.be/dqIo328Rp2s
---------------------------------------------------------------------     
C'est la liaison des données à double sens.

###009. Angular - Structural directive ngIf : https://youtu.be/kuJLIVPito4
-------------------------------------------------------------------------- 
Permet d'écrire des conditions dans notre DOM, pour supprimer ou ajouter des élements dans notre DOM.

###010. Angular - Structural directive ngfor : https://youtu.be/E2KwZMS6KyI
---------------------------------------------------------------------------     
La directive structurelle ngFor permet de répéter dans le DOM.

###011. Angular - Mise en pratique : https://youtu.be/26sDYakDeg0
-----------------------------------------------------------------     


###012. Angular - Découpage des composants : https://youtu.be/v3sH2--lxVc
-------------------------------------------------------------------------     
Dans dossier components, créer dossier task.
Insérer task.component.ts dans dossier task.

###013. Angular - Partager des données à un composant enfant - Input decorator : https://youtu.be/GrWJZYdpnfA
-------------------------------------------------------------------------------------------------------------    
Pour pouvoir recevoir les données du parent, il faut taguer les propriétés avec un décorateur:

@Input, qui est importé depuis le fichier angular/core.

###014. Angular - Émettre des données au composant parent : https://youtu.be/tzPiI8_dAoc
----------------------------------------------------------------------------------------     

###015. Modules : https://youtu.be/sK_IDLCwz_k
---------------------------------------------
1. Modules

- Un module dans une application, sert à compartimenter notre code.


- Sur une application web, chaque page peut-être un module.


- Angular a au moins un module racine, qui aura ses propres dépendances, ses composants.


- Pour définir un module, on utilisera le paramètre: @NgModule.


- Il prendra un objet en paramètre:


    * declarations, qui permet de déclarer des Components, Pipes, Directives.

    *imports: Permet d'importer les modules.

    * providers: Permet de définir les services qui vont être injecter dans notre composant.

    * exports: Permet d'exporter des services

- bootstrap: Va indiquer à Angular d'encapsuler notre module dans un composant.

🔻 app.modules.ts

````typescript
@Ngmodule({
déclarations: [
AppComponent, // Component, Pipe, Directives
],
imports: [
BrowserModule,
AppRoutingModule,
],

providers: [], // Services
exports: [],
bootstrap: [Appcomponent]
})

export class Appmodule {

}
````
Créer un fichier modules.
Insérer dossier users + fichier users.module.ts.

🔻 users.modules.ts :
````typescript
import {NgModule} from '@angular/core';

// Indiquer que cette classe sera un module
@NgModule({
declarations: [
UsersComponent
],

})
````
export class UserModule {}

2. Routing

Angular est pour rappel un framework de single page application. Une page sera chargée et ensuite executera le code
javascript. Developper plusieurs page html sera compliqué. Grâce au système de routing, c'est Angular qui va se
charger de simuler des pages html.
🔻 app.modules.ts

Il y a 4 routes:

- Contact

- Users/info
  
- Users
  
- ""

Pour afficher le 1er component, il faudra alors indiquer à Angular où mettre les components.

Aller dans le template html app.component.html.
Place le routing à l'intérieur:

<router-outlet></router-outlet>
On a indiqué à Angular que les components inscrits dans la const routes, si l'url match avec l'url de la page, les
components sont inscrit à cette partie du html.


▫ NAVBAR

Aller sur bootstrap: Prendre une navbar.
Modifier chaque liens avec le nom au choix (dashboard, users, infos, contact).

▪ Sur Angular, href="#NomFichier.html" qui servait à naviguer entre chaque page, est remplacé
par routerLink="/NameComponent".
▪ Penser à déplacer la balise router avant ou après balise app-navbar.
🔻 app.component.html
<router-outlet></router-outlet>

<app-navbar></app-navbar>

Activer le lien de chaques components dans la navbar par: routerLinkActive="active":

🔻 navbar.component.ts
`````typescript`
<li class="nav-item"routerLink="/contact" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">
   <a class="nav-link">Contat</a>
</li>
````
▪ Les router doivent être placés dans la partie nav-item et non pas nav-link.
▪ Le dashboard, qui hérité du router "/", doit être différentier des autres link.

Ajouter dans nav-item: [routerLinkActiveOptions]="{exact: true}".

▪ Le lien sera actif seulement lorsqu'il y aura le router: "/" uniquement.

````typescript
<li class="nav-item"routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">
   <a class="nav-link">Dashboard</a>
</li>
```` 
▫ EN BREF
````html
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item"routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">
          <a class="nav-link">Dashboard</a>
        </li>

        <li class="nav-item"routerLink="/users" routerLinkActive="active">
          <a class="nav-link">Users</a>
        </li>

        <li class="nav-item"routerLink="/users/info" routerLinkActive="active">
          <a class="nav-link">Info</a>
        </li>

        <li class="nav-item"routerLink="/contact" routerLinkActive="active">
          <a class="nav-link">Contact</a>
        </li>
        
      </ul>
  </div>
</nav>
````
3. Routing : Params

Toutes les informations que l'on va passer dans l'URL:


localhost:4200/users/12
localhost:4200/blog/post-du-jour

▫ Rajouter des utilisateurs dans Users et mettre en place une configuration pour pouvoir passer desparamètres dans l'URL

Dans navbar.component.html: Retirer le lien info.
Dans app.module.ts: Remplacer le {path: 'users/info'...} par 'users/:id'.

🔻 app.module.ts
const routes: Routes = [
// Supprimer 'users/info'
{path: 'users/info', component: InfoComponent},

        // Remplacer par:
{path: 'users/:id', component: InfoComponent},
];

Pour définir un paramètre, on préfixe par :.
On peut aussi avoir une URL avec seulement le paramètre.

EXEMPLE

'users/:id'
':id'

Ajouter un nouveau tableau d'objet dans users.component.ts.

🔻 users.component.ts
users: {id: number, name: string}[] = [
{id: 1, name: "John"},
{id: 2, name: "Sam"}
]

Afficher l'utilisateur en utilisant boucle ngFor dans users.component.html.

🔻 users.component.html
````html
<ul>
  <li *ngFor="let user of users">{{ user.id }}. {{ user.name}}</li>
</ul>
````
▫ Lorsqu'on clique sur un user, on veut changer d'url, et mettre l'url dans l'id de l'utilisateur

Rajouter un attribut routerLink:

🔻 users.component.html
````html
<ul>
  <li *ngFor="let user of users" [routerLink]=[users.id]>
    {{ user.id }}. {{ user.name}}</li>
</ul>
````
En cliquant sur un des 2 users, on sera redirigé vers:
localhost:4200/users/1

ou

localhost:4200/users/2

▫ Ajouter un bouton retour depuis la page info, pour revenir à la page user
🔻 info.component.html
<button class="btn btn-primary" routerLink="..">Retour</button>
// Revient en arrière
ou

<button class="btn btn-primary" routerLink="/users">Retour</button>
// Revient à la page users

▫ Affichier l'ID de l'utilisateur
🔻 info.component.html
<p>ID de l'utilisateur: {{ id }}</p>

<button class="btn btn-primary" routerLink="/users">Retour</button>
On veut récupérer le 2 de l'url et l'afficher à la place de {{ id }}.

Aller dans le component, et ajouter à constructor une private route: ActivatedRoute.
Placer un public id: string;.

🔻 info.component.ts
export class InfoComponent implements OnInit {

public id: string;

constructor(private route: ActivatedRoute) {}

ngOnInit() {
console.log(this.route.snapshot.params.id);
this.id = this route.snapshot.params.id;
}
}

VISUEL


L'ID de l'utilisateur sera maintenant affichée lorsqu'on clique sur le user.

###016. Routing  : https://youtu.be/4G_Uzu1KVGA
-----------------------------------------------

▪ On peut changer l'URL de manière programmatique.

Au bout de 3 secondes, on redirige vers les infos de l'utilisateur 1.

🔻 users.component.ts
````typescript
export class UsersComponent implements OnInit {

users: { id: number, name: string }[] = [
{id: 1, name: "John"},
{id: 2, name: "Sam"}
];

constructor() {
}

ngOnInit() {

      setTimeout(() => {
          // Au bout de 3 secondes on redirige
          // vers les infos de l'utilisateur 1
      }, 3000);
      
      public showInfo(id: number) {
          
    }
}
}
````
🔻 users.component.html

A la place du routerLink, on va écouter un click.

<ul>
  <li *ngFor="let user of users" (click)="showInfo(user.id)">
    {{ user.id }}. {{ user.name}}</li>
</ul>

Pour rediriger, indiquer dans le constructor un router.

🔻 users.component.ts
constructor(private router: Router) {
}
▪ Ne pas oublier d'importer la class Router.

On peut maintenant appeler le router pour pouvoir naviguer avec navigateByUrl().
````typescript
import Router

export class UsersComponent implements OnInit {

users: { id: number, name: string }[] = [
{id: 1, name: "John"},
{id: 2, name: "Sam"}
];

constructor(private router: Router) {
}

ngOnInit() {

      setTimeout(() => {
          // Au bout de 3 secondes on redirige
          // vers les infos de l'utilisateur 1
        this.router.navigateByUrl('/users/1');
      }, 3000);
      
      public showInfo(id: number) {
          console.log('/users/' + id);
    }
}
}
````
▫ Bouton retour de manière programmatique
🔻 info.component.html

On retire le routerLink et on remplace par l'écoute d'évenement (click).
On lui ajoute une fonction goBack().

<p>ID de l'utilisateur: {{ id }}</p>

<button class="btn btn-primary" (click)="goBack()">Retour</button>

Déclarer cette fonction goBack().
Injecter dans constructor le router.
Dans public goBack(): On peut utiliser fonctionnalité qui sont dans router avec this.navigate.
Indiquer qu'on retourne en arrière avec: ['..'].

🔻 info.component.ts
````typescript
export class InfoComponent implements OnInit {

public id: string;

constructor(
private router: Router, // injecter le router
private route: ActivatedRoute) {}

ngOnInit() {
console.log(this.route.snapshot.params.id);
this.id = this route.snapshot.params.id;
}

public goBack() { // déclarer la fonction goBack
this.router.navigate(['..']) // naviguer
}
}
````
VISUEL

5. Component lifecycle

export class LifecycleComponent implements Onchanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecker, OnDestroy {}
1️⃣ ngOnChanges:

1ère méthode appliquée. Permet d'écouter les modifications des propriétés définies par entrées du
component. On va pouvoir écouter les modifications de tout les @Input.
Executé avant ngOnInit.

2️⃣ ngOnInit: Executer du code pour initialiser le component.
3️⃣ ngDoCheck:

Appelé directement après ngOnChanges et ngOnInit.
Détecte et agit sur les changements qu'Angular ne peut ou ne déteste pas par lui même.

4️⃣ ngAfterContentInit:

Executé après le ngDoCheck.
Executé au moment ou Angular va commencer à injecter le template dans le component.

5️⃣ ngAfterContentChecked: Une fois que le contenu a été injecté et checké, Angular va lancer cette méthode.
6️⃣ ngAfterViewInit: La view va être rendue. L'initialisation du rendu de la vue une fois checkée va lancer cette méthode.
7️⃣ ngAfterViewChecked: Une fois la vue checkée, cette méthode sera lancée.
8️⃣ ngOnDestroy:

Pour supprimer des souscriptions ou nettoyer le component.
Méthode utilisé lorsque le component va être détruit.


6. Programmation reactive avec la librairie RxJs

Une programmation réacrive c'est le fait de réagir à des évènements.
Ca nous permet de gérer facilement des flux de données.

EX: Si on clique sur un bouton, à chaque fois qu'on cliquera sur ce bouton, on lancera une fonction.
RxJS: Librairie réactive X en Javascript.
Observable: Flux de données.


Créer dossier rxjs.


Ajouter fichiers index.html et index.js.


Aller sur https://github.com/ReactiveX/rxjs.


Installer via CDN: Copier https://unpkg.com/rxjs/bundles/rxjs.umd.min.js dans le html.


🔻 index.html
// Dans head

<script !src="https://unpkg.com/rxjs/bundles/rxjs.umd.min.js"></script>

// Le projet est maintenant importé dans le html.
Note: Importer les script js à la fin du body.
🔻 index.js
// Avoir accès à la variable rxjs
console.log(rxjs)

// On a maintenant accès à tout les éléments de la librairie.

// Créer un flux de donnée:

const flux = new Observable(function (observer) {
observer.next(1); // on renvoie 1 donnée
observer.next(10); // envoie 10
observer.next(100); // envoie 100
});

// Pour envoyer de nouvelles données:

setTimeout(() => {
observer.next(20); // Renvoie 20 données toutes les 2 secondes
observer.complete(); // l'observer sera complêté
}, 2000);

// Souscrire au flux:

const abonnement = flux.subscribe(
function (value) {},
function (error) {},
function (complete) {}

);

abonnement.unsubscribe(); // on peut se déshabonner du flux de donnée
unsubscribe: Lorsque le flux de donnée est détruit, si on se déshabonne pas, les observables auxquels on a souscrit
dans notre component vont continuer à tourner.

7. Services et injection de dependances


Cours vidéo : Ici.


Un service est une classe qui peut être réutilisé dans plusieurs composants.

On utilise un service pour:

Faire des appels http
Ressortir un code et le rendre réutilisables à plusieurs composants.
Manager des données




▫ Créer un nouveau service

Dans Terminal: ng g service / ng generate service.

###016. Routing  : https://youtu.be/4G_Uzu1KVGA
-----------------------------------------------     

###017. Routing : Params  : https://youtu.be/CX3lbqsAtzA
--------------------------------------------------------     

###018. Routing : Router service  : https://youtu.be/3Hna49OPdEU
----------------------------------------------------------------     

###019. Component lifecycle  : https://youtu.be/rEu4Ct8MrWY
------------------------------------------------------------     

###020. Programmation réactive avec la librairie RxJs : https://youtu.be/Vkp6_ZrF0W4
------------------------------------------------------------------------------------     

###021. Services et injection de dépendances : https://youtu.be/wFgjhxzdeZk
---------------------------------------------------------------------------     

###022. Services et observables : https://youtu.be/m2gHSnI2CPc
--------------------------------------------------------------     

###023. Angular - Reactive Forms - PARTIE 1 : https://youtu.be/uTTxz-xCpew
--------------------------------------------------------------------------     

###023.1. Angular - Reactive Forms - PARTIE 2 : https://youtu.be/iMuQEK9TEhc
----------------------------------------------------------------------------



