MONGO-DB et MONGOOSE
====================
**C'est un système de gestion de base de données, orienté document, le package Mongoose permet de connecter Nodejs avec MongoDB.**

MongoDB (de l'anglais humongous qui peut être traduit par « énorme ») est un système de gestion de base de données orienté documents,

répartissable sur un nombre quelconque d'ordinateurs et ne nécessitant pas de schéma prédéfini des données. Il est écrit en C++. 

Le serveur et les outils sont distribués sous licence SSPL, les pilotes sous licence Apache et la documentation sous licence Creative Commons4.

Il fait partie de la mouvance NoSQL.

   
Historique
----------
MongoDB est développé depuis 2007 par MongoDB. Cette entreprise travaillait alors sur un système de Cloud computing, informatique à données largement

réparties, similaire au service Google App Engine de Google.

Il est depuis devenu un des SGBD les plus utilisés, notamment pour les sites web de Craigslist, eBay, Foursquare, SourceForge.net, Viacom, pages jaunes

et le New York Times5.

Licence
-------
MongoDB Community Server

Depuis octobre 2018, MongoDB est publié sous la Server Side Public License (SSPL), une licence développée par le projet. Elle remplace la GNU Affero

General Public License, et est presque identique à la GNU General Public License version 3, mais exige que ceux qui rendent le logiciel disponible

au public dans le cadre d'un "service" doivent rendre l'ensemble du code source du service disponible sous cette licence.6-7 La SSPL a été soumise

pour certification à l'Open Source Initiative mais a ensuite été retirée devant les réticences de l'OSI. 8 Les pilotes de langue sont disponibles

sous une Licence Apache. En outre, MongoDB Inc. propose des licences propriétaires pour MongoDB. Les dernières versions sous licence AGPL version 3

sont la 4.0.3 (stable) et la 4.1.4.

MongoDB a été supprimé des distributions Debian, Fedora et Red Hat Enterprise Linux en raison du changement de licence. 

Fedora a considéré que la version 1 de la SSPL n'est pas une licence de logiciel libre parce qu'elle est "intentionnellement conçue pour être

agressivement discriminatoire" envers les utilisateurs commerciaux.9,10

Principales caractéristiques
----------------------------

Données manipulées

MongoDB permet de manipuler des objets structurés au format BSON (JSON binaire), sans schéma prédéterminé. En d'autres termes, des clés peuvent être

ajoutées à tout moment « à la volée », sans reconfiguration de la base.

Les données prennent la forme de documents enregistrés eux-mêmes dans des collections, une collection contenant un nombre quelconque de documents.

Les collections sont comparables aux tables, et les documents aux enregistrements des bases de données relationnelles. Contrairement aux bases de

données relationnelles, les champs d'un enregistrement sont libres et peuvent être différents d'un enregistrement à un autre au sein d'une même

collection. Le seul champ commun et obligatoire est le champ de clé principale ("id"). Par ailleurs, MongoDB ne permet ni les requêtes très complexes

standardisées, ni les JOIN, mais permet de programmer des requêtes spécifiques en JavaScript.

Pour des données mises en page de la manière suivante :
-------------------------------------------------------
ID 	Nom 	Prénom 	Âge
6 	DUMOND 	Jean 	43
7 	PELLERIN 	Franck 	NULL
8 	MARTIN 	Emile 	62
9 	KING 	NULL 	51

Le code correspondant dans le fichier de stockage d'une table (par exemple csv) d'un tableur (par exemple Calc de LibreOffice) est :
------------------------------------------------------------------------------------------------------------------------------------

ID,Nom,Prénom,Âge
6,DUMOND,Jean,43
7,PELLERIN,Franck
8,MARTIN,Emile,62
9,KING,,51

On remarque qu'une valeur peut être manquante (NULL), mais que chaque ligne comporte le même nombre de champs.

Différemment, pour la même mise en page (par exemple une page .php dans un navigateur web) une collection NoSQL peut présenter des champs 

différents à chaque ligne (et même imbriquer plusieurs champs dans une ligne) :

{
"_id": ObjectId("4efa8d2b7d284dad101e4bc7"),
"Nom": "DUMOND",
"Prénom": "Jean",
"Âge": 43
},

{
"_id": ObjectId("4efa8d2b7d284dad101e4bc8"),
"Nom": "PELLERIN",
"Prénom": "Franck",
"Adresse": "1 chemin des Loges",
"Ville": "VERSAILLES"
},

{
"_id": ObjectId("4efa8d2b7d284dad101e4bc9"),
"Nom": "KING",
"Âge": "51",
"Adresse": "36 quai des Orfèvres",
"Ville": "PARIS"
}

On remarque tout de suite que des champs nouveaux ont été ajoutés dans les enregistrements les plus récents, sans impact sur l'enregistrement le 

plus ancien. C'est ce qui fait la spécificité des NoSQL. On remarque également que ce stockage sous forme « attribut="valeur" » est très proche 

du XML. Par ailleurs, une sauvegarde de base de données ((en)dump) en langage de requête SQL peut également prendre cette forme « attribut="valeur" »,

mais elle n'est exploitable que recréée sous forme de tables (dans un SGBD).

Les documents d'une collection MongoDB peuvent comporter des champs différents (note : le champ "_id" est un champ obligatoire, généré et ajouté 

par MongoDB, c'est un index unique qui permet d'identifier le document).

Dans un document, des champs peuvent être ajoutés, supprimés, modifiés et renommés à tout moment. Contrairement aux bases de données relationnelles,

il n'y a pas de schéma prédéfini. La structure d'un document est très simple et se compose de paires clef/valeur à la manière des tableaux associatifs,

la clef est le nom du champ, la valeur son contenu (voir à ce propos le format JSON). Les deux sont séparés d'un signe deux-points ":" comme le 

montre l'exemple ci-dessus. Une "valeur" peut être un nombre ou du texte, mais aussi une donnée binaire (comme une image) ou une collection d'autres

paires clefs/valeurs comme le montre l'exemple ci-dessous :

{
"_id": ObjectId("4efa8d2b7d284dad101e4bc7"),
"Nom": "PELLERIN",
"Prénom": "Franck",
"Âge": 29,
"Adresse":
{
"Rue" : "1 chemin des Loges",
"Ville": "VERSAILLES"
}
}

Ici sont imbriqués des documents : le champ "Adresse" contient un document de deux champs : "Rue" et "Ville".

Certaines opérations sur les champs telles que l'incrémentation peuvent être effectuées de façon atomique, c'est-à-dire sans déplacement ni copie du

document.

Manipulation des données
------------------------
MongoDB est livré avec des liaisons pour les principaux langages de programmation :

    C
    C++
    Dart
    Erlang
    Go
    Haskell
    Java
    JavaScript
    .NET (C# F#, PowerShell, etc)
    Perl
    PHP
    Python
    Ruby
    Scala

Ces pilotes permettent de manipuler la base de données et ses données directement depuis ces langages. Cependant MongoDB possède également un outil

qui peut être utilisé en ligne de commande et qui donne accès au langage natif de la base de données : le JavaScript, par l'intermédiaire duquel on

peut également manipuler la base.

En tapant :

| ./mongo |
|---------|

dans la ligne de commande du système d'exploitation, on lance l'interpréteur de commandes interactif de MongoDB.

Dans une base de données nommée vente qui contient plusieurs collections, voici comment on afficherait tous les documents de la collection nommée clients :
----------------------------------------------------------------------------------------------------------------------------------------------------------
> use vente           // Sélectionne la base de données "vente"
> db.clients.find();  // Cherche et affiche tous les documents de la collection "clients".

Le résultat s'imprime à l'écran :
---------------------------------
{ "_id": 28974, "Nom": "ID Technologies", "Adresse" : "7 Rue de la Paix, Paris"}
{ "_id": 89136, "Nom": "Yoyodine", "Adresse" : "8 Rue de la Reine, Versailles"}

La documentation officielle de MongoDB décrit en détail les mécanismes de manipulation de données par l'intermédiaire de l'outil mongo.

Pour manipuler les bases MongoDB depuis un langage de programmation, il convient en revanche de se reporter à la documentation correspondant au

pilote (driver) du langage en question.

Solutions basées sur MongoDB
----------------------------
Insertion d'un enregistrement dans MongoDB avec Robomongo 0.8.5.

Le site MongoDB Tools (indépendant de l'entreprise MongoDB) référence différentes solutions utilisant la base MongoDB11.

Un exemple notable d'application cliente de gestion de ce système de gestion de base de données est Robo 3T (Anciennement Robomongo)12.

Utilisation comme système de fichiers

MongoDB peut être utilisé comme système de fichiers, cette fonction assez inhabituelle pour une base de données permet pourtant de 

profiter de toutes les caractéristiques décrites dans la section Déploiement, à savoir la réplication et la répartition de données sur

un ensemble de serveurs, et de les mettre à profit pour gérer des fichiers.

Cette fonction, nommée GridFS, est incluse dans les pilotes fournis avec MongoDB et utilisable sans difficulté particulière dans les 

langages de programmation. Le programmeur dispose de fonctions pour manipuler les fichiers et leur contenu, MongoDB se chargeant de leur gestion.

Dans un système fonctionnant sur plusieurs serveurs MongoDB, les fichiers peuvent ainsi être automatiquement répartis et dupliqués entre 

les ordinateurs de façon transparente, offrant au système la tolérance aux pannes et la répartition de la charge que MongoDB apporte aux données.

Positionnement
--------------

Les bases de données hiérarchiques ont été développées pour les matériels peu puissants des années 1970.

Ceux des années 1990, plus puissants, ont pu utiliser le modèle relationnel (bien plus consommateur, mais beaucoup plus souple).

Les années 2010 permettent l'introduction des bases not-only-SQL ("NoSQL" : pas-seulement-SQL) comme MongoDB associant aux données 

des attributs (ou champs) placés à la demande en temps réel, et exploitables ensuite par le biais de requêtes. 

Chacune de ces deux approches tire profit de la baisse des coûts et de l'augmentation de puissance du matériel pour permettre des développements 

plus simples et plus rapides.

Popularité
----------

Selon db-engines.com13, MongoDB occupe la 5e place dans le classement des systèmes de gestion de bases de données les plus populaires en mars 2017,

tous types confondus, et la première place pour les systèmes de gestion de bases de données NoSQL 

(en nombre de résultats de recherches Google et Bing, et en nombre de requêtes Google).

Déploiement
-----------

MongoDB prend en charge la réplication via un modèle maître-esclave à des fins de résistance aux pannes et de répartition de la charge.

En pratique MongoDB peut tourner sur plusieurs ordinateurs, en répartissant ou en dupliquant les données.

Il est ainsi possible de répartir les données sur plusieurs machines pour répartir la charge de travail, mais il est également possible

de dupliquer les données de chaque ordinateur sur un ou plusieurs autres ordinateurs afin de garder le système de base de données 

opérationnel même en cas d'une panne de l'un d'eux. MongoDB rend d'ailleurs ces configurations aisées à mettre en place en les automatisant.

De cette façon, il est tout à fait possible d'ajouter à la volée un ou des ordinateurs à une base de données en cours de fonctionnement.

Performance et surveillance (monitoring)
----------------------------------------

**Métriques à surveiller**

Des problèmes de performance de MongoDB peuvent avoir un impact important sur les applications utilisant cette base de données 

dans leur infrastructure en production. Afin de les éviter, il est indispensable de surveiller les métriques (statistiques) 

clés fournies par MongoDB, que vous utilisiez MongoDB avec WiredTiger14, maintenant par défaut, ou bien MMAPv115 :

    Débit / opérations :
        Nombre de requêtes de lecture de données,
        Nombre de requêtes d'écriture de données,
        Nombre de clients effectuant avec des opérations de lecture de données en cours ou en attente,
        Nombre de clients effectuant avec des opérations d'écriture de données en cours ou en attente ;
    Performance de la base de données :
        Taille de l'oplog,
        Fenêtre de l'oplog,
        Lag de réplication,
        « Headroom » de réplication,
        Statut des membres du réplica set ;
    Utilisation des ressources :
        Nombre de clients connectés au serveur de la base de données,
        Nombre de connexions non utilisées disponibles pour les nouveaux clients ;
    Saturation des ressources :
        Nombre de requêtes de lecture de données en attente,
        Nombre de requêtes d'écriture de données en attente ;
    Erreurs (asserts).

**Collection des métriques**

Toutes ces statistiques peuvent être collectées de trois façons16 :

    En utilisant les utilitaires comme mongostat ou mongotop. 

    Les utilitaires, offerts par MongoDB, peuvent fournir en temps réel des statistiques à propos des clusters MongoDB 

    et sont particulièrement utiles pour vérifier sur le moment l'état de la base de données.

    Avec des commandes lancées directement sur le moteur de base de données, via un client,
    Au travers d'outils de surveillance

001 | MongoDB - introduction => https://youtu.be/zqwAOjVwCFM
-------------------------------------------------------------

MANG stack = mongo db, express js, angular où react, node js

Avec ces quatre technologies basées sur javascript, il est possible de couvrir la totalité des composants nécessaire à la création de site web

dynamique où d'application web.

Il est possible de dévellopper un projet complet javascript sur trois niveau, un dévelloppeur fullstack doit savoir maîtrisé ces compétences 

afin de créer un produit fini.

niveau 1) affichage avec angular où react

niveau 2) application avec express et node.js

niveau 3) base de données avec mongo db
 
002 | MongoDB - présentation de mongodb => https://youtu.be/1CGmzHLxs2M
-----------------------------------------------------------------------

- mango est orienté document

- format de données bson => json qui est un élément essentiel à la syntaxe javascript

- évite les problèmes de conversion entre les objets du web et les bases de données

- dynamique pour les données non structurées et ne sont pas relationnelles

- magasin de documents, de clé valeur, graphique où colonne large

003 | MongoDB - présentation de mongoose => https://youtu.be/2MEXa_KttOY
------------------------------------------------------------------------

[LIEN VERS MONGOOSE](https://mongoosejs.com/)

npm install mongoose --save

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});

const Cat = mongoose.model('Cat', { name: String });

const kitty = new Cat({ name: 'Zildjian' });
kitty.save().then(() => console.log('meow'));

004 | MongoDB - installation de mongodb => https://youtu.be/VDPsAIrxY7M
-----------------------------------------------------------------------

FAIT => inclure mongo compass

Commencer
---------
Assurez-vous d'abord que MongoDB et Node.js sont installés.

Ensuite, installez Mongoose à partir de la ligne de commande en utilisant npm:

**$ npm install mongoose --save**

Maintenant, disons que nous aimons les chatons flous et que nous voulons enregistrer chaque chaton que nous rencontrons dans MongoDB. 

La première chose que nous devons faire est d'inclure la mangouste dans notre projet et d'ouvrir une connexion à la testbase de données sur notre

instance exécutée localement de MongoDB.

// getting-started.js
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true, useUnifiedTopology: true});

Nous avons une connexion en attente à la base de données de test exécutée sur localhost. Nous devons maintenant être avertis si nous nous connectons

avec succès ou si une erreur de connexion se produit:

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
// we're connected!
});

Une fois notre connexion ouverte, notre rappel sera appelé. Par souci de concision, supposons que tout le code suivant se trouve dans ce rappel.

Avec Mongoose, tout est dérivé d'un schéma . Faisons référence et définissons nos chatons.

const kittySchema = new mongoose.Schema({
name: String
});

Jusqu'ici tout va bien. Nous avons un schéma avec une propriété name, qui sera un String. L'étape suivante consiste à compiler notre schéma dans un

modèle .

const Kitten = mongoose.model('Kitten', kittySchema);
Un modèle est une classe avec laquelle nous construisons des documents. Dans ce cas, chaque document sera un chaton avec des propriétés et des 

comportements tels que déclarés dans notre schéma. Créons un document chaton représentant le petit gars que nous venons de rencontrer sur le 

trottoir à l'extérieur:

const silence = new Kitten({ name: 'Silence' });
console.log(silence.name); // 'Silence'
Les chatons peuvent miauler, alors voyons comment ajouter la fonctionnalité «parler» à nos documents:

// NOTE: methods must be added to the schema before compiling it with mongoose.model()
kittySchema.methods.speak = function () {
const greeting = this.name
? "Meow name is " + this.name
: "I don't have a name";
console.log(greeting);
}

const Kitten = mongoose.model('Kitten', kittySchema);
Les fonctions ajoutées à la methodspropriété d'un schéma sont compilées dans le Modelprototype et exposées sur chaque instance de document:

const fluffy = new Kitten({ name: 'fluffy' });
fluffy.speak(); // "Meow name is fluffy"
Nous avons des chatons qui parlent! Mais nous n'avons toujours rien sauvegardé dans MongoDB. Chaque document peut être enregistré dans la base de 

données en appelant sa méthode de sauvegarde . Le premier argument du rappel sera une erreur le cas échéant.

fluffy.save(function (err, fluffy) {
if (err) return console.error(err);
fluffy.speak();
});
Disons que le temps passe et que nous voulons afficher tous les chatons que nous avons vus. Nous pouvons accéder à tous les documents sur les chaton

grâce à notre modèle Kitten .

Kitten.find(function (err, kittens) {
if (err) return console.error(err);
console.log(kittens);
})
Nous venons de connecter tous les chatons de notre base de données à la console. Si nous voulons filtrer nos chatons par nom, Mongoose prend en 

charge la syntaxe de requête riche de MongoDB .

Kitten.find({ name: /^fluff/ }, callback);
Cela effectue une recherche pour tous les documents avec une propriété de nom qui commence par "fluff" et renvoie le résultat sous la forme d'un 

tableau de chatons au rappel.

Toutes nos félicitations
C'est la fin de notre démarrage rapide. Nous avons créé un schéma, ajouté une méthode de document personnalisée, enregistré et interrogé des chatons

dans MongoDB à l'aide de Mongoose. Consultez le guide ou la documentation sur l'API pour en savoir plus.
````typescript
// Installer express et cors
const express = require('express');
const cors = require('cors');

const server = express();
const PORT = 8080;

server.use(cors({
origin: "*",
optionsSuccessStatus: 200,
}));

// CRUD Company

// L => List
server.get('/companies', (req, res) => {
const companies = ["WR"]; // Sauvegarde une liste de compagnie
res.send(companies);
});

// C => Create
server.get('/companies/create', (req, res) => {
res.send({categories: []});
});

// Sauvegarde ou insertion en bdd
server.post('/companies', (req, res) => {
res.send({message: "Company created successfully!"});
});

// R => Read => Show
server.get('/companies/:id', (req, res) => {
const company = {};

    res.send(company);
});

// U => Update
server.get('/companies/edit/:id', (req, res) => {
const id = req.params.id;
const company = {};

    res.send({company, categories: []});
});

// Sauvegarder ou mettre à jour la compagnie
server.put('/companies/edit/:id', (req, res) => {
res.send({message: "Company updated successfully!"});
})

// D => Delete
server.delete('/companies/:id', (req, res) => {
res.send({message: "Company deleted successfully!"});
});

server.listen(PORT, '127.0.0.1', function () {
console.log('Server running on http://localhost:' + PORT);
});
````
005 | MongoDB - préparation d'une nouvelle api => https://youtu.be/YsSabmoHH2U
------------------------------------------------------------------------------

npm install mongoose --save
````typescript
// Ajouter l'installation mongoose
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// Créer nouvelle connexion
mongoose.connect('mongodb://127.0.0.1/mean', {useNewUrlParser: true});

const db = mongoose.connection;
db.on('error', (event) => {
console.log("error :", event);
});

db.once('open', () => {
console.log("Successfully connected!");
});
````

006 | MongoDB - créer une connection => https://youtu.be/qEyq39SEcrc
--------------------------------------------------------------------

[LIEN COMPASS](https://www.mongodb.com/products/compass)

007 | MongoDB - télécharger compass pour visualiser les données => https://youtu.be/2hFbTj9BHJc
-----------------------------------------------------------------------------------------------

FAIT

008 | MongoDB - définir un modèle => https://youtu.be/eFXMqtJpD9c
-----------------------------------------------------------------

Créer fichier models > Company.js.
````js
const mongoose = require('mongoose');

// Créer un schéma
const TagSchema = new mongoose.Schema({
name: String
});

// Document
const CompanySchema = new mongoose.Schema({
name: String,
email: {
type: String,
default: 'ok@gmail.com',
required: true,
},
description: String,
date: Date,
tags: [TagSchema]
});

mongoose.model('User', mongoose.Schema({
firstname: String,
email: String,
password: String,
}));

// Créer un model et le rendre exportable

module.exports = mongoose.model("Company", CompanySchema);
[Retour au Sommaire]
````
009 | MongoDB - créer une company => https://youtu.be/Yvph1MfPWRA
-----------------------------------------------------------------
````js
// .json à déclarer pour le body
server.use(express.json());

server.post('/companies', (req, res) => {

    const company = new Company(req.body); // Ne pas oublier de déclarer le package.json

    company.save().then(() => {
        res.send({message: "Company created successfully!"});
    }).catch(() => {
        res.status(500).send({message: "Error!"});
    });
});
````
010 | MongoDB - récupérer la liste des compagnies => https://youtu.be/Z8dsYXwjW54
---------------------------------------------------------------------------------

▫ Find
````js
Model.find()
// L => List
server.get('/companies', (req, res) => {
Company.find( (err, companies) => {
res.send(companies);
});
});
// Get sur Insomnia pour récupérer les id
````
▫ FindById
````js
Model.findById()
// L => List
server.get('/companies', (req, res) => {
const query = Company.find();
query.select("id name description");
query.where("name").equals("WR");
query.limit(4);
query.exec((err, companies) => (
res.send(companies);
});
````
▫ Détailler la recherche
Model.findById()
````js
// L => List
server.get('/companies', (req, res) => {
const query = Company.findById( "604d167a8f2a098b5ef18db4",(err, company) => {
res.send(company);
});
});
````
011 | MongoDB - afficher les informations d'une compagnie => https://youtu.be/keFMuJCcInw
-----------------------------------------------------------------------------------------
````js
Model.findById()
// R => Read => Show
server.get('/companies/:id', (req, res) => {
const company = {};
const id = req.params.id;
Company.findById(id, (err, company) => {
res.send(company);
});
});

// Utiliser par exemple cet id : "604d167a8f2a098b5ef18db4"
````
012 | MongoDB - mise à jour d'une compagnie => https://youtu.be/Lz9DDTJKyWk
----------------------------------------------------------------------------
````js
Model.findById()
// U => Update
server.get('/companies/edit/:id', (req, res) => {
const id = req.params.id;
const company = {};

    Company.findById(id, (err, company) => {
res.send(company);
});
});
Model.findByIdAndUpdate()
// Sauvegarder ou mettre à jour la compagnie
server.put('/companies/:id', (req, res) => {

    if(!req.body.name) {
        res.status(404).send({message: "Name required!"});
    }


    const id =req.params.id;
    Company.findByIdAndUpdate(id, req.body,{new: true},(err, company) => {

        if(!!err) {
            res.status(404).send({message: "Company not found!"});
        }

        res.send({message: "Company updated successfully!", company});
    });
});
````
013 | MongoDB - refactoring router => https://youtu.be/lHEaJUjh42w
-------------------------------------------------------------------
C'est la réorganisation, la retouche du code sans y apporter de modifications.
▪ Connection à la base de données
````js
mongoose.connect('mongodb://127.0.0.1/mean', {useNewUrlParser: true});

const db = mongoose.connection;

db.on('error', (event) => {
console.log("error :", event);
});
db.once('open', () => {
console.log("Successfully connected!");
});

const server = express();
const PORT = 8080;
▪ Gestion des middleware
const server = express();
const PORT = 8080;
▪ Définition et création de la logique
server.get('/companies', (req, res) => {
Company.find((err, companies) => {
res.send(companies);
});
});

// C => Create
server.get('/companies/create', (req, res) => {
res.send({categories: []});
});

// Sauvegarde ou insertion en bdd
server.post('/companies', (req, res) => {

    const company = new Company(req.body);

    company.save().then(() => {
        res.send({message: "Company created successfully!"});
    }).catch(() => {
        res.status(500).send({message: "Error!"});
    });
});

// R => Read => Show
server.get('/companies/:id', (req, res) => {
const company = {};
const id = req.params.id;
Company.findById(id, (err, company) => {
res.send(company);
});
});

// U => Update
server.get('/companies/edit/:id', (req, res) => {
const id = req.params.id;
const company = {};

    Company.findById(id, (err, company) => {
res.send(company);
});
});

server.put('/companies/:id', (req, res) => {

    if(!req.body.name) {
        res.status(404).send({message: "Name required!"});
    }


    const id =req.params.id;
    Company.findByIdAndUpdate(id, req.body,{new: true},(err, company) => {

        if(!!err) {
            res.status(404).send({message: "Company not found!"});
        }

        res.send({message: "Company updated successfully!", company});
    });
});

// D => Delete
server.delete('/companies/:id', (req, res) => {
res.send({message: "Company deleted successfully!"});
});
▪ Lancer le serveur
server.listen(PORT, '127.0.0.1', function () {
console.log('Server running on http://localhost:' + PORT);
});
````
Définition et création de la logique: Dossier router + fichier CompaniesRouter.js.
````js
// Importer express
const express = require('express');

// Créer un router, importer Company et exporter une nouvelle constante prefix
const router = express.Router();
const Company = require('../models/Company');

const prefix = '/companies';

// => bas de page

// Exporter le router
module.exports = {prefix, router};


// Retirer maintenant tout les 'companies' des path.
````
014 | MongoDB - refactoring controller => https://youtu.be/9g_OuFXE7e4
-----------------------------------------------------------------------
Dossier http + dossier Controllers + fichier CompaniesControllers.

▪ Ce nouveau fichier va servir à déplacer toute la logique du fichier router en créant des méthodes.
▪ Il restera donc dans le fichier router, que les les méthodes et paths.
▪ CompaniesController sera alors implémenté dans les méthodes du fichier CompaniesRouter.
🔻 CompaniesRouter.js
````js
const express = require('express');
const router = express.Router();
const CompaniesController = require('../http/Controllers/CompaniesController');

const prefix = '/companies';

router.get('/', CompaniesController.index);
router.get('/create', CompaniesController.create);
router.post('/', CompaniesController.store);
router.get('/:id', CompaniesController.show);
router.get('/edit/:id', CompaniesController.edit);
router.put('/:id', CompaniesController.update);
router.delete('/:id', CompaniesController.delete);


module.exports = {prefix, router};
🔻 CompaniesController.js
const Company = require('../../models/Company');

class CompaniesController {

    index(req, res) {
        Company.find((err, companies) => {
            res.send(companies);
        });

        // Détailler la recherche

// server.get('/', (req, res) => {
//     const query = Company.find();
//     query.select("id name description");
//     query.where("name").equals("WR");
//     query.limit(4);
//     query.exec((err, companies) => (
//         res.send(companies);
// });
}

    create(req, res) {
        res.send({categories: []});
    }

    store(req, res) {
        const company = new Company(req.body);

        company.save().then(() => {
            res.send({message: "Company created successfully!"});
        }).catch(() => {
            res.status(500).send({message: "Error!"});
        });
    }

    show(req, res) {
        const company = {};
        const id = req.params.id;
        Company.findById(id, (err, company) => {
            res.send(company);
        });
    }

    edit(req, res) {
        const id = req.params.id;
        const company = {};

        Company.findById(id, (err, company) => {
            res.send(company);
        });
    }

    update(req, res) {
        if(!req.body.name) {
            res.status(404).send({message: "Name required!"});
        }


        const id =req.params.id;
        Company.findByIdAndUpdate(id, req.body,{new: true},(err, company) => {

            if(!!err) {
                res.status(404).send({message: "Company not found!"});
            }

            res.send({message: "Company updated successfully!", company});
        });
    }

    delete(req, res) {
        res.send({message: "Company deleted successfully!"});
    }

}

module.exports = new CompaniesController();
````
015 | MongoDB - refactoring la connexion à la base de données => https://youtu.be/Abcohn1qv5w
---------------------------------------------------------------------------------------------
Fichier index.js

````js
require('./database');
require('./server');
🔻 server.js
const express = require('express');
const cors = require('cors');

const CompaniesRouter = require('./router/CompaniesRouter');
const server = express();
const PORT = 8080;

server.use(express.json());

server.use(cors({
origin: "*",
optionsSuccessStatus: 200,
}));

server.use(CompaniesRouter.prefix, CompaniesRouter.router);



server.listen(PORT, '127.0.0.1', function () {
console.log('Server running on http://localhost:' + PORT);
});

module.exports = server;
````
server.js
![img_29.png](img_29.png)
index.js
![img_28.png](img_28.png)
CompagniesRouters.js
![img_30.png](img_30.png)
database.js
![img_31.png](img_31.png)
