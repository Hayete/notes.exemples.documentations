#HTML
=====

:black_medium_small_square: [DOCUMENTATION](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language)

##INTRODUCTION
--------------

:small_red_triangle: Le HyperText Markup Language, généralement abrégé HTML ou dans sa dernière version HTML5, est le

langage de balisage conçu pour représenter les pages web.

Ce langage permet :

    d’écrire de l’hypertexte, d’où son nom,

    de structurer sémantiquement la page,

    de mettre en forme le contenu,

    de créer des formulaires de saisie,

    d’inclure des ressources multimédias dont des images, des vidéos, et des programmes

    informatiques, de créer des documents interopérables avec des équipements très variés de 

    manière conforme 

    aux exigences de l’accessibilité du web.

REFERENCEMENT = POSITION DANS LE MOTEUR DE RECHERCHE

NE PAS OUBLIER DE METTRE UN TITRE DANS LE HEAD (POUR ETRE BIEN REFERENCER)

:small_red_triangle: Il est souvent utilisé conjointement avec le langage de programmation JavaScript et des feuilles

de style en cascade (CSS). HTML est inspiré du Standard Generalized Markup Language (SGML). Il

s'agit d'un format ouvert.

###STRUCTURE D'UNE PAGE HTML
----------------------------

| taper ! puis tabulation pour avoir une page html déjà prête |
|-------------------------------------------------------------|

````html

````

ASTUCE : taper par exemple h1, puis tabulation pour avoir automatiquement les chevrons. Pour intégrer des images on a le choix 

entre créer un dossier assets(directory) et y insérer des images où les insérer par leur url. 

BOLD : pour écrire en gras

<span>
------
L' élément HTML<span> est un conteneur en ligne générique pour le phrasé du contenu, qui ne représente rien en soi. Il peut être

utilisé pour regrouper des éléments à des fins de style (à l'aide des attributs class o% id), ou parce qu'ils partagent des 

valeurs d'attribut, telles que lang. Il ne doit être utilisé que si aucun autre élément sémantique n'est approprié. 

<span>ressemble beaucoup à un <div>élément, mais <div>est un élément de niveau bloc alors que a <span>est un élément en ligne .
````html
<p>Add the <span class="ingredient">basil</span>, 
<span class="ingredient">pine nuts</span> and
<span class="ingredient">garlic</span>
to a blender and blend into a paste.</p>

<p>Gradually add the <span class="ingredient">olive oil</span>
 while running the blender slowly.</p>
````
````css
span.ingredient {
color: #f00;
}
````
EXEMPLE 1
````html
<p><span>Some text</span></p>
````
EXEMPLE 2
````html
<li><span>
    <a href="portfolio.html" target="_blank">See my portfolio</a>
</span></li>
````
````css
i span {
  background: gold;
 }
````
Déclarer une classe simple
--------------------------
Dans l'exemple qui suit, on définit une classe Polygone pour laquelle on crée un sous-classe Carré. On note ici que la méthode super() ne peut être

utilisée qu'au sein d'un constructeur et doit être appelée avant l'utilisation du mot-clé this.
````html
class Polygon {
constructor(height, width) {
this.name = 'Polygon';
this.height = height;
this.weight = width;
}
}

class Square extends Polygon {
constructor(length) {
super(length, length);
this.name = 'Square';
}
}
````
**Attention** : Déclarer une classe deux fois lèvera une exception SyntaxError. 

De même, on ne pourra pas réutiliser un nom qui a déjà été utilisé dans une expression de classe.

// Deux déclarations avec le même nom
class Toto {};
class Toto {}; // Uncaught SyntaxError: Identifier 'Toto' has already been declared

// Expression puis déclaration
var Truc = class {};
class Truc {}; // Uncaught TypeError: Identifier 'Truc' has already been declared
