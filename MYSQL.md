MY SQL
======


**Une base de données MySQL impose de définir un schéma de données à l'avance pour pouvoir travailler avec.**

[DOCUMENTATION W3schools](https://www.w3schools.com/sql/sql_insert.asp)

[MYSQL](https://www.mysql.com/products/workbench/)

[TABLEPLUS](https://tableplus.com/)

[TUTORIAL](https://www.tutorialrepublic.com/sql-tutorial/)

[NPMJS](https://www.npmjs.com/package/mysql)

001. MySQL - Introduction => https://youtu.be/
     ----------------------------------------
     
     ![](![img_14.png](img_14.png))
     
002. MySQL - définitions mysql sql => https://youtu.be/1pGpKOjl7rc
     -------------------------------------------------------------
     
     ![](![img_15.png](img_15.png))
     
**MySQL est un système de gestion de données relationnel (SGBDL).**
     
**Créer en 1994 par Mickael WEDENUSS, il permet de rechercher, ajouter, modifier où supprimer dans les bases de données relationnelles.**
     
**MySQL est un systèmes de gestion de données relationnelles et SQL est un langage standard permettant d'effectuer des requêtes à notre base de données relationnelles.**
     
**Les bases de données sont organisés dans un tableau à 2 dimensions, les lignes sont appelées des enregistrements où records en anglais, les colonnes sont appelées des attributs où champ(column).**

003. MySQL - installation => https://youtu.be/waf2ljjGvjg
     ----------------------------------------------------
     [LIEN DE TELECHARGEMENT](https://sourceforge.net/projects/wampserver/SourceForge)
     
     Puis suivre la procédure d'installation.
004. MySQL - connexion au server mysql => https://youtu.be/1LMqSjqbvdo
     -----------------------------------------------------------------
     
     INSERT INTO table_name (column1, column2, column3, ...)
     VALUES (value1, value2, value3, ...);
     
005. MySQL - créer une table découverte des options => https://youtu.be/IVE7pGjknSc
     -------------------------------------------------------------------------------

![img_23.png](img_23.png)

- créer et configurer une base de données :
    - click droit sur table puis create table
    - indiquez le nom de la table
    - ex.products(je veux que cette table contienne tous les enregistre de mes produits, c'est dans cette table que je vais stocker tous mes produits)
    - dans la 1er colonne on peut choisir le nom de la colonne
    - 2ème colonne on peut choisir le type de celle-ci(select et on peut dérouler pour choisir) :
          - INT pour integer(intégrer)un nombre entier
      
          - VARCHAR va contenir une chaîne de caractères
      
          - DECIMAL (décimal)
      
          - DATE TIME
      
          - ETC
      
          - après certain mot il y a des parenthèses : c'est pour fixer une limite de caractères
        
          - 
006. MySQL - configuration de la table produit => https://youtu.be/dpi-9FplE2s
     -------------------------------------------------------------------------
     
007. MySQL - insérer des données insert into => https://youtu.be/4-mz-PwZvnM
     -----------------------------------------------------------------------
     
008. MySQL - supprimer des données delete from => https://youtu.be/vcXVZBIuRGs
     --------------------------------------------------------------------------
     
009. MySQL - mise à jour des données update => https://youtu.be/_vdYNhbX7Kw
     ----------------------------------------------------------------------
     
010. MySQL - sélectionner des données avec select => https://youtu.be/Ej2mRKXyLjc
     ----------------------------------------------------------------------------
     
011. MySQL - trier les données avec order by => https://youtu.be/5CsdFpj7Nyc
     -----------------------------------------------------------------------
     
012. MySQL - limiter les données sélectionnées avec limit => https://youtu.be/zP8J8u2THZI
     ------------------------------------------------------------------------------------
     
013. MySQL - compter les données avec la fonction count => https://youtu.be/_x-5nrTnQww
     ----------------------------------------------------------------------------------
     
014. MySQL - grouper les données et compter par groupe => https://youtu.be/cNWj9RmRFmQ
     ---------------------------------------------------------------------------------
     
015. MySQL - additionner les données grâce a la fonction numérique sum => https://youtu.be/GuyovYkZdaU
     -------------------------------------------------------------------------------------------------
     
016. MySQL - rechercher des données de manière partielle => https://youtu.be/jnryTVWPfvk
     -----------------------------------------------------------------------------------
     
017. MySQL - conclusion => https://youtu.be/COLKWLz0omY
     -------------------------------------------------------

const sequelize = new Sequelize({
database: 'superpizza',
dialect: 'mysql',
host: 'localhost',
username: 'root',
password: '',
});
