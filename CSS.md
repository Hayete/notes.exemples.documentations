#CSS
====


##HISTORIQUE
------------

Les feuilles de style en cascade1, généralement appelées CSS de l'anglais Cascading Style

Sheets, forment un langage informatique qui décrit la présentation des documents HTML et XML.

Les standards définissant CSS sont publiés par le World Wide Web Consortium (W3C). Introduit 

au milieu des années 1990, CSS devient couramment utilisé dans la conception de sites web et

bien pris en charge par les navigateurs web dans les années 2000.

##UTILISATION
-------------

- On utilise le css pour mettre en forme un contenu de page.
  
- Comme changer la couleur, la police d'écriture, etc, ...
  
- Où écrire le css?
  
- *dans l'élèment head du html. 
  
- dans la balise ouvrante d'un document.
  
- *dans un fichier css séparé.

- il est recommandé d'utilisé la dernière méthode.

##UTILITAIRE
------------

- pour écrire un commentaire en css : /*--------comemntaire*/

- un point (.) pour aller cher un mot dans le html

- 


/*-----------------------------------------------------GENERAL*/
````css
h1, h2, button, a {
font-family: "Josefin Sans", sans-serif;
}

p {
font-family: "Playfair Display", serif;
font-size: 14px;
}

h1 {
font-size: 58px;
line-height: 64px;
}

h2 {
font-size: 44px!important;
letter-spacing: 5px;
text-transform: uppercase;
margin: 60px 0 40px!important;
}

h3 {
font-size: 30px!important;
font-weight: 600;
text-transform: uppercase;
color: #ad8438;
}

button {
text-transform: uppercase!important;
border-radius: 0!important;
padding-top: 10px!important;
padding-bottom: 10px!important;
}

/*-----------------------------------------------------NAV*/

nav {
font-size: 20px;
}

nav li a {
font-size: 16px;
transition: ease-in-out 0.3s;
}

nav li a:hover {
color: black;
background-color: rgba(255, 255, 255, 0.3);
}

/*-----------------------------------------------------PARTIE ACCROCHE*/

.bg-accroche {
background-image: url("assets/img/dinner-4137381_1920.jpg");
background-size: cover;
background-repeat: no-repeat;
height: 800px;
color: white;
}

.alpha-bg {
background-color: rgba(0,0,0,0.5);
padding-right: 50px!important;
}

/*-----------------------------------------------------PARTIE SERVICES*/

.bg-light-grey {
background-color: #efefef;
}

.services {
max-width: 1200px;
}

/*-----------------------------------------------------PARTIE FORMULAIRE*/

.bg-footer  {
background-image: url('assets/img/table-3084384_1920.jpg');
background-size: cover;
background-repeat: no-repeat;
color: white;
}

footer h2 {
color: white;
}

footer .alpha-bg {
padding-right: 15px!important;
max-width: 1400px;
}

form div {
margin: 15px 0;
}

label {
font-family: "Josefin Sans", sans-serif;
text-transform: uppercase;
}

input, textarea {
border-radius: 0!important;
background-clip: unset;
border: none!important;
}

input::placeholder, textarea::placeholder {
font-size: 12px;
color: #ad8438 !important;
}

footer .copyright {
font-family: "Josefin Sans", sans-serif;
font-size: 12px!important;
text-align: center;
}

footer .copyright a {
color: #ad8438;
}

@media screen and (max-width: 1200px) {
.alpha-bg {
padding-right: 15px!important;
}
}
/---------------------------------------------/
lesson css
.container-lesson{
background-color: white;
height: 1000px;
grid-area: cl;
margin: 10px 10px 0 10px;
}
/-------------------------------------------/
lesson jsx
````jsx
import React from 'react'
import "./Lesson.css"

function Lesson(){
return (
<div className="container-lesson cl"></div>

    )
}
````
export default Lesson;
/----------------------------------------------------/
````css
header css
.me_container{
width: 100%;
background-color: white;
display: flex;
justify-content: center;
align-items: center;
}
.container-contenu{
display: flex;
justify-content: space-between;
align-items: center;
width: 95%;
}
.container-p1{
display: flex;
height: 100px;
left: 41px;
top: 0px;
display: flex;
align-items: center;
column-gap: 10px;
}
.container-p1 h2{
font-family: PT Sans;
font-style: normal;
font-weight: bold;
font-size: 25px;
color: #303030;
}
.container-p1 input{
font-family: PT Sans;
font-style: italic;
font-weight: bold;
font-size: 14px;
line-height: 18px;
color: #8B8B8B;
width: 300px;
height: 35px;

    background: #E8E8E8;
    border: 1px solid #D4D4D4;
    box-sizing: border-box;
    border-radius: 6px;
}

.container-p2 button{
border-radius: 150px;
color: white;
background-color: #587896;
font-size: 12px;
height: 35px;
width: 110px;
border: none;
font-family: PT Sans;

}
.container-p1 button{
border-radius: 150px;
color: white;
background-color: black;
font-size: 12px;
height: 35px;
width: 110px;
border: none;
font-family: PT Sans;
}
/----------------------------------------------------------------/
/*asside*/
*{
margin: 0;
padding: 0;
}
.aside{
display: flex;
height: 600px;
background-color: white;
margin: 10px 0 0 10px;
flex-direction: column;

    grid-area: aside;
}

.container-menu{
height: 85px;
display: flex;
width: 100%;
border-bottom: solid 1px #E8E8E8;
color: #8B8B8B;
font-size: 16px;
}
.ajout{
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
height: 85px;
width: 50%;
padding-top: 20px;
}
.ajout svg{
font-size: 20px;
}
.ajout p{
font-family: PT Sans;
font-weight: bold;
display: flex;
align-items: center;
text-align: center;
}
.container-menu div:first-child{
border-right: solid 1px #E8E8E8;
}

.container-items{
margin-top: 20px;
}

.big-item{
display: flex;
flex-direction: column;
justify-content: space-between;

    width: 95%;
}
.rg{
display: flex;
flex-direction: row;
justify-content: space-between;
}
.icp{
color: #D4D4D4;
}
.st{
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
}
.st h6{
margin: 0;
color: #587896;
font-family: PT Sans;
font-style: normal;
font-weight: bold;
font-size: 17px;
}
.container-icon-big-items{
display: flex;
flex-direction: row;
justify-content: space-between;
width: 110px;
}
.i{
width: 50px;
height: 50px;
border: solid 1px #E8E8E8;
border-radius: 10px;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
color: #D4D4D4;
}
.fa-ul-me{
margin-left: 0px;
margin-top: 10px;
}
.d2{
width: 100%;
display: flex;
flex-direction: row;
justify-content: center;
align-items: center;
margin-left: 20px;
}
.d2 input{
width: 95%;
height: 35px;

    background: #E8E8E8;

    /* Grey/level 2 */
    border: 1px solid #D4D4D4;
    box-sizing: border-box;
    border-radius: 6px;
    font-family: PT Sans;
    font-style: italic;
    font-weight: bold;
    font-size: 14px;
    line-height: 18px;
    display: flex;
    align-items: center;

    color: #8B8B8B;
}
/------------------------------------------------------------/
main.css
.main{
display: grid;
grid-template-columns: 1fr 1fr 1fr 1fr;
grid-template-rows: 1fr 1fr 1fr;
gap: 0px 0px;
grid-template-areas:
"aside cl cl cl"
"aside cl cl cl"
"aside cl cl cl";
}

````
![img_63.png](img_63.png)

CSS GRID(grille)
----------------
[css-trick grid](https://css-tricks.com/snippets/css/complete-guide-grid/)

![img.png](img.png)

![img_1.png](img_1.png)

![img_10.png](img_10.png)

![img_11.png](img_11.png)

![img_12.png](img_12.png)

![img_70.png](img_70.png)

![img_74.png](img_74.png)

![img_71.png](img_71.png)

![img_73.png](img_73.png)

![img_75.png](img_75.png)

![img_76.png](img_76.png)

![img_77.png](img_77.png)

![img_78.png](img_78.png)

![img_79.png](img_79.png)

![img_80.png](img_80.png)

![img_81.png](img_81.png)

![img_82.png](img_82.png)

![img_83.png](img_83.png)

![img_84.png](img_84.png)

![img_85.png](img_85.png)

![img_86.png](img_86.png)

Ce comportement peut également être défini sur des éléments de grille individuels 
via la justify-selfpropriété.

![img_87.png](img_87.png)

![img_88.png](img_88.png)

![img_89.png](img_89.png)

![img_90.png](img_90.png)

![img_91.png](img_91.png)

![img_92.png](img_92.png)

![img_93.png](img_93.png)

![img_94.png](img_94.png)

![img_95.png](img_95.png)

![img_96.png](img_96.png)

![img_97.png](img_97.png)

![img_98.png](img_98.png)

![img_99.png](img_99.png)



  
